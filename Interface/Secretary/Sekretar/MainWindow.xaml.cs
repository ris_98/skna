﻿using Bolnica.Controller;
using Bolnica.Model;
using Bolnica.Model.ModelDTO;
using Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sekretar
{
    /// <summary>
    /// Interaction logic for login.xaml
    /// </summary>
    public partial class login : Window
    {
        private LogInContoller logInContoller = new LogInContoller();
        public login()
        {
            InitializeComponent();
           
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            UserDTO info = new UserDTO(username.Text.ToString(),pass.Password.ToString());
            UserDTO logininfo = logInContoller.SignIn(info);
            
            if (logininfo == null)
            {
                obavestenje.Visibility = Visibility.Visible;
                return;

            }
            Pomoc.user = logininfo;
            MainWindow obj = new MainWindow();
            obj.Show();
            this.Close();



        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void StackPanel_Loaded(object sender, RoutedEventArgs e)
        {
            //Wizard wizard = new Wizard();
            //wizard.Show();
        }
    }
}

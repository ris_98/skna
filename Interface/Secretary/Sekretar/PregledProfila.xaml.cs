﻿using Bolnica.Model;
using HandyControl.Tools.Extension;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Sekretar
{
    /// <summary>
    /// Interaction logic for PregledProfila.xaml
    /// </summary>
    public partial class PregledProfila : UserControl
    {
        private Grid grid;
        private TextBlock tekstPomoc;



        public PregledProfila(Grid grid, TextBlock tekst)
        {
            this.grid = grid;
            this.tekstPomoc = tekst;
            grid.Children.Clear();
            InitializeComponent();
            

            
            tekstPomoc.Text = "Izmenite vaše osnovne podatke IZMENA PROFILA ili dodajte novu sliku IZMENA SLIKE";
            email.Text ="email: " +Pomoc.user.EMail;
            brtel.Text = "Broj telefona: " +Pomoc.user.TelephoneNumber ;
            grad.Text = "Grad: " + Pomoc.user.Adress.City;
            korime.Text = "Korisnicko ime: " + Pomoc.user.Username ;
            ime.Text = "Ime: " + Pomoc.user.FirstName;
            prezime.Text = "Prezime: " + Pomoc.user.LastName;
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            grid.Children.Clear();
            grid.Children.Add(new izmenaprofila(tekstPomoc, grid,Pomoc.user.ID,"sekretar"));
            
        }
    }
}
 
﻿using Bolnca.Controller;
using Bolnica.Model.ModelDTO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Input;

namespace Sekretar
{
    /// <summary>
    /// Interaction logic for QA.xaml
    /// </summary>
    public partial class QA : UserControl
    {
        private Grid grid;
        private TextBlock tekstPomoc;
        QuestionController questionController = new QuestionController();
        public ObservableCollection<Pitanje> Pitanja { get; set; }


        public QA(Grid gridPanel, TextBlock tekst)
        {

            InitializeComponent();
            Pitanja = new ObservableCollection<Pitanje>();
            List<QuestionDTO> questions = questionController.ShowQuestions();
            foreach(QuestionDTO q in questions){
                Pitanje pit = new Pitanje { id = q.ID.ToString(),odgovreno="DA",naslov="Naslov pitanja",pitanje = q.QuestionContent, odgovor = q.QuestionAnswer, imeprezimeP = q.Patient.FirstName + " " + q.Patient.LastName };
                if (q.QuestionAnswer.Equals(""))
                    pit.odgovreno = "NE";
                    
                Pitanja.Add(pit);
            }
            pitanjaa.ItemsSource = Pitanja;


            this.tekstPomoc = tekst;
            this.grid = gridPanel;
            //this.tekstPomoc.Text = "Pritiskom na pitanje možete ga obrisati/odgovoriti na njega ili samo pogledati";
        }

        private void dataGridPacijenti_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Console.WriteLine(kljuc.Text);

            grid.Children.Clear();
            grid.Children.Add(new PrikazJednogPitanja(grid, tekstPomoc,int.Parse(kljuc.Text)));
        }


        public class Pitanje
        {
            public string id { get; set; }
            public string naslov { get; set; }
            public string imeprezimeP { get; set; }

            public string pitanje { get; set; }
            public string odgovor { get; set; }

            public string odgovreno { get; set; }
        }

    }
}





﻿using Bolnica.Controller;
using Bolnica.Model.ModelDTO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Sekretar
{
    /// <summary>
    /// Interaction logic for izmenaprofila.xaml
    /// </summary>
    public partial class izmenaprofila : UserControl
    {
        SecretaryController secretaryController = new SecretaryController();

        private TextBlock tekstPomoc;
        private Grid grid;
        string type;
        UserDTO userDTO;

        public izmenaprofila(TextBlock tekst, Grid grid, int id, string type)
        {
            InitializeComponent();
            this.tekstPomoc = tekst;
            tekstPomoc.Text = "Ukoliko zelite da promenit lozinku kliknite na dugme PROMENA LOZINKE i dobicete detaljnije instrukcije na Vas email";
            userDTO = new UserDTO();
            this.grid = grid;
            this.type = type;
            if (type.Equals("sekretar")){
                userDTO = Pomoc.user;
                korime.Text = userDTO.Username;
                grad.Text = userDTO.Adress.City;
                drzava.Text = userDTO.Adress.Country;
                brtel.Text = userDTO.TelephoneNumber;
                JMBG.Text = userDTO.JMBG;
                ime.Text = userDTO.FirstName;
                prezime.Text = userDTO.LastName;
                email.Text = userDTO.EMail;
                userDTO.Adress.ZipCode = userDTO.Adress.ZipCode;
                userDTO.Adress.Street = userDTO.Adress.Street;
                userDTO.Adress.ID = userDTO.Adress.ID;
                userDTO.ID = userDTO.ID;
            }
            else if (type.Equals("registrovan"))
            {
                PatientDTO patientDTO = secretaryController.GetPatientByID(id);
                korime.Text = patientDTO.Username;
                grad.Text = patientDTO.Adress.City;
                drzava.Text = patientDTO.Adress.Country;
                brtel.Text = patientDTO.TelephoneNumber;
                JMBG.Text = patientDTO.JMBG;
                ime.Text = patientDTO.FirstName;
                prezime.Text = patientDTO.LastName;
                email.Text = patientDTO.EMail;
                userDTO.ID = patientDTO.ID;
                userDTO.Adress.ZipCode = patientDTO.Adress.ZipCode;
                userDTO.Adress.Street = patientDTO.Adress.Street;
                userDTO.Adress.ID = patientDTO.Adress.ID;
            }
            else
            {
                PersonDTO personDTO = secretaryController.GetPersonByID(id);
                grad.Text = personDTO.Adress.City;
                drzava.Text = personDTO.Adress.Country;
                brtel.Text = personDTO.TelephoneNumber;
                JMBG.Text = personDTO.JMBG;
                ime.Text = personDTO.FirstName;
                prezime.Text = personDTO.LastName;
                dugme.Visibility = Visibility.Collapsed;
                userDTO.Adress.ZipCode = personDTO.Adress.ZipCode;
                userDTO.Adress.Street = personDTO.Adress.Street;
                userDTO.Adress.ID = personDTO.Adress.ID;
                userDTO.ID = personDTO.ID;

            }


        }

        private void Sacuvaj(object sender, RoutedEventArgs e)
        {
            bool greska = false;
            //IME
            if (ime.Text.Equals(""))
            {
                greska = true;
                ime.BorderBrush = Brushes.Red;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(ime, "Polje ime je prazno");
            }
            else
            {
                ime.BorderBrush = Brushes.Green;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(ime, "");

            }
            //PREZIME
            if (prezime.Text.Equals(""))
            {
                greska = true;
                prezime.BorderBrush = Brushes.Red;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(prezime, "Polje prezime je prazno");
            }
            else
            {
                prezime.BorderBrush = Brushes.Green;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(prezime, "");

            }
            //JMBG
            if (JMBG.Text.Equals(""))
            {
                greska = true;
                JMBG.BorderBrush = Brushes.Red;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(JMBG, "Polje JMBG je prazno");
            }
            else if (JMBG.Text.Length != 13)
            {
                greska = true;
                JMBG.BorderBrush = Brushes.Red;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(JMBG, "Pogresan broj cifara JMBGa");
            }
            else
            {
                JMBG.BorderBrush = Brushes.Green;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(JMBG, "");

            }
            //GRAD
            if (grad.Text.Equals(""))
            {
                greska = true;
                grad.BorderBrush = Brushes.Red;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(grad, "Polje grad je prazno");
            }
            else
            {
                grad.BorderBrush = Brushes.Green;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(grad, "");

            }
            //DRZAVA
            if (drzava.Text.Equals(""))
            {
                greska = true;
                drzava.BorderBrush = Brushes.Red;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(drzava, "Polje drzava je prazno");
            }
            else
            {
                drzava.BorderBrush = Brushes.Green;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(drzava, "");

            }
            //TELEFON
            if (brtel.Text.Equals(""))
            {
                greska = true;
                brtel.BorderBrush = Brushes.Red;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(brtel, "Polje broj telefona je prazno");
            }
            else if (!brtel.Text.StartsWith("+3816"))
            {
                greska = true;
                brtel.BorderBrush = Brushes.Red;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(brtel, "Broj telefona mora da pocinje sa +3816");
            }
            else if (brtel.Text.Length != 13)
            {
                greska = true;
                brtel.BorderBrush = Brushes.Red;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(brtel, "Broj nije validan");
            }
            else
            {
                brtel.BorderBrush = Brushes.Green;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(brtel, "");
            }
            
            if (type.Equals("registrovan")) // registrovan
            {

                if (korime.Text.Equals(""))
                {
                    korime.BorderBrush = Brushes.Red;
                    greska = true;
                    MaterialDesignThemes.Wpf.HintAssist.SetHelperText(korime, "Polje korisnicko ime je prazno");

                }
                else
                {
                    korime.BorderBrush = Brushes.Green;
                    MaterialDesignThemes.Wpf.HintAssist.SetHelperText(korime, "");

                }

                if (email.Text.Equals(""))
                {
                    email.BorderBrush = Brushes.Red;
                    greska = true;
                    MaterialDesignThemes.Wpf.HintAssist.SetHelperText(email, "Polje email je prazno");

                }
                else
                {
                    email.BorderBrush = Brushes.Green;
                    MaterialDesignThemes.Wpf.HintAssist.SetHelperText(email, "");

                }
            }
            
            if (greska)
                return;

            userDTO.Adress.City = grad.Text;
            userDTO.Adress.Country = drzava.Text;
            if (type.Equals("sekretar"))
            {
                SecretaryDTO secretaryDTO = new SecretaryDTO(email.Text, korime.Text, userDTO.Password, userDTO.ID, ime.Text, prezime.Text, brtel.Text, JMBG.Text, userDTO.Adress);
                secretaryController.UpdateMYACC(secretaryDTO);
                
            }
            else if (type.Equals("registrovan"))
            {
                PatientDTO patientDTO = new PatientDTO(email.Text, korime.Text, userDTO.Password, userDTO.ID, ime.Text, prezime.Text, brtel.Text, JMBG.Text, userDTO.Adress);
                secretaryController.UpdateUser(patientDTO);
            }
            else
            {
                PersonDTO person = new PersonDTO(userDTO.ID, ime.Text, prezime.Text, brtel.Text, JMBG.Text, userDTO.Adress);
                secretaryController.UpdatePerson(person);
            }

            grid.Children.Clear();
            grid.Children.Add(new PregledProfila(grid, tekstPomoc));

            

        }

        private void Odustani(object sender, RoutedEventArgs e)
        {
            if (type.Equals("sekretar"))
            {
                grid.Children.Clear();
                grid.Children.Add(new PregledProfila(grid, tekstPomoc));
            }
            else
            {
                grid.Children.Clear();
                grid.Children.Add(new PregledPacijenata(grid, tekstPomoc));
            }

        }

        private void PromenaLozinke(object sender, RoutedEventArgs e)
        {

            if (type.Equals("sekretar"))
            {
                bool? Result = new popup("Korisnik će na mail dobiti uputstva", MessageType.Success, MessageButtons.Ok).ShowDialog();

                if (Result.Value)
                {
                    grid.Children.Clear();
                    grid.Children.Add(new PregledProfila(grid, tekstPomoc));
                }


            }
            else
            {
                bool? Result = new popup("Na email ćete dobiti uputstva o promeni šifre ", MessageType.Success, MessageButtons.Ok).ShowDialog();

                if (Result.Value)
                {
                    grid.Children.Clear();
                    grid.Children.Add(new PregledPacijenata(grid, tekstPomoc));
                }
            }
        }
    }
}

﻿using Bolnica.Controller;
using Bolnica.Model.ModelDTO;
using System.Windows;
using System.Windows.Controls;

namespace Sekretar
{
    /// <summary>
    /// Interaction logic for PrikazJednogPacijenta.xaml
    /// </summary>
    public partial class PrikazJednogPacijenta : UserControl
    {
        private EmployeeContoller employeeContoller = new EmployeeContoller();
        private SecretaryController secretaryController = new SecretaryController();
        PatientDTO patientDTO;

        private Grid grid;
        private TextBlock tekstPomoc;
        public PrikazJednogPacijenta(Grid grid, TextBlock tekst, int id)
        {
            patientDTO = employeeContoller.GetPatientByID(id);

            if (Pomoc.tip)
            {
                PersonDTO personDTO = secretaryController.GetPersonByID(id);
                patientDTO.Adress = personDTO.Adress;
                patientDTO.EMail = "nema";
                patientDTO.FirstName = personDTO.FirstName;
                patientDTO.LastName = personDTO.LastName;
                patientDTO.Password = "nema";
                patientDTO.TelephoneNumber = personDTO.TelephoneNumber;
                patientDTO.Username = "nema";
            }
            InitializeComponent();
            this.grid = grid;
            this.tekstPomoc = tekst;
            
            Ime.Text = "Ime: " + patientDTO.FirstName;
            Prezime.Text = "Prezime: " + patientDTO.LastName;
            Grad.Text = "Grad: " + patientDTO.Adress.City;
            JMBG.Text = "JMBG: " + patientDTO.JMBG;
            Drzava.Text = "Drzava: " + patientDTO.Adress.Country;
            Brojtel.Text = "Brojtel: " + patientDTO.TelephoneNumber;
            if (!Pomoc.tip)
            {
                ikonicaDugme.Kind = MaterialDesignThemes.Wpf.PackIconKind.PasswordReset;
                textDugme.Text = "Promena lozinke";
                email.Text = "email: " + patientDTO.EMail;
                KorisnickoIme.Text = "KorisnickoIme: " + patientDTO.Username;
            }
            else
            {
                ikonicaDugme.Kind = MaterialDesignThemes.Wpf.PackIconKind.AccountAdd;
                textDugme.Text = "Nadogradi nalog";
            }


        }

        private void Izmeni(object sender, RoutedEventArgs e)
        {
            string a = "registrovan";
            if (patientDTO.EMail == "nema")
                a= "cao";
            grid.Children.Clear();
            grid.Children.Add(new izmenaprofila(tekstPomoc, grid,patientDTO.ID,a));
        }

        private void Dual(object sender, RoutedEventArgs e)
        {

            if (Pomoc.tip) 
            {

                Pomoc.tip = false;
                grid.Children.Clear();
                grid.Children.Add(new izmenaprofila(tekstPomoc, grid,patientDTO.ID, "guest"));
               

            }
            else
            {
                bool? Result = new popup("Korisnik će na mail dobiti uputstva", MessageType.Success, MessageButtons.Ok).ShowDialog();

                if (Result.Value)
                {
                    grid.Children.Clear();
                    grid.Children.Add(new PregledPacijenata(grid,  tekstPomoc));
                }

            }

        }

        private void PrikazTermina(object sender, RoutedEventArgs e)
        {
                        
            grid.Children.Add(new IzmenaTermina(grid, tekstPomoc));
        }

        private void SviPacijenti(object sender, RoutedEventArgs e)
        {
            grid.Children.Clear();
            grid.Children.Add(new PregledPacijenata(grid,tekstPomoc));
        }


        private void ZakaziOperaciju(object sender, RoutedEventArgs e)
        {
            grid.Children.Clear();
            grid.Children.Add(new ZakazivanjePregleda(grid, tekstPomoc));

        }

        private void ZakaziPregled(object sender, RoutedEventArgs e)
        {
            grid.Children.Clear();
            grid.Children.Add(new ZakazivanjePregleda( grid, tekstPomoc));

        }
    }
}

﻿using Bolnica.Controller;
using Bolnica.Model.ModelDTO;
using Controller;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Sekretar
{
    /// <summary>
    /// Interaction logic for ZakazivanjePregleda.xaml
    /// </summary>
    public partial class ZakazivanjePregleda : UserControl
    {
        UserController userController = new UserController();
        SecretaryController secretaryController = new SecretaryController();
        EmployeeContoller employeeContoller = new EmployeeContoller();
        AppointmentController appointmentController = new AppointmentController();
        AppointmentDTO appointmentDTO = new AppointmentDTO();

        public ObservableCollection<Lekar> Lekari { get; set; }
        public ObservableCollection<Pacijent> Pacijenti { get; set; }
        private Grid grid;
        public int suma = 0;
        private TextBlock tekstPomoc;

        public ZakazivanjePregleda(Grid grid, TextBlock tekst)
        {
            Lekari = new ObservableCollection<Lekar>();
            Pacijenti = new ObservableCollection<Pacijent>();

            List<DoctorDTO> doctors = userController.GetAllDoctors();
            List<PatientDTO> patients = employeeContoller.ShowPatients();

            foreach (PatientDTO p in patients)
            {
                Pacijent pac = new Pacijent { brojTelefona = p.TelephoneNumber, id = p.ID.ToString(), pass = p.Password, drzava = p.Adress.Country, email = p.EMail, grad = p.Adress.City, ime = p.FirstName, ImePrezime = p.FirstName + " " + p.LastName, JMBG = p.JMBG, korisnickoIme = p.Username, prezime = p.LastName, tipNaloga = "registrovan" };
                Pacijenti.Add(pac);
            }

            List<PersonDTO> personDTOs = employeeContoller.GetAllPersons();
            foreach (PersonDTO p in personDTOs)
            {
                Pacijent pac = new Pacijent { brojTelefona = p.TelephoneNumber, id = p.ID.ToString(), pass = "nema", drzava = p.Adress.Country, email = "nema", grad = p.Adress.City, ime = p.FirstName, ImePrezime = p.FirstName + " " + p.LastName, JMBG = p.JMBG, korisnickoIme = "nema", prezime = p.LastName, tipNaloga = "guest" };
                Pacijenti.Add(pac);
            }

            foreach (DoctorDTO d in doctors)
            {
                Lekar l = new Lekar { id = d.ID.ToString(), Ime = d.FirstName, Prezime = d.LastName, Specijalnost = d.Specialization.ToString(), ImePrezime = d.FirstName + " " + d.LastName };
                Lekari.Add(l);
            }


            InitializeComponent();           

            dataGridPacijenti.ItemsSource = Pacijenti;
            dataGridLekar.ItemsSource = Lekari;
            this.grid = grid;
            this.tekstPomoc = tekst;
            
            if (Pomoc.da)
            {
                appointmentDTO = appointmentController.GetAppointmentByID(Pomoc.kljucic);
                dataGridPacijenti.Visibility = Visibility.Collapsed;
                radiobatni.Visibility = Visibility.Collapsed;
                bug.Visibility = Visibility.Collapsed;
                Batoni.Visibility = Visibility.Visible;
                pacijentName.Text = appointmentDTO.PatientDTO.FirstName + " " + appointmentDTO.PatientDTO.LastName;
                suma += 1001;

                if (appointmentDTO.AppointmentType == Bolnica.Model.AppointmentType.Examination)
                {
                    naslov.Text = "Zakazivanje pregleda [" + pacijentName.Text + "]";
                }
                else
                {
                    naslov.Text = "Zakazivanje operacije [" + pacijentName.Text + "]";
                }
                //if (!terminInfo.id.Equals("neBrisi"))
                  //  Termini.Remove(this.terminInfo);
            
            }
             tekstPomoc.Text = "";
            
        }

        private void TraziVreme(object sender, RoutedEventArgs e)
        {

            obavestenje.Visibility = Visibility.Collapsed;

            if (doktorImeiPrezime.Text.Equals(""))
            {
                if (suma % 10 == 0)
                {
                    obavestenje.Visibility = Visibility.Visible;
                    obavestenje.Text = "Odaberite prvo doktora";
                    return;
                }
                
            }
            if (suma % 10 == 0)
            {
                suma += 1;
            }
            VremeOdabir.Visibility = Visibility.Visible;
            terminbutton.Visibility = Visibility.Collapsed;
            preporukabutton.Visibility = Visibility.Collapsed;
            dataGridLekar.Visibility = Visibility.Collapsed;
            dataGridPacijenti.Visibility = Visibility.Collapsed;
            
            suma += 10;
            if (suma == 1111)
            {
                ZakaziButton.IsEnabled = true;
            }
        }

        private void TraziDoktora(object sender, RoutedEventArgs e)
        {

            obavestenje.Visibility = Visibility.Collapsed;

            if (jmbg.Text.Equals(""))
            {
                if (suma % 10==0)
                {

                    obavestenje.Visibility = Visibility.Visible;
                    return;
                }
            }

            dataGridLekar.Visibility = Visibility.Visible;
            DoktorOdabir.Visibility = Visibility.Visible;
            doctorbutton.Visibility = Visibility.Collapsed;
            preporukabutton.Visibility = Visibility.Collapsed;
            dataGridPacijenti.Visibility = Visibility.Collapsed;

            //SadasnjiTermin.ImeiPrezimePacijenta = pacijentName.Text;
            terminbutton.IsEnabled = true;
            suma += 100;
            if (suma == 1111)
            {
                ZakaziButton.IsEnabled = true;
            }
        }

        private void Preporuka(object sender, RoutedEventArgs e)
        {

            obavestenje.Visibility = Visibility.Collapsed;
            if (TekstBlockPreporuka.Text.Equals("Preporuka termina"))
            {
                obavestenje.Visibility = Visibility.Collapsed;
                if (jmbg.Text.Equals(""))
                {
                    if (suma % 10 == 0)
                    {

                        obavestenje.Visibility = Visibility.Visible;
                        return;
                    }
                }
                if (suma % 10 == 0)
                {
                    suma += 1;
                }
                stackpanelpreporuka.Visibility = Visibility.Visible;
                doctorbutton.Visibility = Visibility.Collapsed;
                terminbutton.Visibility = Visibility.Collapsed;
                dataGridPacijenti.Visibility = Visibility.Collapsed;
                dataGridPacijenti.Visibility = Visibility.Collapsed;

                TekstBlockPreporuka.Text = "Preporuči";

                //SadasnjiTermin.ImeiPrezimePacijenta = pacijentName.Text;


            }
            else
            {

                if (DejtPicker.SelectedDate == null)
                {
                    obavestenje.Text = "Odaberite datum ";
                    obavestenje.Visibility = Visibility.Visible;
                    preporukabutton.IsEnabled = true;
                    return;

                }
                preporukabutton.IsEnabled = false;

                DoktorOdabir.Visibility = Visibility.Visible;
                dataGridLekar.Visibility = Visibility.Collapsed;
                stackpanelpreporuka.Visibility = Visibility.Collapsed;
                VremeOdabir.Visibility = Visibility.Visible;

                OdabranDatum.SelectedDate = DejtPicker.SelectedDate;
                OdabranoVreme.SelectedTime = DejtPicker.SelectedDate;

                //doktorImeiPrezime.Text = Lekari[1].ImePrezime;

                DateTime datum = (DateTime) DejtPicker.SelectedDate;

                //SadasnjiTermin.ImeiPrezimeLekara = Lekari[1].ImePrezime;
                //SadasnjiTermin.DatumVreme = datum;
                suma += 110;
                if (suma == 1111)
                {
                    ZakaziButton.IsEnabled = true;
                }
            }

        }

        private void Zakazi(object sender, RoutedEventArgs e)
        {
            //SadasnjiTermin.ImeiPrezimePacijenta = pacijentName.Text;
            //SadasnjiTermin.ImeiPrezimeLekara = doktorImeiPrezime.Text;
            /*
            if (terminInfo != null)
            {
                SadasnjiTermin.ImeiPrezimePacijenta = terminInfo.ImeiPrezimePacijenta;
            }
            */
            if (OdabranoVreme.SelectedTime == null)
            {
                obavestenje.Text = "Odaberite vreme ";
                obavestenje.Visibility = Visibility.Visible;
                return;
            }
            if (OdabranDatum.SelectedDate == null)
            {
                obavestenje.Text = "Odaberite datum ";
                obavestenje.Visibility = Visibility.Visible;
                return;
            }
            DateTime sat = (DateTime)OdabranoVreme.SelectedTime;
            DateTime datum = (DateTime)OdabranDatum.SelectedDate;
            TimeSpan s = sat.Subtract(datum);
            DateTime nov = datum.AddHours(s.Hours).AddMinutes(s.Minutes);
            DoctorDTO doctor = userController.GetDoctorByID(int.Parse(idDOktora.Text));
            appointmentDTO.DateTime = nov;

            if (Pomoc.da)
            {
                Pomoc.da = false;
                appointmentController.ModifyAppointment(appointmentDTO);
                return;

            }
            ;
            if (tipusername.Text.Equals("nema"))
            {
                PersonDTO personDTO = secretaryController.GetPersonByID(int.Parse(idpacijenta.Text));
                PatientDTO patient = new PatientDTO();
                patient.ID = -personDTO.ID;
                patient.FirstName = personDTO.FirstName;
                patient.LastName = personDTO.LastName;
                patient.Password = "nema";
                patient.TelephoneNumber = personDTO.TelephoneNumber;
                patient.Username = "nema";
                patient.Adress = personDTO.Adress;
                patient.JMBG = personDTO.JMBG;
                appointmentDTO.PatientDTO = patient;
            }
            else
            {
                PatientDTO patient = employeeContoller.GetPatientByID(int.Parse(idpacijenta.Text));
                appointmentDTO.PatientDTO = patient;
            }

            appointmentDTO.Duration = 30;
            appointmentDTO.DoctorDTO = doctor;
            appointmentDTO.ID = 0;
            appointmentController.ScheduleAppointment(appointmentDTO);
            /*
            SadasnjiTermin.DatumVreme = nov;
            SadasnjiTermin.id = (Termini.Count()+1).ToString();
            Termini.Add(SadasnjiTermin);
            */
            grid.Children.Clear();
            grid.Children.Add(new IzmenaTermina(grid,tekstPomoc));
        }
        
        private void OperacijaButton(object sender, RoutedEventArgs e)
        {

            naslov.Text = "Zakazivanje operacije";
            appointmentDTO.AppointmentType = Bolnica.Model.AppointmentType.Operation;
            radiobatni.Visibility = Visibility.Collapsed;
            Batoni.Visibility = Visibility.Visible;
            suma += 1000;
        }

        private void PregledaButton(object sender, RoutedEventArgs e)
        {

            naslov.Text = "Zakazivanje pregleda";
            appointmentDTO.AppointmentType = Bolnica.Model.AppointmentType.Examination;
            radiobatni.Visibility = Visibility.Collapsed;
            Batoni.Visibility = Visibility.Visible;
            suma += 1000;
        }

        private void Doktor_KeyUp(object sender, KeyEventArgs e)
        {
            var filtered = Lekari.Where(Lekar => Lekar.ImePrezime.StartsWith(doktorImeiPrezime.Text));
            dataGridLekar.ItemsSource = filtered;
        }

        private void JMBG_KeyUp(object sender, KeyEventArgs e)
        {
            var filtered = Pacijenti.Where(Pacijent => Pacijent.JMBG.StartsWith(jmbg.Text));
            dataGridPacijenti.ItemsSource = filtered;
        }

        private void Pacijent_KeyUp(object sender, KeyEventArgs e)
        {
            var filtered = Pacijenti.Where(Pacijent => Pacijent.ImePrezime.StartsWith(pacijentName.Text));
            dataGridPacijenti.ItemsSource = filtered;
        }
        private void Odustani(object sender, RoutedEventArgs e)
        {
            grid.Children.Clear();
            grid.Children.Add(new ZakazivanjePregleda(grid, tekstPomoc));
        }


        public class Lekar
        {
            public string id { get; set; }
            public string Ime { get; set; }
            public string Prezime { get; set; }

            public string ImePrezime { get; set; }

            public string Specijalnost { get; set; }

        }
    }
}

﻿using Bolnica.Controller;
using Bolnica.Model.ModelDTO;
using System.Windows;
using System.Windows.Controls;

namespace Sekretar
{
    /// <summary>
    /// Interaction logic for PrikazJednogTermina.xaml
    /// </summary>
    public partial class PrikazJednogTermina : UserControl
    {
        private AppointmentController appointmentController = new AppointmentController();
        Termin termin = new Termin();
        private AppointmentDTO appointmentDTO = new AppointmentDTO();
        private Grid grid;
        private TextBlock tekstPomoc;
        public PrikazJednogTermina(Grid gridPanel, TextBlock tekst)
        {
            this.grid = gridPanel;
            this.tekstPomoc = tekst;
            InitializeComponent();
            appointmentDTO = appointmentController.GetAppointmentByID(Pomoc.kljucic);
            
            TipTermina.Text ="Tip termina: " +  appointmentDTO.AppointmentType.ToString();
            Datum.Text = "Datum i vreme: " + appointmentDTO.DateTime.ToString();
            Doktor.Text = "Doktor: " + appointmentDTO.DoctorDTO.FirstName + " " +appointmentDTO.DoctorDTO.LastName;
            Pacijent.Text = "Pacijent: " + appointmentDTO.PatientDTO.FirstName + " " + appointmentDTO.PatientDTO.LastName;
            trajanje.Text = "Trajanje: " + appointmentDTO.Duration + "min";
        }

        private void Otkazi(object sender, RoutedEventArgs e)
        {
            bool? Result = new popup("Potvrdite brisanje termina", MessageType.Confirmation, MessageButtons.YesNo).ShowDialog();

            if (Result.Value)

            {
                appointmentController.CancelAppointment(appointmentDTO);
                grid.Children.Clear();
                grid.Children.Add(new IzmenaTermina(grid,tekstPomoc));
            }
        }

        private void Back(object sender, RoutedEventArgs e)
        {
            grid.Children.Clear();
            grid.Children.Add(new IzmenaTermina(grid, tekstPomoc));
        }

        private void Edit(object sender, RoutedEventArgs e)
        {
            Pomoc.da = true;
            Pomoc.kljucic = appointmentDTO.ID;
            grid.Children.Clear();
            grid.Children.Add(new ZakazivanjePregleda(grid,tekstPomoc));
        }
    }
}

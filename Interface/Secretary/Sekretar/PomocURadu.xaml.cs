﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sekretar
{
    /// <summary>
    /// Interaction logic for PomocURadu.xaml
    /// </summary>
    public partial class PomocURadu : UserControl
    {
        private TextBlock tekstPomoc;
        public PomocURadu(TextBlock tekstPomoc)
        {
            InitializeComponent();
           this.tekstPomoc = tekstPomoc;
            this.tekstPomoc.Text = "Pozovite nas ili pokrenite video za pomoć";
            if (tekstPomoc.IsVisible)
            {
                help.Text = "Isključivanje pomoći";

            }
            else
            {
                help.Text = "Ukluči pomoć";

            }
        }

        private void PomocUI(object sender, RoutedEventArgs e)
        {
            if(help.Text.Equals("Isključivanje pomoći"))
            {
                help.Text = "Ukluči pomoć";
                tekstPomoc.Visibility = Visibility.Hidden;
            }
            else
            {
                help.Text = "Isključivanje pomoći";
                tekstPomoc.Visibility = Visibility.Visible;
            }
        }

        private void Video(object sender, RoutedEventArgs e)
        {
            Wizard wizard = new Wizard();
            wizard.Show();
        }
    }
}

﻿using Bolnica.Controller;
using Bolnica.Model.ModelDTO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Sekretar
{
    /// <summary>
    /// Interaction logic for IzmenaTermina.xaml
    /// </summary>
    public partial class IzmenaTermina : UserControl
    {
        private AppointmentController appointmentController = new AppointmentController();
        public ObservableCollection<Termin> Termini { get; set; }


        private Grid grid;
        private TextBlock tekstPomoc;
        public IzmenaTermina(Grid gridPanel,TextBlock tekstPomoc)
        {
            Termini = new ObservableCollection<Termin>();
            List<AppointmentDTO> list = appointmentController.ShowScheduledAppointments(Pomoc.user);
            foreach(AppointmentDTO a in list)
            {
                Termin t = new Termin { DatumVreme = a.DateTime, duzinaTermina = a.Duration.ToString()+"min", id = a.ID.ToString(), TipTermina = a.AppointmentType.ToString(), ImeiPrezimeLekara = a.DoctorDTO.FirstName + " " + a.DoctorDTO.LastName, ImeiPrezimePacijenta = a.PatientDTO.FirstName + " " + a.PatientDTO.LastName };
                Termini.Add(t);
            }
            DateTime dateTime = new DateTime(2020, 06, 06);
            List<AppointmentDTO> lista = appointmentController.GetFreeAppointments(dateTime);
            foreach (AppointmentDTO l in lista)
                Console.WriteLine("{0} {1}", l.DateTime, l.DoctorDTO.FirstName);
            InitializeComponent();
            dataGridPacijenti.ItemsSource = Termini;

            this.grid = gridPanel;
            this.tekstPomoc = tekstPomoc;
            /*
            pacijent.Text = filter;
            this.tekstPomoc.Text = "Kliknite na termin koji želite da izmenite";
            if (filter.Equals("") == false)
            {
                naslov.Text = "Prikaz termina [" + filter + "]";
                var filtered = Termini.Where(Termin => Termin.ImeiPrezimePacijenta.StartsWith(pacijent.Text));
                dugmebitno.Visibility = Visibility.Visible;
                dataGridPacijenti.ItemsSource = filtered;
            }
            */
            
        }

        private void dataGridPacijenti_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Pomoc.kljucic =int.Parse(kljuc.Text);
            grid.Children.Clear();
            grid.Children.Add(new PrikazJednogTermina(grid,tekstPomoc));

        }

        private void Pacijent_KeyUp(object sender, KeyEventArgs e)
        {
           var filtered = Termini.Where(Termin => Termin.ImeiPrezimePacijenta.StartsWith(pacijent.Text));

           dataGridPacijenti.ItemsSource = filtered;
        }

        private void Doktor_KeyUp(object sender, KeyEventArgs e)
        {
            var filtered = Termini.Where(Termin => Termin.ImeiPrezimeLekara.StartsWith(doktor.Text));
            dataGridPacijenti.ItemsSource = filtered;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ovaj.Visibility = Visibility.Collapsed;
        }
    }

    public class Termin
    {

        public string id { get; set; }
        public string ImeiPrezimeLekara { get; set; }

        public string ImeiPrezimePacijenta { get; set; }
        public DateTime DatumVreme { get; set; }
        public string duzinaTermina { get; set; }
        public string TipTermina { get; set; }

    }
}

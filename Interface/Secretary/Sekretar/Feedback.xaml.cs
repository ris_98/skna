﻿using Bolnica.Model.ModelDTO;
using Controller;
using System.Windows;
using System.Windows.Controls;

namespace Sekretar
{
    /// <summary>
    /// Interaction logic for Feedback.xaml
    /// </summary>
    public partial class Feedback : UserControl
    {
        UserController userController = new UserController();
        private Grid grid;
        private TextBlock tekstPomoc;

        public Feedback(Grid grid, TextBlock tekst)
        {
            this.grid = grid;
            this.tekstPomoc = tekst;
            InitializeComponent();
            this.tekstPomoc.Text = "Odaberite broj zvezdica i unesite komentar ukoliko želite da biste nas ocenili";
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {
            bool? Result = new popup("Da li želite da odustanete od ocenjivanja", MessageType.Confirmation, MessageButtons.YesNo).ShowDialog();

            if (Result.Value)
            {
                grid.Children.Clear();
                grid.Children.Add(new PregledProfila(grid, tekstPomoc));
            }
        }

        private void Send(object sender, RoutedEventArgs e)
        {
            bool? Result = new popup("Hvala sto ste nas ocenili", MessageType.Success, MessageButtons.Ok).ShowDialog();

            if (Result.Value)
            {
                FeedbackDTO feedbackDTO = new FeedbackDTO(0,ocena.Value,komentar.Text);
                userController.LeaveFeedback(feedbackDTO);
                grid.Children.Clear();
                grid.Children.Add(new PregledProfila(grid, tekstPomoc));
                
            }
        }
    }
}

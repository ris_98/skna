﻿using Bolnica.Controller;
using Bolnica.Model.ModelDTO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;

namespace Sekretar
{
    /// <summary>
    /// Interaction logic for PregledPacijenata.xaml
    /// </summary>




    public partial class PregledPacijenata : UserControl
    {
        private EmployeeContoller employeeContoller = new EmployeeContoller();
        public ObservableCollection<Pacijent> Pacijenti { get; set; }

        private Grid grid;
        private TextBlock tekstPomoc;
        private string id;

        public PregledPacijenata(Grid gridPanel, TextBlock tekst)
        {
            Pacijenti = new ObservableCollection<Pacijent>();
            InitializeComponent();
            List<PatientDTO> patientDTOs =  employeeContoller.ShowPatients();
            foreach(PatientDTO p in patientDTOs)
            {
                Pacijent pac = new Pacijent { brojTelefona = p.TelephoneNumber, id = p.ID.ToString(), pass = p.Password, drzava=p.Adress.Country, email = p.EMail, grad= p.Adress.City, ime = p.FirstName , ImePrezime = p.FirstName + " " + p.LastName, JMBG= p.JMBG, korisnickoIme =p.Username, prezime =p.LastName, tipNaloga= "registrovan" };
                Pacijenti.Add(pac);
            }

            List<PersonDTO> personDTOs= employeeContoller.GetAllPersons();
            foreach (PersonDTO p in personDTOs)
            {
                Pacijent pac = new Pacijent { brojTelefona = p.TelephoneNumber, id = p.ID.ToString(), pass = "nema", drzava = p.Adress.Country, email = "nema", grad = p.Adress.City, ime = p.FirstName, ImePrezime = p.FirstName + " " + p.LastName, JMBG = p.JMBG, korisnickoIme = "nema", prezime = p.LastName, tipNaloga = "guest" };
                Pacijenti.Add(pac);
            }


            dataGridPacijenti.ItemsSource = Pacijenti;



            this.grid = gridPanel;
            this.tekstPomoc = tekst;
            this.tekstPomoc.Text = "Klikom na pacijenta možete pogledati njegove zakazane termine / menjati mu nalog...";
        }

        private void JMBG_KeyUp(object sender, KeyEventArgs e)
        {
            var filtered = Pacijenti.Where(Pacijent => Pacijent.JMBG.StartsWith(JMBG.Text));
            dataGridPacijenti.ItemsSource = filtered;
        }

        private void Ime_KeyUp(object sender, KeyEventArgs e)
        {
            var filtered = Pacijenti.Where(Pacijent => Pacijent.ime.StartsWith(ime.Text));
            dataGridPacijenti.ItemsSource = filtered;
        }

        private void Prezime_KeyUp(object sender, KeyEventArgs e)
        {
            var filtered = Pacijenti.Where(Pacijent => Pacijent.prezime.StartsWith(prezime.Text));
            dataGridPacijenti.ItemsSource = filtered;
        }

        private void dataGridPacijenti_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (nalog.Text.Equals("guest"))
            {
                Pomoc.tip = true;
            }
            grid.Children.Clear();
            grid.Children.Add(new PrikazJednogPacijenta( grid,tekstPomoc,int.Parse(kljuc.Text)));
        }


    }
    public class Pacijent
    {
        public string id { get; set; }
        public string ime { get; set; }
        public string prezime { get; set; }
        public string JMBG { get; set; }
        public string drzava { get; set; }
        public string grad { get; set; }
        public string brojTelefona { get; set; }
        public string korisnickoIme { get; set; }
        public string email { get; set; }
        public string pass { get; set; }
        public string ImePrezime { get; set; }


        public string tipNaloga { get; set; }

    }
}

﻿using Bolnica.Controller;
using Bolnica.Model.ModelDTO;
using HandyControl.Tools.Extension;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Sekretar
{
    /// <summary>
    /// Interaction logic for DodavanjePacijenta.xaml
    /// </summary>
    public partial class DodavanjePacijenta : UserControl
    {
        private SecretaryController secretaryController = new SecretaryController();
        
        private TextBlock tekstPomoc;
        Grid GridPanel;
        

        public DodavanjePacijenta(TextBlock tekst,  Grid g)
        {

            InitializeComponent();
            this.GridPanel = g;
            this.tekstPomoc = tekst;
            tekstPomoc.Text = "Opcijom NAPRAVI KORISNIČKI NALOG dobijate nova polja potreba za pravljenje trajnog korisničkog naloga";          
        
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (nalog.IsVisible)
                nalog.Collapse();
            else
                nalog.Show();
        }

        private void DodajPacijenta(object sender, RoutedEventArgs e)
        {
            bool greska = false;
            string tipN = "guest";
            //IME
            if (ime.Text.Equals(""))
            {
                greska = true;
                ime.BorderBrush = Brushes.Red;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(ime, "Polje ime je prazno");
            }
            else
            {
                ime.BorderBrush = Brushes.Green;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(ime, "");

            }
            //PREZIME
            if (prezime.Text.Equals(""))
            {
                greska = true;
                prezime.BorderBrush = Brushes.Red;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(prezime, "Polje prezime je prazno");
            }
            else
            {
                prezime.BorderBrush = Brushes.Green;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(prezime, "");

            }
            //JMBG
            if (jmbg.Text.Equals(""))
            {
                greska = true;
                jmbg.BorderBrush = Brushes.Red;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(jmbg, "Polje JMBG je prazno");
            }
            else if(jmbg.Text.Length != 13)
            {
                greska = true;
                jmbg.BorderBrush = Brushes.Red;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(jmbg,"Pogresan broj cifara JMBGa");
            }
            else
            {
                jmbg.BorderBrush = Brushes.Green;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(jmbg, "");

            }
            //GRAD
            if (grad.Text.Equals(""))
            {
                greska = true;
                grad.BorderBrush = Brushes.Red;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(grad, "Polje grad je prazno");
            }
            else
            {
                grad.BorderBrush = Brushes.Green;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(grad, "");

            }
            //DRZAVA
            if (drzava.Text.Equals(""))
            {
                greska = true;
                drzava.BorderBrush = Brushes.Red;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(drzava, "Polje drzava je prazno");
            }
            else
            {
                drzava.BorderBrush = Brushes.Green;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(drzava, "");

            }
            //TELEFON
            if (brtel.Text.Equals(""))
            {
                greska = true;
                brtel.BorderBrush = Brushes.Red;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(brtel, "Polje broj telefona je prazno");
            }
            else if (!brtel.Text.StartsWith("+3816"))
            {
                greska = true;
                brtel.BorderBrush = Brushes.Red;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(brtel, "Broj telefona mora da pocinje sa +3816");
            }
            else if(brtel.Text.Length != 13)
            {
                greska = true;
                brtel.BorderBrush = Brushes.Red;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(brtel, "Broj nije validan");
            }
            else
            {
                brtel.BorderBrush = Brushes.Green;
                MaterialDesignThemes.Wpf.HintAssist.SetHelperText(brtel, "");
            }


            //NALOG
            if (nalog.IsVisible)
            {
                tipN = "registrovan";

                if (korime.Text.Equals(""))
                {
                    korime.BorderBrush = Brushes.Red;
                    greska = true;
                    MaterialDesignThemes.Wpf.HintAssist.SetHelperText(korime, "Polje korisnicko ime je prazno");

                }
                else
                {
                    korime.BorderBrush = Brushes.Green;
                    MaterialDesignThemes.Wpf.HintAssist.SetHelperText(korime, "");

                }

                if (sifra.Password.Equals(potvrdaSifre.Password) == false || sifra.Password.Equals(""))
                {
                    potvrdaSifre.BorderBrush = Brushes.Red;
                    sifra.BorderBrush = Brushes.Red;
                    MaterialDesignThemes.Wpf.HintAssist.SetHelperText(sifra, "Greska pri unosu sifre");
                    greska = true;
                }
                else
                {
                    sifra.BorderBrush = Brushes.Green;
                    potvrdaSifre.BorderBrush = Brushes.Green;
                    MaterialDesignThemes.Wpf.HintAssist.SetHelperText(sifra, "");

                }

                if (email.Text.Equals(""))
                {
                    email.BorderBrush = Brushes.Red;
                    greska = true;
                    MaterialDesignThemes.Wpf.HintAssist.SetHelperText(email, "Polje email je prazno");

                }
                else
                {
                    email.BorderBrush = Brushes.Green;
                    MaterialDesignThemes.Wpf.HintAssist.SetHelperText(email, "");

                }
            }

            if (greska)
                return;
            
            AdressDTO adressDTO = new AdressDTO(0,drzava.Text, grad.Text, "15000", "Ulica");

            if (tipN.Equals("registrovan"))
            {
                PatientDTO patientDTO = new PatientDTO(email.Text, korime.Text, sifra.Password, 0, ime.Text, prezime.Text, brtel.Text, jmbg.Text, adressDTO);
                secretaryController.RegisterUser(patientDTO);
            }
            else
            {
                PersonDTO personDTO = new PersonDTO(0, ime.Text, prezime.Text, brtel.Text, jmbg.Text, adressDTO);
                secretaryController.RegisterPerson(personDTO);
            
            }
            GridPanel.Children.Clear();
            GridPanel.Children.Add(new PregledPacijenata(GridPanel,tekstPomoc));

        }

        private void Odustani(object sender, RoutedEventArgs e)
        {
            bool? Result = new popup("Da li želite da odustanete od pravljenja naloga?", MessageType.Confirmation, MessageButtons.YesNo).ShowDialog();
            if (Result.Value)
            {
                GridPanel.Children.Clear();
                GridPanel.Children.Add(new PregledProfila(GridPanel, tekstPomoc));
            }
        }
    }
}

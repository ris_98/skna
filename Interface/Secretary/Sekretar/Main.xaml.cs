﻿using Bolnica.Model;
using Bolnica.Model.ModelDTO;
using System.Windows;
using System.Windows.Controls;

namespace Sekretar
{
   
    public partial class MainWindow : Window
    {
        

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;


            tekstPomoc.Text = "Ukoliko Vam je potrebna pomoć potražite je na kartici POMOĆ U RADU";

        }

        private void ListViewMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = ListViewMenu.SelectedIndex;
            MoveCorsorMenu(index);
            switch (index)
            {
                case 0:
                    GridPanel.Children.Clear();
                    GridPanel.Children.Add(new PregledProfila(GridPanel, tekstPomoc));
                    break;
                case 1:
                    GridPanel.Children.Clear();
                    GridPanel.Children.Add(new DodavanjePacijenta(tekstPomoc,GridPanel));
                    break;
                case 2:
                    GridPanel.Children.Clear();
                    GridPanel.Children.Add(new PregledPacijenata(GridPanel, tekstPomoc));
                    break;
                case 3:
                    GridPanel.Children.Clear();
                    GridPanel.Children.Add(new ZakazivanjePregleda(GridPanel,tekstPomoc));
                    break;
                case 4:
                    GridPanel.Children.Clear();
                    GridPanel.Children.Add(new IzmenaTermina(GridPanel, tekstPomoc));
                    break;
                case 5:
                    GridPanel.Children.Clear();
                    GridPanel.Children.Add(new Izvestaj(tekstPomoc));
                    break;
                case 6:
                    GridPanel.Children.Clear();
                    GridPanel.Children.Add(new QA(GridPanel,tekstPomoc));
                    break;
                case 7:
                    GridPanel.Children.Clear();
                    GridPanel.Children.Add(new PomocURadu(tekstPomoc));
                    break;
                case 8:
                    GridPanel.Children.Clear();
                    GridPanel.Children.Add(new PrijavaProblema(GridPanel, tekstPomoc));
                    break;
                case 9:
                    GridPanel.Children.Clear();
                    GridPanel.Children.Add(new Feedback(GridPanel, tekstPomoc));
                    break;
                case 10:
                    Application.Current.Shutdown();
                    break;
                default:
                    break;
            }
        }

        public void MoveCorsorMenu(int index)
        {
            TrainsitionigContentSlide.OnApplyTemplate();
            GridCorsor.Margin = new Thickness(0, (50 + 60 * index), 0, 0);
        }
    }

}

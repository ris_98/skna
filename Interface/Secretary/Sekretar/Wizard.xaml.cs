﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sekretar
{
    /// <summary>
    /// Interaction logic for Wizard.xaml
    /// </summary>
    public partial class Wizard : Window
    {
        int slajd;
        public Wizard()
        {
            InitializeComponent();
            this.slajd = 0;
        }
        
        private void Prethodna(object sender, RoutedEventArgs e)
        {
            switch (slajd)
            {
                case 0:

                case 1:
                    slika1.Visibility = Visibility.Collapsed;
                    slika0.Visibility = Visibility.Visible;
                    GridCorsor.Width = 114 * slajd;
                    break;
                case 2:
                    slika2.Visibility = Visibility.Collapsed;
                    slika1.Visibility = Visibility.Visible;
                    GridCorsor.Width = 114 * slajd;
                    slajd--;
                    break;
                case 3:
                    slika3.Visibility = Visibility.Collapsed;
                    slika2.Visibility = Visibility.Visible;
                    GridCorsor.Width = 114 * slajd;
                    slajd--;
                    break;
                case 4:
                    slika4.Visibility = Visibility.Collapsed;
                    slika3.Visibility = Visibility.Visible;
                    GridCorsor.Width = 114 * slajd;
                    slajd--;
                    break;
                case 5:
                    slika5.Visibility = Visibility.Collapsed;
                    slika4.Visibility = Visibility.Visible;
                    GridCorsor.Width = 114 * slajd;
                    slajd--;
                    break;
                case 6:
                    slika6.Visibility = Visibility.Collapsed;
                    slika5.Visibility = Visibility.Visible;
                    GridCorsor.Width = 114 * slajd;
                    slajd--;
                    break;
                case 7:
                    slika7.Visibility = Visibility.Collapsed;
                    slika6.Visibility = Visibility.Visible;
                    GridCorsor.Width = 114 * slajd;
                    slajd--;
                    break;
                case 8:
                    slika8.Visibility = Visibility.Collapsed;
                    slika7.Visibility = Visibility.Visible;
                    GridCorsor.Width = 114 * slajd;
                    slajd--;
                    break;
                default:
                    break;
            }
        }
        private void Sledeca(object sender, RoutedEventArgs e)
        {
            switch (slajd)
            {
                case 0:
                    slika0.Visibility = Visibility.Collapsed;
                    slika1.Visibility = Visibility.Visible;
                    slajd++;
                    GridCorsor.Width = 114 * slajd + 114;
                break;
                case 1:
                    slika1.Visibility = Visibility.Collapsed;
                    slika2.Visibility = Visibility.Visible;
                    slajd++;
                    GridCorsor.Width = 114 * slajd + 114;
                    break;
                case 2:
                    slika2.Visibility = Visibility.Collapsed;
                    slika3.Visibility = Visibility.Visible;
                    slajd++;
                    GridCorsor.Width = 114 * slajd + 114;
                    break;
                case 3:
                    slika3.Visibility = Visibility.Collapsed;
                    slika4.Visibility = Visibility.Visible;
                    slajd++;
                    GridCorsor.Width = 114 * slajd + 114;
                    break;
                case 4:
                    slika4.Visibility = Visibility.Collapsed;
                    slika5.Visibility = Visibility.Visible;
                    slajd++;
                    GridCorsor.Width = 114 * slajd + 114;
                    break;
                case 5:
                    slika5.Visibility = Visibility.Collapsed;
                    slika6.Visibility = Visibility.Visible;
                    slajd++;
                    GridCorsor.Width = 114 * slajd + 114;
                    break;
                case 6:
                    slika6.Visibility = Visibility.Collapsed;
                    slika7.Visibility = Visibility.Visible;
                    slajd++;
                    GridCorsor.Width = 114 * slajd + 114;
                    break;
                case 7:
                    slika7.Visibility = Visibility.Collapsed;
                    slika8.Visibility = Visibility.Visible;
                    slajd++;
                    GridCorsor.Width = 114 * slajd+114;
                    break;
                case 8:
            break;
            default:
                    break;
        }
    }
        private void Exit(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        
    }
}

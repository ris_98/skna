﻿using Bolnca.Controller;
using Bolnica.Model.ModelDTO;
using System;
using System.Windows;
using System.Windows.Controls;
using static Sekretar.QA;

namespace Sekretar
{
    /// <summary>
    /// Interaction logic for PrikazJednogPitanja.xaml
    /// </summary>
    public partial class PrikazJednogPitanja : UserControl
    {
        QuestionController questionController = new QuestionController();

        private Grid GridPanel;
        private TextBlock tekstPomoc;
        private QuestionDTO questionDTO;

        public PrikazJednogPitanja( Grid GridPanel, TextBlock tekstPomoc, int id)
        {
            this.GridPanel = GridPanel;
            questionDTO = questionController.GetQuestionByID(id);
            Pitanje pitanje = new Pitanje { id = questionDTO.ID.ToString(), odgovreno = "DA", naslov = "Naslov pitanja", pitanje = questionDTO.QuestionContent, odgovor = questionDTO.QuestionAnswer, imeprezimeP = questionDTO.Patient.FirstName + " " + questionDTO.Patient.LastName };
            if (questionDTO.QuestionAnswer.Equals(""))
                pitanje.odgovreno = "Ne";

            InitializeComponent();
            
            ajdi.Text ="ID pitanja: " + pitanje.id;
            naslov.Text = "Naslov: " + pitanje.naslov;
            ipp.Text = "Ime i prezime pacijenta: " + pitanje.imeprezimeP;
            pitanjee.Text = "Pitanje: " + pitanje.pitanje;
            if (pitanje.odgovreno.Equals("Ne")){
                dugme.Visibility = Visibility.Visible;
            }
            else
            {
                odgovor.Text = "Odgovor: " + pitanje.odgovor;

            }
        }

        private void Obrisi(object sender, RoutedEventArgs e)
        {
            bool? Result = new popup("Potvrdite brisanje pitanja", MessageType.Confirmation, MessageButtons.YesNo).ShowDialog();

            if (Result.Value)

            {
                questionController.DeleteQuestion(questionDTO);
                GridPanel.Children.Clear();
                GridPanel.Children.Add(new QA(GridPanel, tekstPomoc));
            }
        }

        private void Back(object sender, RoutedEventArgs e)
        {
            
            GridPanel.Children.Clear();
            GridPanel.Children.Add(new QA(GridPanel, tekstPomoc));
            
        }

        private void Odgovori(object sender, RoutedEventArgs e)
        {
            if(odgovornapitanje.Visibility == Visibility.Collapsed)
            {
                odgovornapitanje.Visibility = Visibility.Visible;
                imeDugmeta.Text = "Potvrdi odgovor";
            }
            else
            {
                questionDTO.QuestionAnswer = odgovornapitanje.Text.ToString();
                questionController.AnswerQuestion(questionDTO);
                dugme.Visibility = Visibility.Collapsed;
                odgovornapitanje.Visibility = Visibility.Collapsed;

            }
        }
    }
}

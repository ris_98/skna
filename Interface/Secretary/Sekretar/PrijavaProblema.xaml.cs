﻿
using Bolnica.Model.ModelDTO;
using Controller;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace Sekretar
{
    /// <summary>
    /// Interaction logic for PrijavaProblema.xaml
    /// </summary>
    public partial class PrijavaProblema : UserControl
    {
        UserController userController = new UserController();
        private Grid grid;
        private TextBlock tekstPomoc;

        public PrijavaProblema(Grid grid, TextBlock tekst)
        {
            this.grid = grid;
            this.tekstPomoc = tekst;
            InitializeComponent();
            this.tekstPomoc.Text = "Opisite problem što detaljnije ili nas nazovite direktno preko kartice POMOĆ U RADU";
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {
            bool? Result = new popup("Da li želite da odustanete od prijave problema", MessageType.Confirmation, MessageButtons.YesNo).ShowDialog();

            if (Result.Value)
            {
                grid.Children.Clear();
                grid.Children.Add(new PregledProfila(grid, tekstPomoc));
            }
        }

        private void Send(object sender, RoutedEventArgs e)
        {
            bool? Result = new popup("Hvala sto ste prijavili problem", MessageType.Success, MessageButtons.Ok).ShowDialog();

            if (Result.Value)
            {
                ReportDTO reportDTO = new ReportDTO(0, tekst.Text);
                userController.LeaveBugReport(reportDTO);
                grid.Children.Clear();
                grid.Children.Add(new PregledProfila(grid, tekstPomoc));

            }
        }
    }
}

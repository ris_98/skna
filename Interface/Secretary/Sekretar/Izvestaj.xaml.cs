﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using System.ComponentModel;
using System.Drawing;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows;
using System.Reflection.Metadata;
using Syncfusion.Pdf.Grid;
using System.Data;
using System.Collections.ObjectModel;
using Bolnica.Repository;
using Bolnica.Controller;
using Bolnica.Model.ModelDTO;

namespace Sekretar
{
    /// <summary>
    /// Interaction logic for Izvestaj.xaml
    /// </summary>
    public partial class Izvestaj : UserControl
    {
        AppointmentController appointmentController = new AppointmentController();
        private TextBlock tekstPomoc;
        public ObservableCollection<Termin> Termini { get; set; }

        public Izvestaj(TextBlock tekst)
        {
            this.tekstPomoc = tekst;
            Termini = new ObservableCollection<Termin>();
            tekstPomoc.Text = "Nemojte zaboraviti da označite da li želite izveštaje o pregledima ili operacijama ili oba";
            InitializeComponent();
            List<AppointmentDTO> list = appointmentController.ShowScheduledAppointments(Pomoc.user);
            foreach (AppointmentDTO a in list)
            {
                Termin t = new Termin { DatumVreme = a.DateTime, duzinaTermina = a.Duration.ToString() + "min", id = a.ID.ToString(), TipTermina = a.AppointmentType.ToString(), ImeiPrezimeLekara = a.DoctorDTO.FirstName + " " + a.DoctorDTO.LastName, ImeiPrezimePacijenta = a.PatientDTO.FirstName + " " + a.PatientDTO.LastName };
                Termini.Add(t);
            }
        }
        
        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
          
            //Create a new PDF document
            using (PdfDocument document = new PdfDocument())
  
                obavestenje.Visibility = Visibility.Collapsed;

                PdfDocument doc = new PdfDocument();
                //Add a page.
                PdfPage page = doc.Pages.Add();
                //Create a PdfGrid.
                PdfGrid pdfGrid = new PdfGrid();
                //Create a DataTable.
                DataTable dataTable = new DataTable();
                //Add columns to the DataTable
                dataTable.Columns.Add("ID termina");
                dataTable.Columns.Add("Ime i prezime doktora");
                dataTable.Columns.Add("Ime i prezime pacijenta");
                dataTable.Columns.Add("Datum i vreme");
                dataTable.Columns.Add("Tip termina");
                //Add rows to the DataTable.

                if (Pocetak.SelectedDate == null)
                {
                    obavestenje.Text = "Odaberite datum ";
                    obavestenje.Visibility = Visibility.Visible;
                    return;

                }
                if (Kraj.SelectedDate == null)
                {
                    obavestenje.Text = "Odaberite datum ";
                    obavestenje.Visibility = Visibility.Visible;
                    return;

                }


                List < Termin > termini = Termini.ToList();


                foreach (Termin t in termini)
                {
                    if ( (bool) CheckOperacije.IsChecked)
                    {
                        if((bool)CheckPregledi.IsChecked)
                        {
                            int r1 = DateTime.Compare((DateTime)Pocetak.SelectedDate, t.DatumVreme);
                            int r2 = DateTime.Compare(t.DatumVreme, (DateTime)Kraj.SelectedDate);
                            Console.WriteLine("{0} {0}", r1, r2);
                            if (r1 < 0)
                            {
                                if (r2 < 0)

                                {
                                    dataTable.Rows.Add(new object[] { t.id, t.ImeiPrezimeLekara, t.ImeiPrezimePacijenta, t.DatumVreme, t.TipTermina });
                                }

                            }

                        }
                        else
                        {
                            if (t.TipTermina.Equals("operacija"))
                            {
                                int r1 = DateTime.Compare((DateTime)Pocetak.SelectedDate, t.DatumVreme);
                                int r2 = DateTime.Compare(t.DatumVreme, (DateTime)Kraj.SelectedDate);
                                Console.WriteLine("{0} {0}", r1, r2);
                                if (r1 < 0)
                                {
                                    if (r2 < 0)

                                    {
                                        dataTable.Rows.Add(new object[] { t.id, t.ImeiPrezimeLekara, t.ImeiPrezimePacijenta, t.DatumVreme, t.TipTermina });
                                    }

                                }

                            }
                        }


                    }
                    else
                    {
                        if ((bool)CheckPregledi.IsChecked)
                        {
                            if (t.TipTermina.Equals("pregled"))
                            {

                                int r1 = DateTime.Compare((DateTime)Pocetak.SelectedDate, t.DatumVreme);
                                int r2 = DateTime.Compare(t.DatumVreme, (DateTime)Kraj.SelectedDate);
                                Console.WriteLine("{0} {0}", r1, r2);
                                if (r1 < 0)
                                {
                                    if (r2 < 0)

                                    {
                                        dataTable.Rows.Add(new object[] { t.id, t.ImeiPrezimeLekara, t.ImeiPrezimePacijenta, t.DatumVreme, t.TipTermina });
                                    }

                                }
                            }
                            

                        }
                    }

                }





                //Assign data source.
                pdfGrid.DataSource = dataTable;
                //Draw grid to the page of PDF document.
                pdfGrid.Draw(page, new PointF(10, 10));
                //Save the document.
                doc.Save("Output.pdf");
                //close the document
                doc.Close(true);

                //Launching the Excel file using the default Application.[MS Excel Or Free ExcelViewer]
                System.Diagnostics.Process.Start("Output.pdf");


        
        }

}
    }


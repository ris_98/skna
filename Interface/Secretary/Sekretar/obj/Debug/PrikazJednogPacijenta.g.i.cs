﻿#pragma checksum "..\..\PrikazJednogPacijenta.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "59DDC8E0180F62DBE3A3A0B1B0E65F0A104A1A3A70A4C4F14F395134D9AB2B67"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using Sekretar;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Sekretar {
    
    
    /// <summary>
    /// PrikazJednogPacijenta
    /// </summary>
    public partial class PrikazJednogPacijenta : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 13 "..\..\PrikazJednogPacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MaterialDesignThemes.Wpf.Transitions.TransitioningContent TrainsitionigContentSlide;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\PrikazJednogPacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Ime;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\PrikazJednogPacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Prezime;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\PrikazJednogPacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock JMBG;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\PrikazJednogPacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Drzava;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\PrikazJednogPacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Grad;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\PrikazJednogPacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Brojtel;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\PrikazJednogPacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock KorisnickoIme;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\PrikazJednogPacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock email;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\PrikazJednogPacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MaterialDesignThemes.Wpf.PackIcon ikonicaDugme;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\PrikazJednogPacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock textDugme;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Sekretar;component/prikazjednogpacijenta.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\PrikazJednogPacijenta.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.TrainsitionigContentSlide = ((MaterialDesignThemes.Wpf.Transitions.TransitioningContent)(target));
            return;
            case 2:
            this.Ime = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.Prezime = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.JMBG = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.Drzava = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.Grad = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.Brojtel = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.KorisnickoIme = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 9:
            this.email = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 10:
            
            #line 27 "..\..\PrikazJednogPacijenta.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.PrikazTermina);
            
            #line default
            #line hidden
            return;
            case 11:
            
            #line 37 "..\..\PrikazJednogPacijenta.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Izmeni);
            
            #line default
            #line hidden
            return;
            case 12:
            
            #line 44 "..\..\PrikazJednogPacijenta.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Dual);
            
            #line default
            #line hidden
            return;
            case 13:
            this.ikonicaDugme = ((MaterialDesignThemes.Wpf.PackIcon)(target));
            return;
            case 14:
            this.textDugme = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 15:
            
            #line 54 "..\..\PrikazJednogPacijenta.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.ZakaziOperaciju);
            
            #line default
            #line hidden
            return;
            case 16:
            
            #line 61 "..\..\PrikazJednogPacijenta.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.ZakaziPregled);
            
            #line default
            #line hidden
            return;
            case 17:
            
            #line 69 "..\..\PrikazJednogPacijenta.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.SviPacijenti);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}


﻿using Patient.View.Patient;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Telerik.Pivot.Adomd;
using System.Text.RegularExpressions;
using System.ComponentModel;
using System.Security.Cryptography.X509Certificates;
using System.ComponentModel.DataAnnotations;
using System.Windows.Forms;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows;
using Bolnica.Controller;
using Bolnica.Model.ModelDTO;
using Bolnica.Model;

/*public class Pacijent
{
    private string _name;

    public string Name
    {
        get { return _name; }
        set
        {
            _name = value;
            if (String.IsNullOrEmpty(value))
            {
                throw new ApplicationException("Customer name is mandatory.");
            }
        }
    }
}*/

/*public abstract class ValidationRule
{
    public abstract ValidationResult Validate(
        object value,
        CultureInfo culture);
}*/

/*public class LoginProvera
{
    private string _email;

    public string Email
    {
        get { return _email; }
        set { _email = value;
             if (String.IsNullOrEmpty(value))
        {
            throw new ApplicationException("Customer name is mandatory.");
        }
        
        }
    }
}*/



namespace Patient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private PatientController patientController = new PatientController();
        private LogInContoller logInContoller = new LogInContoller();
        public UserDTO userDTO = new UserDTO();
        public AdressDTO adressDTO = new AdressDTO();
        public string Email { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Drzava { get; set; }
        public string Grad { get; set; }
        public string Ulica { get; set; }
        public string Broj { get; set; }
        public string BrojTelefona { get; set; }
        public string Visina { get; set; }
        public string Tezina { get; set; }
        public MainWindow()
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            Login.Visibility = Visibility.Visible;
            Registracija.Visibility = Visibility.Hidden;
            Greska.Visibility = Visibility.Hidden;
            Greska2.Visibility = Visibility.Hidden;
            Greska3.Visibility = Visibility.Hidden;
            this.Title = "Prijava";

            this.DataContext = this;


}
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Title = "Prijava";
            
            UserDTO info = new UserDTO(Username.Text, Pass.Password);
            var logininfo = logInContoller.SignIn(info);
            //if (logininfo == null)
                //return;
            var win = new Window1(logininfo);
            win.Show();
            Close();
            /* if (Username.Text.Equals("Admin") && Pass.Password.Equals("Admin"))
             {
                     var win = new Window1();
                win.Show();
                Close();
             }
             else
             {
                 if (Username.Text.Equals("Admin"))
                 {
                     Login.Visibility = Visibility.Visible;
                     Greska2.Visibility = Visibility;
                 }
                 else
                 {
                     if (Pass.Password.Equals("Admin"))
                     {
                         Login.Visibility = Visibility.Visible;
                         Greska3.Visibility = Visibility;
                     }
                     else {
                         Login.Visibility = Visibility.Visible;
                         Greska.Visibility = Visibility;
                     }
                 }*/
        }
        private void RegistracijaKlik(object sender, RoutedEventArgs e)
        {
            
           
            
            Login.Visibility = Visibility.Collapsed;
            Registracija.Visibility = Visibility.Visible;
            this.Title = "Registracija";


            //Ime.Visibility = Visibility.Visible;
            //Prezime.Visibility = Visibility.Visible;




        }
        private void Prijava(object sender, RoutedEventArgs e)
        {
            Login.Visibility = Visibility.Visible;
            Registracija.Visibility = Visibility.Hidden;
            this.Title = "Prijava";



        }
        private void LoginKlik(object sender, RoutedEventArgs e)
        {
            /*UserDTO info = new UserDTO("brunclik", "sifra");
            var logininfo = logInContoller.SignIn(info);
            if (logininfo == null)
                return;*/

            Login.Visibility = Visibility.Visible;
            Registracija.Visibility = Visibility.Hidden;
            Greska.Visibility = Visibility.Hidden;
            Greska2.Visibility = Visibility.Hidden;
            Greska3.Visibility = Visibility.Hidden;
            this.Title = "Prijava";

        }
        private void LoginKlik2(object sender, RoutedEventArgs e)
        {
            AdressDTO adress = new AdressDTO(1,CountryUser.Text, CityUser.Text, ZipCode.Text, StreetUser.Text);
            UserDTO user = new UserDTO(1,EmailUser.Text, UsernameUser.Text, PasswordUser.Text, NameUser.Text, SurnameUser.Text, PhoneNumberUser.Text, JMBGUser.Text,adress);


            patientController.RegisterPatient(user);
            
           
            Login.Visibility = Visibility.Visible;
            Registracija.Visibility = Visibility.Hidden;
            Greska.Visibility = Visibility.Hidden;
            Greska2.Visibility = Visibility.Hidden;
            Greska3.Visibility = Visibility.Hidden;
            this.Title = "Prijava";

        }
       


        private void UnesiKorisnicko(object sender, RoutedEventArgs e)
        {
           
            UlogujUnos.Visibility = Visibility.Visible;
            UlogujUnos.Focus();
        }

        private void UnesiSifru(object sender, RoutedEventArgs e)
        {
           
            SifraUnos.Visibility = Visibility.Visible;
            SifraUnos.Focus();
        }

        




        /*private void StringCheck(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^a-zA-Z]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private double  _test1;
        /*private double _test2;
        private double _test3;
        public double Test1
        {
            get
            {
                return _test1;
            }
            set
            {
                if (value != _test1)
                {
                    _test1 = value;
                    OnPropertyChanged("Test1");
                }
            }
        }*/









    }


  
    

}

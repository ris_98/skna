﻿using Bolnica.Controller;
using Bolnica.Model.ModelDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Patient.View.Patient
{
    /// <summary>
    /// Interaction logic for Window9.xaml
    /// </summary>
    public partial class Window9 : Window
    {
        private MedicalRecordDTO medicalRecordDTO = new MedicalRecordDTO();
        private MedicalRecordController medicalRecordController = new MedicalRecordController();
        public Window9(int medicalId)
        {
            InitializeComponent();
            medicalRecordDTO = medicalRecordController.GetById(medicalId);
            string ime = medicalRecordDTO.Appointment.DoctorDTO.LastName;
            string prezime = medicalRecordDTO.Appointment.DoctorDTO.LastName;

            InitializeComponent();
            Lekar.Content = "Dr" + " " + medicalRecordDTO.Appointment.DoctorDTO.FirstName + " " + medicalRecordDTO.Appointment.DoctorDTO.LastName;
            Datum.Content = medicalRecordDTO.Appointment.DateTime.ToString();
            Uput.Content = "Potreban nalaz " + medicalRecordDTO.ExaminationReport.LabTestRequisition.Note.ToString(); 
            
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
        }
    }
}

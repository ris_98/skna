﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Patient.View.Patient;

namespace Patient.View.Patient
{
    /// <summary>
    /// Interaction logic for Window5.xaml
    /// </summary>
    public partial class Window5 : Window
    {
       
       // private List<Lekari> lekari2;

       

        internal Window5(ref List<Lekari> lekari)
        {
            
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            Lekari3.ItemsSource = lekari;

            

        }

        private void Potvrdi(object sender, RoutedEventArgs e)
        {

            
            
                Close();
           
           
            
        }
        private void Odustani(object sender, RoutedEventArgs e)
        {
          
                Close();
           
        }
    }
}

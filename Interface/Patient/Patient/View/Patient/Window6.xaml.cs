﻿using Bolnca.Controller;
using Bolnica.Controller;
using Bolnica.Model.ModelDTO;
using Controller;
using System;
using System.Windows;

namespace Patient.View.Patient
{
    /// <summary>
    /// Interaction logic for Window6.xaml
    /// </summary>
    public partial class Window6 : Window
    {
        private QuestionController questionController = new QuestionController();
        private UserController userController = new UserController();
        private PatientController patientController = new PatientController();

        private QuestionDTO question = new QuestionDTO(1, "2");
        private UserDTO user = new UserDTO();
        //private PatientDTO patient = new PatientDTO();


        public Window6(UserDTO userDTO)
        {
            InitializeComponent();
            PatientDTO patient = new PatientDTO(userDTO.EMail, userDTO.Username, userDTO.Password, userDTO.ID, userDTO.FirstName, userDTO.LastName, userDTO.TelephoneNumber, userDTO.JMBG, userDTO.Adress);
            question = new QuestionDTO(10, "", "", patient);

            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
        }

        private void SlanjePitanjaKlik(object sender, RoutedEventArgs e)
        {
            String tekst;
            tekst = pitanje2.Text;
            question.QuestionContent = tekst;

            if (MessageBox.Show("Želite da postavite pitanje?",
            "Potvrda", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {


               
                questionController.AskQuestion(question);
                Close();




            }
            else
            {
                // Do not close the window  
            }
        }

    }
    
}

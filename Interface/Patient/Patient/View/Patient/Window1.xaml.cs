﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Syncfusion.Pdf;
using System.Drawing;
using System.Globalization;
using System.Data;
using Syncfusion.Pdf.Grid;
using Controller;
using Bolnica.Controller;
using Bolnica.Model.ModelDTO;
using Bolnca.Controller;
using System.Windows.Media.Animation;
using Bolnica.Repository;
using Bolnica.Model;

enum krvna_grupa
{
    A,B,AB,O
}

class TextToVisibilityConverter 
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value.ToString() == "someValue")
        {
            return Visibility.Visible;
        }
        return Visibility.Collapsed;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}


class Korisnik
{
    public String Ime{ get; set; }
    public String Prezime { get; set; }
    public String Država { get; set; }
    public String Grad { get; set; }
    public String Ulica { get; set; }
    public String Broj { get; set; }
    public String Broj_telefona { get; set; }
    public krvna_grupa Krvna_grupa { get; set; }
    public int Visina { get; set; }
    public double Tezina { get; set; }
    public string Datum_rođenja { get; set; }
    public string Email { get; set; }
    public string Pol { get; set; }
    public string JMBG { get; set; }


    public Korisnik(String ime, String prezime, String država, String grad, String broj, String brojtelefona, String ulica, krvna_grupa krvna, int vi, double te, string datum,string email2,string pol,string JMBG)
    {
        this.Ime = ime;
        this.Prezime = prezime;
        this.Država = država;
        this.Grad = grad;
        this.Ulica = ulica;
        this.Broj = broj;
        this.Broj_telefona = brojtelefona;
        this.Krvna_grupa = krvna;
        this.Visina = vi;
        this.Tezina = te;
        this.Datum_rođenja = datum;
        this.Pol = pol;
        this.Email = email2;
        this.JMBG = JMBG;
    }
}

class Terapija
{
    public String Ime_leka { get; set; }
    public String Početak_terapije { get; set; }
    public String Kraj_terapije { get; set; }
    public String Način_uzimanja { get; set; }


    public Terapija(String ime, String pocetak, String kraj, String uzimanje)
    {
        this.Ime_leka = ime;
        this.Početak_terapije = pocetak;
        this.Kraj_terapije = kraj;
        this.Način_uzimanja = uzimanje;


    }
}

public class Pomoc {

    public DateTime Datum { get; set; }
}


public class Author
{
    public string Ime_leka { get; set; }
    public DateTime Pocetak_terapije { get; set; }
    public DateTime Kraj_terapije { get; set; }
    public int Nacin_uzimanja_terapije { get; set; }
    public int Sati_do_sledece_terapije { get; set; }
    public int Minuti_do_sledece_terapije { get; set; }
}
class Termini
{

    public DateTime Doktor_termini;
}


class RadnoVreme
{

    public String Pon { get; set; }
    public String Uto { get; set; }
    public String Sre { get; set; }
    public String Cetv { get; set; }
    public String Petak { get; set; }
}




class Lekari
{
    public String Lekar { get; set; }
    public double Ocena { get; set; }
    public RadnoVreme radnoVreme { get; set; }
    public List<Termini> doktortermini { get; set; }
    public int iD { get; set; }


}




class Termin2
{
    public int Id { get; set; }
    public string Lekar { get; set; }
    public DateTime Datum_termina { get; set; }
    public bool zauzet { get; set; }
    public string Vrsta_pregleda { get; set; }
    public DateTime Kraj_termina { get; set; }


}



class Karton
{
    public DateTime Datum_pregleda { get; set; }
    public DateTime Vreme_pregleda { get; set; }
    public String Doktor { get; set; }
    public String Vrsta_pregleda { get; set; }
    
    
    


   
}



class TerminIgranje
{
    public DateTime Datum_pregleda { get; set; }

    public string Doktor { get; set; }
    public String Vrsta_pregleda { get; set; }


}

class Vrsta_pregleda {
    public string Vrsta { get; set; }
}
namespace Patient.View.Patient
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    /// 

    static class Container
    {
        public static List<Termin2> termini = new List<Termin2>();
    }
    static class Container2
    {
        public static List<Author> authors = new List<Author>();
       
    }
    static class Container3
    {

        public static Termin2 dataizabrana;
    }

    static class Container4
    {

        public static List<Author> lekovi2 = new List<Author>();
    }

    public partial class Window1 : Window
    {
        public object KorisnikData { get; }

        private PatientController patientController = new PatientController();
        private MedicationController medicationController = new MedicationController();
        private NotVerifiedMedicationController experimentmedicationController = new NotVerifiedMedicationController();
        private QuestionController questionController = new QuestionController();
        private AppointmentController appointmentController = new AppointmentController();
        //private LabTestRequisitionController labTestRequisitionController = new LabTestRequisitionController();
        private LabTestRequisitionDTO labTestRequisition = new LabTestRequisitionDTO();
        //private DiagnosisController diagnosisController = new DiagnosisController();
        private DiagnosisDTO diagnosisDTO = new DiagnosisDTO();
        //private PrescriptionController prescriptionController = new PrescriptionController();
        private ExaminationReportController examinationReportController = new ExaminationReportController();
        private ExaminationReportDTO examinationReportDTO = new ExaminationReportDTO();
        private UserController userController = new UserController();
        private DoctorController doctorController = new DoctorController();
        private MedicalRecordController medicalRecordController = new MedicalRecordController();
        private DoctorFeedbackDTO doctorFeedbackDTO;
        private PrescriptionDTO prescriptionDTO;
        private MedicalRecordDTO medicalRecordDTO = new MedicalRecordDTO();
        private List<AppointmentDTO> appointment = new List<AppointmentDTO>();
        private List<AppointmentDTO> appointmentfree = new List<AppointmentDTO>();
        private AppointmentDTO appointmentID= new AppointmentDTO();
        private FeedbackDTO feedbackDTO = new FeedbackDTO(1,3,"");
        private ReportDTO reportDTO = new ReportDTO(1, "11");
       

        
        private List<PrescriptionDTO> prescriptions = new List<PrescriptionDTO>();
        private List<QuestionDTO> questions = new List<QuestionDTO>();
        private List<DoctorDTO> doctors = new List<DoctorDTO>();
        private List<DoctorDTO> doctorsSpecialization = new List<DoctorDTO>();
        private List<MedicationDTO> medications = new List<MedicationDTO>();
        private DoctorFeedbackDTO feed=new DoctorFeedbackDTO();
        //private List<ExperimentMedicationDTO> experimentmedications = new List<ExperimentMedicationDTO>();
        //private ExperimentMedicationDTO experimentmedication = new ExperimentMedicationDTO(1, "Bromazepan", "novi lek");


        internal List<Lekari> lekari;
        private UserDTO logininfo;

        public Window1(UserDTO logininfo)
        {
            
            this.logininfo = logininfo;
    
            
            InitializeComponent();
            /*  public int ID;
          public int Rating;
          public int NumberOfRatings;
          public Doctor Doctor;*/
            doctors = userController.GetAllDoctorsBySpecialization("opsti");
            feed = new DoctorFeedbackDTO(1,3,0,doctors[0]);
            Console.WriteLine("{0}", doctors[0].Rating);
            patientController.DoctorFeedback(feed);
            Console.WriteLine("{0}", doctors[0].Rating);

            //experimentmedicationController.SendToVerify(experimentmedication);
            /*experimentmedications = experimentmedicationController.ShowExperimentMedication();
            Console.WriteLine("{0}", experimentmedications[0].Name);*/
            questions = questionController.ShowQuestions();
            medications = medicationController.ShowNotApprovedMedication();
            appointmentID = appointmentController.GetAppointmentByID(1);
            Console.WriteLine("{0}", appointmentID.AppointmentType.ToString());


            List<MedicalRecordDTO>  izvestajilekara = new List<MedicalRecordDTO>();
            izvestajilekara = medicalRecordController.ShowMedicalRecords(logininfo);
            foreach (MedicalRecordDTO m in izvestajilekara){

                Console.WriteLine("{0}", m.ExaminationReport.Prescription.Medication.Name);
            }
            

            //prescriptionDTO = prescriptionController.GetById(1);
            //Console.WriteLine("{0}", prescriptionDTO.Dosage);
            // examinationReportDTO = examinationReportController.GetById(1);
            //Console.WriteLine("{0}", examinationReportDTO.Prescription.Medication.Name);

            //prescriptions = prescriptionController.ShowPrescriptions();

            //Console.WriteLine("{0}", appointment[0].PatientDTO.FirstName);
            //medicalRecordDTO = medicalRecordController.GetById(1);
            //Console.WriteLine("{0}", medicalRecordDTO.ExaminationReport.Prescription.Medication.Name);
            //.WriteLine("{0}", medicalRecordDTO.Appointment.AppointmentType.ToString());


            //medications = medicationController.ShowApprovedMedication();

            appointment = appointmentController.ShowScheduledAppointments(logininfo);

            doctors = userController.GetAllDoctors();
            doctorsSpecialization = userController.GetAllDoctorsBySpecialization("opsti");
            Console.WriteLine("{0}", doctorsSpecialization[0].LastName);



            //Lekarii.ItemsSource = doctors;

            //Console.WriteLine("{0}", doctors.Count());
            //medicationController.VerifyMedication(medications[0], new DoctorDTO());


            proba.Visibility = Visibility.Hidden;
            proba.Visibility = Visibility.Hidden;
            proba2.Visibility = Visibility.Hidden;

             

        Korisnik korisnik = new Korisnik("Zorana", "Stamenković", "Srbija", "Beograd", "2", "0631111111", "Vojvođanska  ", krvna_grupa.A, 154, 32, "5.1.2000.","zorana@gmail.com","Ženski","2112322112211");
            Korisnik kor = new Korisnik(logininfo.FirstName, logininfo.LastName, logininfo.Adress.Country, logininfo.Adress.City, logininfo.Adress.ZipCode, logininfo.TelephoneNumber, logininfo.Adress.Street, krvna_grupa.A, 152, 34, "5.1.2000.", logininfo.EMail, "Ženski", logininfo.JMBG);
            dr.Content= kor.Država;
            ad.Content = kor.Ulica;
            //ad2.Content = kor.Broj;
            visina.Content = kor.Visina;
            krvnag.Content = kor.Krvna_grupa;
            broj.Content = kor.Broj_telefona;
            date.Content = kor.Datum_rođenja;
            tezina.Content = kor.Tezina;
            email.Content = kor.Email;
            pol1.Content = kor.Pol;

            Ime.Text = kor.Ime;
            Ime2.Content = kor.Ime;
            ImeUnos.Text = kor.Ime;
            Prezime.Text = kor.Prezime;
            Prezime2.Content = kor.Prezime;
            PrezimeUnos.Text = kor.Prezime;
            Drzava.Text = kor.Država;
            DrzavaUnos.Text = kor.Država;
            Grad.Text = kor.Grad;
            GradUnos.Text = kor.Grad;
            Ulica.Text = kor.Ulica;
           UlicaUnos.Text = kor.Ulica;
            Broj.Text = kor.Broj;
           BrojUnos.Text = kor.Broj;
            Email.Text = kor.Email;
            EmailUnos.Text = kor.Email;
            JMBG.Text = kor.Broj_telefona;
           JMBGUnos.Text = kor.Broj_telefona;

            LekarChange.Visibility = Visibility.Hidden;

            MenuItem mItem1 = new MenuItem();
           

            mItem1.Name = "MenuItem1";
            
            mItem1.Header = "Vaš operacija je otkazana";
            /*M_Test.Items.Add(mItem1);

            MenuItem mItem2 = new MenuItem();
            mItem2.Name = "MenuItem2";
            mItem2.Header = "Vaš termin  je pomeren za 30 minuta";
            M_Test.Items.Add(mItem2);*/



            Container2.authors.Add(new Author()
            {
                Ime_leka = "Bromazepan",
                Pocetak_terapije = new DateTime(2020,1,1,0,0,0),
                Kraj_terapije = new DateTime(2020, 7, 2,2,2,0),
                Nacin_uzimanja_terapije = 12,
                Sati_do_sledece_terapije = 12
            });
            Container2.authors.Add(new Author()
            {
                Ime_leka = "Amoksicilin",
                Pocetak_terapije = new DateTime(2020, 6, 3,14,15,0),
                Kraj_terapije = new DateTime(2020, 7, 2,8,0,0),
                Nacin_uzimanja_terapije = 8,
                Sati_do_sledece_terapije = 8

            });


            Container2.authors.Add(new Author()
            {
                Ime_leka = "Sinacilin",
                Pocetak_terapije = new DateTime(2020, 6, 1,12,0,0),
                Kraj_terapije = new DateTime(2020, 7, 2,12, 0, 0),
                Nacin_uzimanja_terapije = 8,
                Sati_do_sledece_terapije = 8
            });

            Container2.authors.Add(new Author()
            {
                Ime_leka = "Amoksicilin",
                Pocetak_terapije = new DateTime(2020, 6, 14, 2, 0, 0),
                Kraj_terapije = new DateTime(2020, 7, 2, 12, 0, 0),
                Nacin_uzimanja_terapije = 8,
                Sati_do_sledece_terapije = 8

            });

            /*Container2.authors.Add(new Author()
            {
                Ime_leka = "Brufen",
                Pocetak_terapije = new DateTime(2020, 6, 1, 12, 0, 0),
                Kraj_terapije = new DateTime(2020, 7, 2, 12, 0, 0),
                Nacin_uzimanja_terapije = "Tri puta dnevno na 8h"
            });

            Container2.authors.Add(new Author()
            {
                Ime_leka = "Amoksicilin",
                Pocetak_terapije = new DateTime(2020, 6, 3, 12, 0, 0),
                Kraj_terapije = new DateTime(2020, 7, 2, 12, 0, 0),
                Nacin_uzimanja_terapije = "Tri puta dnevno na 8h"

            });*/

            List<Author> lekovi = new List<Author> ();
           

            foreach (var el in izvestajilekara)
            {
                string inputString = el.ExaminationReport.Prescription.Dosage;
                int input = Int32.Parse(inputString);

                Author lek = new Author { Ime_leka = el.ExaminationReport.Prescription.Medication.Name, Kraj_terapije = el.ExaminationReport.Prescription.ExpirationDate, Pocetak_terapije = el.Appointment.DateTime, Nacin_uzimanja_terapije = input };

                Container4.lekovi2.Add(lek);
            }

            foreach (var lek in Container4.lekovi2) { 
                
                


                if (lek.Pocetak_terapije <= DateTime.Now && lek.Kraj_terapije >= DateTime.Now)
                {
                    int pomoc = lek.Nacin_uzimanja_terapije * 60;
                    int debag = Convert.ToInt32((DateTime.Now - lek.Pocetak_terapije).TotalMinutes);

                    while (debag > pomoc || debag < 0)
                    {

                        debag -= pomoc;

                    }

                    int rezultat = pomoc - debag;

                    lek.Sati_do_sledece_terapije = rezultat;
                    lekovi.Add(lek);
                }

            }


            List<Author> SortedList = lekovi.OrderBy(o => o.Sati_do_sledece_terapije).ToList();

            LekoviData.ItemsSource = SortedList;
            TerapijaData.ItemsSource = Container4.lekovi2;

            List<Author> pomocni = new List<Author>();
            foreach(var el in Container4.lekovi2)
            {
                if (pomocni.Count == 0)
                {
                    pomocni.Add(el);
                }
                else
                {
                    foreach(var el2 in pomocni)
                    {
                        if(el.Ime_leka== el2.Ime_leka)
                        {
                            break;
                        }

                        

                    }
                    
                }
            }
            Terapijaa.ItemsSource = Container4.lekovi2;








            List<Karton> karton= new List<Karton>();
            karton.Add(new Karton()
            {
                Doktor = "Petar Petrovic",
                Datum_pregleda = new DateTime(2020, 5, 2, 12, 45, 0),
                Vreme_pregleda = new DateTime(2020, 5, 2, 13, 45, 0),
                Vrsta_pregleda = "Kontrola"

            });
            karton.Add(new Karton()
            {
                Doktor = "Petar Petrovic",
                Datum_pregleda = new DateTime(2020, 5, 2, 12, 45, 0),
                Vreme_pregleda = new DateTime(2020, 5, 2, 13, 45, 0),
                Vrsta_pregleda = "Kontrola"

            });
            karton.Add(new Karton()
            {
                Doktor = "Petar Petrovic",
                Datum_pregleda = new DateTime(2020, 5, 2, 12, 45, 0),
                Vreme_pregleda = new DateTime(2020, 5, 2, 13, 45, 0),
                Vrsta_pregleda = "Kontrola"

            });
            karton.Add(new Karton()
            {
                Doktor = "Petar Petrovic",
                Datum_pregleda = new DateTime(2020, 5, 2, 12, 45, 0),
                Vreme_pregleda = new DateTime(2020, 5, 2, 13, 45, 0),
                Vrsta_pregleda = "Kontrola"

            });
            karton.Add(new Karton()
            {
                Doktor = "Petar Petrovic",
                Datum_pregleda = new DateTime(2020, 5, 2, 12, 45, 0),
                Vreme_pregleda = new DateTime(2020, 5, 2, 13, 45, 0),
                Vrsta_pregleda = "Kontrola"


            });

            KartonData1.ItemsSource = karton;

            List<Termini> doktor_termini = new List<Termini>();
            doktor_termini.Add(new Termini()
            {
                Doktor_termini = new DateTime(2020, 3, 7, 12, 45, 0),


            });
            doktor_termini.Add(new Termini()
            {
                Doktor_termini = new DateTime(2020, 4, 7, 13, 45, 0),



            });
            doktor_termini.Add(new Termini()
            {
                Doktor_termini = new DateTime(2020, 5, 7, 12, 45, 0),


            });
            doktor_termini.Add(new Termini()
            {
                Doktor_termini = new DateTime(2020, 6, 7, 11, 00, 0),


            });
            doktor_termini.Add(new Termini()
            {
                Doktor_termini = new DateTime(2020, 7, 7, 18, 00, 0),



            });
            doktor_termini.Add(new Termini()
            {
                Doktor_termini = new DateTime(2020, 8, 7, 19, 00, 0),


            });

            //ZakazivanjeData3.ItemsSource = doktor_termini;

            

            List<RadnoVreme> radno = new List<RadnoVreme>();
            radno.Add(new RadnoVreme() { Pon = "8:00-14:00", Uto = "8:00-14:00", Sre = "8:00-14:00", Cetv = "8:00-14:00" , Petak = "8:00-14:00"  });
            radno.Add(new RadnoVreme() { Pon = "14:00-20:00", Uto = "14:00-20:00", Sre = "14:00-20:00", Cetv = "14:00-20:00", Petak = "14:00-20:00" });
            radno.Add(new RadnoVreme() { Pon = "8:00-20:00", Uto = "14:00-20:00", Sre = "14:00-20:00", Cetv = "8:00-20:00", Petak = "14:00-20:00" });

            lekari = new List<Lekari>();
            lekari.Add(new Lekari()
            {
                Lekar = "Petar Petrovic",
                Ocena = 4.5,
                radnoVreme = radno[0],
                //doktortermini = { doktor_termini[0], doktor_termini[1], doktor_termini[2], doktor_termini[3] }


        }) ;
            lekari.Add(new Lekari()
            {
                Lekar = "Petra Ilić",
                Ocena = 4,
                radnoVreme = radno[0],
                //doktortermini = { doktor_termini[0], doktor_termini[1], doktor_termini[2], doktor_termini[3] }

            }) ; 
            lekari.Add(new Lekari()
            {
                Lekar = "Kristijan Savić",
                Ocena = 3.5,
                radnoVreme = radno[1],
                //doktortermini = { doktor_termini[4], doktor_termini[5] }


            });
            lekari.Add(new Lekari()
            {
                Lekar = "Filip Zdelar",
                Ocena = 3.5,
                radnoVreme = radno[2],
                //doktortermini = { doktor_termini[2], doktor_termini[5] }

            });

            /* List<Lekari> = new List<Lekar>();

             foreach (DoctorDTO d in doctors)
             {
                 Lekar l = new Lekar { id = d.ID.ToString(), Ime = d.FirstName, Prezime = d.LastName, Specijalnost = d.Specialization.ToString(), ImePrezime = d.FirstName + " " + d.LastName };
                 Lekari.Add(l);
             }*/

            //Lekari.ItemsSource = lekari;
            List<Lekari> lekarisvi = new List<Lekari>();
            foreach (DoctorDTO d in doctorsSpecialization)
            {
                
                Lekari a = new Lekari { iD = d.ID, Lekar = d.FirstName + " " + d.LastName, Ocena = d.Rating, radnoVreme = radno[0]};
                lekarisvi.Add(a);
            }

            List<Lekari> lekaribezspecijalizacije = new List<Lekari>();
            foreach (DoctorDTO d in doctors)
            {
                Lekari a = new Lekari { iD = d.ID, Lekar = d.FirstName + " " + d.LastName,  Ocena = d.Rating, radnoVreme = radno[0] };
                lekaribezspecijalizacije.Add(a);
            }

            Lekarii.ItemsSource = lekaribezspecijalizacije;
            Lekarii.ItemsSource = lekaribezspecijalizacije;
            LekariNovo2.ItemsSource = lekaribezspecijalizacije;
            Lekar4.ItemsSource = lekaribezspecijalizacije;
            LekariNovo.ItemsSource = lekaribezspecijalizacije;

            
            

            //List<Termin2> termini = new List<Termin2>();
            Container.termini.Add(new Termin2()
            {
                Lekar = lekari[0].Lekar,
                Datum_termina = new DateTime(2020, 3, 7, 12, 45, 0),
                zauzet = true,
                Vrsta_pregleda = "Kontrola"

            }); ;
            Container.termini.Add(new Termin2()
            {
                Lekar = lekari[1].Lekar,
                Datum_termina = new DateTime(2020, 7, 7, 12, 45, 0),
                zauzet = true,
                Vrsta_pregleda = "Pregled"
            });

            Container.termini.Add(new Termin2()
            {
                Lekar = lekari[2].Lekar,
                Datum_termina = new DateTime(2020, 7, 7, 12, 45, 0),
                zauzet = true,
                Vrsta_pregleda = "Operacija"
            });
            Container.termini.Add(new Termin2()
            {
                Lekar = lekari[3].Lekar,
                Datum_termina = new DateTime(2020, 7, 7, 12, 45, 0),
                zauzet = false,
                Vrsta_pregleda = "Pregled"

            });

            Container.termini.Add(new Termin2()
            {
                Lekar = lekari[0].Lekar,
                Datum_termina = new DateTime(2020, 8, 10, 12, 45, 0),
                zauzet = false,
                Vrsta_pregleda = "Operacija"
            });
            Container.termini.Add(new Termin2()
            {
                Lekar = lekari[2].Lekar,
                Datum_termina = new DateTime(2020, 9, 8, 12, 45, 0),
                zauzet = false,
                Vrsta_pregleda = "Operacija"
            });
            Container.termini.Add(new Termin2()
            {
                Lekar = lekari[3].Lekar,
                Datum_termina = new DateTime(2020, 8, 10, 12, 45, 0),
                zauzet = false,
                Vrsta_pregleda = "Pregled"
            });
            Container.termini.Add(new Termin2()
            {
                Lekar = lekari[0].Lekar,
                Datum_termina = new DateTime(2020, 7, 11, 12, 45, 0),
                zauzet = false,
                Vrsta_pregleda = "Pregled"

            });
            Container.termini.Add(new Termin2()
            {
                Lekar = lekari[1].Lekar,
                Datum_termina = new DateTime(2020, 9, 12, 12, 45, 0),
                zauzet = false,
                Vrsta_pregleda = "Kontrola"
            });
            Container.termini.Add(new Termin2()
            {
                Lekar = lekari[2].Lekar,
                Datum_termina = new DateTime(2020, 10, 9, 12, 45, 0),
                zauzet = false,
                Vrsta_pregleda = "Kontrola"
            });
            Container.termini.Add(new Termin2()
            {
                Lekar = lekari[3].Lekar,
                Datum_termina = new DateTime(2020, 3, 2, 12, 45, 0),
                zauzet = true,
                Vrsta_pregleda = "Pregled"
            });
            Container.termini.Add(new Termin2()
            {
                Lekar = lekari[0].Lekar,
                Datum_termina = new DateTime(2020, 3, 1, 12, 45, 0),
                zauzet = true,
                Vrsta_pregleda = "Pregled"
            });
            Container.termini.Add(new Termin2()
            {
                Lekar = lekari[1].Lekar,
                Datum_termina = new DateTime(2020, 3, 2, 12, 45, 0),
                zauzet = true,
                Vrsta_pregleda = "Pregled"
            });
            Container.termini.Add(new Termin2()
            {
                Lekar = lekari[2].Lekar,
                Datum_termina = new DateTime(2020, 3, 3, 12, 45, 0),
                zauzet = true,
                Vrsta_pregleda = "Kontrola"
            });

            List<Termin2> zakazan = new List<Termin2>();


            foreach (var el in Container.termini)
                if (el.zauzet.Equals(true) && el.Datum_termina > DateTime.Now)
                {
                    zakazan.Add(el);
                }
            ZakazivanjeData2.ItemsSource = zakazan;

            List<Termin2> slobodni = new List<Termin2>();
            List<Pomoc> dateTimes = new List<Pomoc>();

            /*foreach (var el in Container.termini)
                if (el.zauzet.Equals(false) && el.Datum_termina > DateTime.Now)
                {
                    dateTimes.Add(el.Datum_termina);
                }
            O.ItemsSource = dateTimes;*/

            foreach (var el in Container.termini)
                if (el.zauzet.Equals(false) && el.Datum_termina > DateTime.Now)
                {
                    slobodni.Add(el);
                }

            List<Termin2> zakazani = new List<Termin2>();
            foreach (var el in appointment) {

                Termin2 novi = new Termin2 {Id=el.ID, Datum_termina = el.DateTime, Vrsta_pregleda = el.AppointmentType.ToString(), Lekar = el.DoctorDTO.FirstName + " " + el.DoctorDTO.LastName };
                    zakazani.Add(novi);
                }
            ZakazivanjeData.ItemsSource = slobodni;
            ZakazivanjeData2.ItemsSource = slobodni;
            TerminProba.ItemsSource = zakazani;
            Container3.dataizabrana = (Termin2)TerminProba.SelectedItem;
            ZakazanData.ItemsSource = zakazani;

            List<Termin2> kartonData = new List<Termin2>();
            foreach (var el in izvestajilekara)
            {
                DateTime kraj = el.Appointment.DateTime.AddMinutes(el.Appointment.Duration);
                Termin2 novi = new Termin2 {Id=el.ID, Lekar =el.Appointment.DoctorDTO.FirstName + " " + el.Appointment.DoctorDTO.LastName, Datum_termina=el.Appointment.DateTime, Vrsta_pregleda=el.Appointment.AppointmentType.ToString(),Kraj_termina=kraj };
                kartonData.Add(novi);
            }
            /*foreach (var el in Container.termini)
                if (el.zauzet.Equals(true) && el.Datum_termina < DateTime.Now)
                {
                    el.Kraj_termina = el.Datum_termina.AddMinutes(30);
                    kartonData.Add(el);
                }
            */
            KartonData1.ItemsSource = kartonData;

            List<Vrsta_pregleda> opet = new List<Vrsta_pregleda>();
            opet.Add(new Vrsta_pregleda()
            {
                
                Vrsta = "Kontrola"

            });
            opet.Add(new Vrsta_pregleda()
            {

                Vrsta = "Operacija"

            });
            opet.Add(new Vrsta_pregleda()
            {

                Vrsta = "Pregled"

            });
            List<Vrsta_pregleda> opet2 = new List<Vrsta_pregleda>();
            foreach (AppointmentDTO a in appointment)
            {
                
                /*Vrsta_pregleda v = new Vrsta_pregleda { Vrsta = a.AppointmentType.ToString() };
                
                
                if (opet2.Count() == 0)
                {
                    opet2.Add(v);
                }
                else
                {
                    foreach (Vrsta_pregleda vr in opet2)
                    {

                        if (vr.Vrsta != v.Vrsta)
                        {
                            opet2.Add(v);
                        }
                    }
                }*/
                

            }


            Vrsta2.ItemsSource = opet2;






            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            PregledKartona.Visibility = Visibility.Collapsed;
            OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Collapsed;
            Početna.Visibility = Visibility.Visible;
            PregledKartona.Visibility = Visibility.Collapsed;
            Početna.Visibility = Visibility.Visible;
            OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            IzmenaLičnihPodataka.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            Članci.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            GreškeSoftvera.Visibility = Visibility.Collapsed;
            PregledA.Visibility = Visibility.Collapsed;
            PregledTerapije.Visibility = Visibility.Collapsed;
            QandAA.Visibility = Visibility.Collapsed;
            ZakazivanjeTerminaa.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            TimeChange.Visibility = Visibility.Hidden;
            LekarChange.Visibility = Visibility.Hidden;


        }

       
        public void MenuItem_Click(object sender, RoutedEventArgs e)
        {

            var win = new Window1(logininfo);
            win.Show();
            Close();
        }
        public void Zakazi(object sender, RoutedEventArgs e)
        {

            if (MessageBox.Show("Da li želite da zakažete termin?","Confirmation", MessageBoxButton.YesNo ) == MessageBoxResult.Yes)
                            {
                                if (ZakazivanjeData.SelectedItem != null)
                                {
                                    Termin2 termin_izabran = (Termin2)ZakazivanjeData.SelectedItem;
                                    int k = 0;
                                    foreach (var el in Container.termini) {
                                        
                                        if (el.Equals(termin_izabran))
                                        {
                                            Container.termini[k].zauzet = true;
                                            Container.termini[k].Vrsta_pregleda = "Pregled";

                        }
                                        k++;
                                    }


                    List<Termin2> zakazan = new List<Termin2>();

                    foreach (var el in Container.termini)
                        if (el.zauzet.Equals(true) && el.Datum_termina > DateTime.Now)
                        {
                            zakazan.Add(el);
                        }
                    

                    List<Termin2> slobodni = new List<Termin2>();

                    foreach (var el in Container.termini)
                        if (el.zauzet.Equals(false) && el.Datum_termina > DateTime.Now)
                        {
                            slobodni.Add(el);
                        }

                    List<Termin2> zakazani = new List<Termin2>();
                    foreach (var el in appointment)
                    {

                        Termin2 novi = new Termin2 { Id = el.ID, Datum_termina = el.DateTime, Vrsta_pregleda = el.AppointmentType.ToString(), Lekar = el.DoctorDTO.FirstName + " " + el.DoctorDTO.LastName };
                            zakazani.Add(novi);
                    }
                    ZakazivanjeData.ItemsSource = slobodni;
                    ZakazivanjeData2.ItemsSource = slobodni;
                    TerminProba.ItemsSource = zakazani;
                    Container3.dataizabrana = (Termin2)TerminProba.SelectedItem;
                    ZakazanData.ItemsSource = zakazani;



                }
                if (ZakazivanjeData2.SelectedItem != null)
                {
                    Termin2 termin_izabran = (Termin2)ZakazivanjeData2.SelectedItem;
                    int k = 0;
                    string LekarNovi = "";
                    foreach (var el in Container.termini)
                    {

                        if (el.Equals(termin_izabran))
                        {
                            Container.termini[k].zauzet = true;
                            Container.termini[k].Vrsta_pregleda = "Pregled";
                            LekarNovi = Container.termini[k].Lekar;

                        }
                        k++;
                    }


                    List<Termin2> zakazan = new List<Termin2>();

                    foreach (var el in Container.termini)
                        if (el.zauzet.Equals(true))
                        {
                            zakazan.Add(el);
                        }


                    List<Termin2> slobodni = new List<Termin2>();

                    foreach (var el in Container.termini)
                        if (el.zauzet.Equals(false))
                        {
                            slobodni.Add(el);
                        }

                    List<Termin2> slobodni2 = new List<Termin2>();

                    foreach (var el in Container.termini)
                        if (el.zauzet.Equals(false) && el.Lekar.Equals(LekarNovi))
                        {
                            slobodni2.Add(el);
                        }
                    ZakazivanjeData.ItemsSource = slobodni;
                    ZakazivanjeData2.ItemsSource = slobodni2;

                    List<Termin2> zakazani = new List<Termin2>();
                    foreach (var el in appointment)
                    {

                        Termin2 novi = new Termin2 { Id = el.ID, Datum_termina = el.DateTime, Vrsta_pregleda = el.AppointmentType.ToString(), Lekar = el.DoctorDTO.FirstName + " " + el.DoctorDTO.LastName };
                            zakazani.Add(novi);
                    }
                    TerminProba.ItemsSource = zakazani;
                    Container3.dataizabrana = (Termin2)TerminProba.SelectedItem;



                }
            }
                            else
                            {
                                // Do not close the window  
                            }
        }
        public void PrikazKalendara(object sender, RoutedEventArgs e)
        {

            var win = new Window2();
            win.Show();

        }
        public void Anketa(object sender, RoutedEventArgs e)
        {
            Termin2 termin2 = new Termin2();
            termin2 = (Termin2)KartonData1.SelectedItem;
            DoctorDTO lekar= new DoctorDTO();
            String str = termin2.Lekar;

            String[] spearator = {" "};
            Int32 count = 1;

            // using the method 
            String[] strlist = str.Split((string[])spearator, (StringSplitOptions)count);

            List<DoctorDTO> lekari = userController.GetAllDoctors();

            lekar = lekari[0];

            foreach(var aa in lekari)
            {
                if (aa.FirstName == strlist[1] && aa.LastName== strlist[2])
                {
                    lekar = aa;
                }
            }

           

            var win = new Window4(lekar);
            win.Show();

        }

        public void PočetnaKlik(object sender, RoutedEventArgs e)
        {

            this.Title = "Početna stranica";
            List<Author> lekovi = new List<Author>();

            String text= Softver.Text;

            ReportDTO reportDTO = new ReportDTO(1, text);
            userController.LeaveBugReport(reportDTO);

            List<Author> lekovi2 = new List<Author>();

         

            foreach (var lek in Container4.lekovi2)
            {




                if (lek.Pocetak_terapije <= DateTime.Now && lek.Kraj_terapije >= DateTime.Now)
                {
                    int pomoc = lek.Nacin_uzimanja_terapije * 60;
                    int debag = Convert.ToInt32((DateTime.Now - lek.Pocetak_terapije).TotalMinutes);

                    while (debag > pomoc || debag < 0)
                    {

                        debag -= pomoc;

                    }

                    int rezultat = pomoc - debag;

                    lek.Sati_do_sledece_terapije = rezultat;
                    lekovi.Add(lek);
                }

            }
            List<Author> SortedList = lekovi.OrderBy(o => o.Sati_do_sledece_terapije).ToList();



            LekoviData.ItemsSource = SortedList;

            List<Termin2> zakazan = new List<Termin2>();

            foreach (var el in Container.termini)
                if (el.zauzet.Equals(true) && el.Datum_termina > DateTime.Now)
                {
                    zakazan.Add(el);
                }
            ZakazivanjeData2.ItemsSource = zakazan;

            List<Termin2> slobodni = new List<Termin2>();

            foreach (var el in Container.termini)
                if (el.zauzet.Equals(false) && el.Datum_termina > DateTime.Now)
                {
                    slobodni.Add(el);
                }
            

            List<Termin2> zakazani = new List<Termin2>();
            foreach (var el in appointment)
            {

                Termin2 novi = new Termin2 { Id = el.ID, Datum_termina = el.DateTime, Vrsta_pregleda = el.AppointmentType.ToString(), Lekar = el.DoctorDTO.FirstName + " " + el.DoctorDTO.LastName };
                    zakazani.Add(novi);
            }
            List<Termin2> SortedList2 = zakazani.OrderBy(o => o.Datum_termina).ToList();
            ZakazivanjeData.ItemsSource = slobodni;
            ZakazivanjeData2.ItemsSource = slobodni;
            TerminProba.ItemsSource = zakazani;
            Container3.dataizabrana = (Termin2)TerminProba.SelectedItem;
            ZakazanData.ItemsSource = SortedList2;


            PregledKartona.Visibility = Visibility.Collapsed;
                Početna.Visibility = Visibility.Visible;
                OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Collapsed;
                PromenaŠifre.Visibility = Visibility.Collapsed;
                IzmenaLičnihPodataka.Visibility = Visibility.Collapsed;
                PromenaŠifre.Visibility = Visibility.Collapsed;
                Članci.Visibility = Visibility.Collapsed;
                PregledLekara.Visibility = Visibility.Collapsed;
                GreškeSoftvera.Visibility = Visibility.Collapsed;
                PregledA.Visibility = Visibility.Collapsed;
                PregledTerapije.Visibility = Visibility.Collapsed;
                QandAA.Visibility = Visibility.Collapsed;
                ZakazivanjeTerminaa.Visibility = Visibility.Collapsed;
                PregledLekara.Visibility = Visibility.Collapsed;
            TimeChange.Visibility = Visibility.Hidden;
            LekarChange.Visibility = Visibility.Hidden;


        }
        public void PromenaTerminaPregledaKlik(object sender, RoutedEventArgs e)
        {
            this.Title = "Termini";
            List<Termin2> zakazan = new List<Termin2>();

            foreach (var el in Container.termini)
                if (el.zauzet.Equals(true) && el.Datum_termina > DateTime.Now )
                {
                    zakazan.Add(el);
                }

            List<Termin2> zakazani = new List<Termin2>();
            foreach (var el in appointment)
            {

                Termin2 novi = new Termin2 { Datum_termina = el.DateTime, Vrsta_pregleda = el.AppointmentType.ToString(), Lekar = el.DoctorDTO.FirstName + " " + el.DoctorDTO.LastName };
                    zakazani.Add(novi);
            }
            TerminProba.ItemsSource = zakazani;
            Container3.dataizabrana = (Termin2)TerminProba.SelectedItem;
            //TerminProba.





            PregledKartona.Visibility = Visibility.Collapsed;
            Početna.Visibility = Visibility.Collapsed;
            OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Visible;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            IzmenaLičnihPodataka.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            Članci.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            GreškeSoftvera.Visibility = Visibility.Collapsed;
            PregledA.Visibility = Visibility.Collapsed;
            PregledTerapije.Visibility = Visibility.Collapsed;
            QandAA.Visibility = Visibility.Collapsed;
            ZakazivanjeTerminaa.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            TimeChange.Visibility = Visibility.Hidden;
            LekarChange.Visibility = Visibility.Hidden;
        }
        private void PregledZakazanihTerminaKlik(object sender, RoutedEventArgs e)
        {
            this.Title = "Termini";
            PregledKartona.Visibility = Visibility.Collapsed;
            Početna.Visibility = Visibility.Collapsed;
            OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Visible;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            IzmenaLičnihPodataka.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            Članci.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            GreškeSoftvera.Visibility = Visibility.Collapsed;
            PregledA.Visibility = Visibility.Collapsed;
            PregledTerapije.Visibility = Visibility.Collapsed;
            QandAA.Visibility = Visibility.Collapsed;
            ZakazivanjeTerminaa.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            TimeChange.Visibility = Visibility.Hidden;
            LekarChange.Visibility = Visibility.Hidden;
        }
        private void PromenaLekaraKlik(object sender, RoutedEventArgs e)
        {
            this.Title = "Termini";
            PregledKartona.Visibility = Visibility.Collapsed;
            Početna.Visibility = Visibility.Collapsed;
            OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Visible;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            IzmenaLičnihPodataka.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            Članci.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            GreškeSoftvera.Visibility = Visibility.Collapsed;
            PregledA.Visibility = Visibility.Collapsed;
            PregledTerapije.Visibility = Visibility.Collapsed;
            QandAA.Visibility = Visibility.Collapsed;
            ZakazivanjeTerminaa.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            TimeChange.Visibility = Visibility.Hidden;
            LekarChange.Visibility = Visibility.Hidden;
        }
        private void PregledKartonaKlik(object sender, RoutedEventArgs e)
        {
            this.Title = "Pregled kartona";
            PregledKartona.Visibility = Visibility.Visible;
            Početna.Visibility = Visibility.Collapsed;
            OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            IzmenaLičnihPodataka.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            Članci.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            GreškeSoftvera.Visibility = Visibility.Collapsed;
            PregledA.Visibility = Visibility.Collapsed;
            PregledTerapije.Visibility = Visibility.Collapsed;
            QandAA.Visibility = Visibility.Collapsed;
            ZakazivanjeTerminaa.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            TimeChange.Visibility = Visibility.Hidden;
            LekarChange.Visibility = Visibility.Hidden;
        }
        private void PrijaviGreškeSoftveraKlik(object sender, RoutedEventArgs e)
        {
            this.Title = "Prijava zamerki na softver";
            PregledKartona.Visibility = Visibility.Collapsed;
            Početna.Visibility = Visibility.Collapsed;
            OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            IzmenaLičnihPodataka.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            Članci.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            GreškeSoftvera.Visibility = Visibility.Visible;
            PregledA.Visibility = Visibility.Collapsed;
            PregledTerapije.Visibility = Visibility.Collapsed;
            QandAA.Visibility = Visibility.Collapsed;
            ZakazivanjeTerminaa.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            TimeChange.Visibility = Visibility.Hidden;
            LekarChange.Visibility = Visibility.Hidden;

        }
        private void PregledLekaraKlik(object sender, RoutedEventArgs e)
        {
            this.Title = "Pregled lekara";
            PregledKartona.Visibility = Visibility.Collapsed;
            Početna.Visibility = Visibility.Collapsed;
            OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            IzmenaLičnihPodataka.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            Članci.Visibility = Visibility.Collapsed;
            PregledA.Visibility = Visibility.Collapsed;
            GreškeSoftvera.Visibility = Visibility.Collapsed;
            PregledA.Visibility = Visibility.Collapsed;
            PregledTerapije.Visibility = Visibility.Collapsed;
            QandAA.Visibility = Visibility.Collapsed;
            ZakazivanjeTerminaa.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Visible;
            TimeChange.Visibility = Visibility.Hidden;
            LekarChange.Visibility = Visibility.Hidden;

        }
        private void ZakazivanjeTerminaKlik(object sender, RoutedEventArgs e)
        {
            this.Title = "Zakazivanje termina";
            List<Termin2> slobodni = new List<Termin2>();
            foreach (var el in Container.termini)
                if ( el.Datum_termina > DateTime.Now && el.zauzet.Equals(false))
                {
                    slobodni.Add(el);
                }
            ZakazivanjeData.ItemsSource = slobodni;
            
            PregledKartona.Visibility = Visibility.Collapsed;
            Početna.Visibility = Visibility.Collapsed;
            OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            IzmenaLičnihPodataka.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            Članci.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            GreškeSoftvera.Visibility = Visibility.Collapsed;
            PregledA.Visibility = Visibility.Collapsed;
            PregledTerapije.Visibility = Visibility.Collapsed;
            QandAA.Visibility = Visibility.Collapsed;
            ZakazivanjeTerminaa.Visibility = Visibility.Visible;
            PregledLekara.Visibility = Visibility.Collapsed;
            TimeChange.Visibility = Visibility.Hidden;
            LekarChange.Visibility = Visibility.Hidden;
        }
       
        private void Lekar1(object sender, RoutedEventArgs e)
        {
            this.Title = "Detaljni prikaz lekara";
           if (Lekarii.SelectedItem != null)
            {
                Lekari lekar_izabran = (Lekari)Lekarii.SelectedItem;
                izabran.Content = lekar_izabran.Lekar;
                Pon.Content = lekar_izabran.radnoVreme.Pon;
                Uto.Content = lekar_izabran.radnoVreme.Uto;
                Sre.Content = lekar_izabran.radnoVreme.Sre;
                Cetv.Content = lekar_izabran.radnoVreme.Cetv;
                Pet.Content = lekar_izabran.radnoVreme.Petak;
                List<Termin2> terpom = new List<Termin2>();
                foreach (var el in Container.termini)
                    if (el.Lekar.Equals(lekar_izabran.Lekar) && el.zauzet==false)
                    {
                        terpom.Add(el);
                    }
                ZakazivanjeData2.ItemsSource = terpom;
                List<Termin2> zakazan = new List<Termin2>();

                foreach (var el in Container.termini)
                    if (el.zauzet.Equals(true))
                    {
                        zakazan.Add(el);
                    }
                

                List<Termin2> slobodni = new List<Termin2>();

                foreach (var el in Container.termini)
                    if (el.zauzet.Equals(false))
                    {
                        slobodni.Add(el);
                    }
                ZakazivanjeData.ItemsSource = slobodni;
                
                TerminProba.ItemsSource = zakazan;




            }
            
            PregledKartona.Visibility = Visibility.Collapsed;
            Početna.Visibility = Visibility.Collapsed;
            OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            IzmenaLičnihPodataka.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            Članci.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            GreškeSoftvera.Visibility = Visibility.Collapsed;
            PregledA.Visibility = Visibility.Collapsed;
            PregledTerapije.Visibility = Visibility.Collapsed;
            QandAA.Visibility = Visibility.Collapsed;
            ZakazivanjeTerminaa.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Visible;
            TimeChange.Visibility = Visibility.Hidden;
            LekarChange.Visibility = Visibility.Hidden;







        }
        private void LekariPregled(object sender, RoutedEventArgs e)
        {
            this.Title = "Pregled lekara";
            PregledKartona.Visibility = Visibility.Collapsed;
            Početna.Visibility = Visibility.Collapsed;
            OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            IzmenaLičnihPodataka.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            Članci.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            GreškeSoftvera.Visibility = Visibility.Collapsed;
            PregledA.Visibility = Visibility.Visible;
            PregledTerapije.Visibility = Visibility.Collapsed;
            QandAA.Visibility = Visibility.Collapsed;
            ZakazivanjeTerminaa.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            TimeChange.Visibility = Visibility.Hidden;
            LekarChange.Visibility = Visibility.Hidden;

        }
        private void OtkazivanjeTerminaKlik(object sender, RoutedEventArgs e)
        {
            this.Title = "Termini";
            PregledKartona.Visibility = Visibility.Collapsed;
            Početna.Visibility = Visibility.Collapsed;
            OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Visible;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            IzmenaLičnihPodataka.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            Članci.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            GreškeSoftvera.Visibility = Visibility.Collapsed;
            PregledA.Visibility = Visibility.Collapsed;
            PregledTerapije.Visibility = Visibility.Collapsed;
            QandAA.Visibility = Visibility.Collapsed;
            ZakazivanjeTerminaa.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            TimeChange.Visibility = Visibility.Hidden;
            LekarChange.Visibility = Visibility.Hidden;
        }
        private void PrepisanaTerapijaKlik(object sender, RoutedEventArgs e)
        {
            this.Title = "Prepisana terapija";
            PregledKartona.Visibility = Visibility.Collapsed;
            Početna.Visibility = Visibility.Collapsed;
            OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            IzmenaLičnihPodataka.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            Članci.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            GreškeSoftvera.Visibility = Visibility.Collapsed;
            PregledA.Visibility = Visibility.Collapsed;
            PregledTerapije.Visibility = Visibility.Visible;
            QandAA.Visibility = Visibility.Collapsed;
            ZakazivanjeTerminaa.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            TimeChange.Visibility = Visibility.Hidden;
            LekarChange.Visibility = Visibility.Hidden;
        }
        private void ČlanciKlik(object sender, RoutedEventArgs e)
        {
            this.Title = "Članci";
            PregledKartona.Visibility = Visibility.Collapsed;
            Početna.Visibility = Visibility.Collapsed;
            OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            IzmenaLičnihPodataka.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            Članci.Visibility = Visibility.Visible;
            PregledLekara.Visibility = Visibility.Collapsed;
            GreškeSoftvera.Visibility = Visibility.Collapsed;
            PregledA.Visibility = Visibility.Collapsed;
            PregledTerapije.Visibility = Visibility.Collapsed;
            QandAA.Visibility = Visibility.Collapsed;
            ZakazivanjeTerminaa.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            TimeChange.Visibility = Visibility.Hidden;
            LekarChange.Visibility = Visibility.Hidden;
        }
        private void Registaracija(object sender, RoutedEventArgs e)
        {
            this.Title = "Registracija";
            PregledKartona.Visibility = Visibility.Collapsed;
            Početna.Visibility = Visibility.Collapsed;
            OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            IzmenaLičnihPodataka.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            Članci.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            PregledA.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            GreškeSoftvera.Visibility = Visibility.Collapsed;
            TimeChange.Visibility = Visibility.Hidden;
            LekarChange.Visibility = Visibility.Hidden;

            PregledTerapije.Visibility = Visibility.Collapsed;
            QandAA.Visibility = Visibility.Collapsed;
            ZakazivanjeTerminaa.Visibility = Visibility.Collapsed;
        }
        private void IzmenaLozinkeKlik(object sender, RoutedEventArgs e)
        {
            this.Title = "Promena šifre";
            proba.Visibility = Visibility.Hidden;
            proba2.Visibility = Visibility.Hidden;
            PregledKartona.Visibility = Visibility.Collapsed;
            Početna.Visibility = Visibility.Collapsed;
            OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            IzmenaLičnihPodataka.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Visible;
            Članci.Visibility = Visibility.Collapsed;
            TimeChange.Visibility = Visibility.Hidden;
            LekarChange.Visibility = Visibility.Hidden;

            PregledA.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            GreškeSoftvera.Visibility = Visibility.Collapsed;

            PregledTerapije.Visibility = Visibility.Collapsed;
            QandAA.Visibility = Visibility.Collapsed;
            ZakazivanjeTerminaa.Visibility = Visibility.Collapsed;
        }
        private void IzmenaLicnihPodatakaKlik(object sender, RoutedEventArgs e)
        {

            proba.Visibility = Visibility.Hidden; 
            this.Title = "Promena ličnih podataka";
            PregledKartona.Visibility = Visibility.Collapsed;
            Početna.Visibility = Visibility.Collapsed;
            OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            IzmenaLičnihPodataka.Visibility = Visibility.Visible;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            GreškeSoftvera.Visibility = Visibility.Collapsed;
            PregledA.Visibility = Visibility.Collapsed;
            PregledTerapije.Visibility = Visibility.Collapsed;
            QandAA.Visibility = Visibility.Collapsed;
            ZakazivanjeTerminaa.Visibility = Visibility.Collapsed;
            Članci.Visibility = Visibility.Collapsed;
            TimeChange.Visibility = Visibility.Hidden;
            LekarChange.Visibility = Visibility.Hidden;
        }
        private void QandAKlik(object sender, RoutedEventArgs e)
        {
            this.Title = "Pitanja i odgovori";
            Članci.Visibility = Visibility.Collapsed;
            PregledKartona.Visibility = Visibility.Collapsed;
            Početna.Visibility = Visibility.Collapsed;
            OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            IzmenaLičnihPodataka.Visibility = Visibility.Collapsed;
            PromenaŠifre.Visibility = Visibility.Collapsed;
            PregledLekara.Visibility = Visibility.Collapsed;
            GreškeSoftvera.Visibility = Visibility.Collapsed;
            PregledA.Visibility = Visibility.Collapsed;
            PregledTerapije.Visibility = Visibility.Collapsed;
            QandAA.Visibility = Visibility.Visible;
            ZakazivanjeTerminaa.Visibility = Visibility.Collapsed;
            TimeChange.Visibility = Visibility.Hidden;
            LekarChange.Visibility = Visibility.Hidden;
        }
        private void Članak(object sender, RoutedEventArgs e)
        {
            this.Title = "Članci";
            var win = new Window3();
            win.Show();
        }
        private void Login(object sender, RoutedEventArgs e)
        {
            this.Title = "Prijava";
            var win = new MainWindow();
            win.Show();
            Close();
        }
        private void DataGrid_Loaded3(object sender, RoutedEventArgs e)
        {
            // ... Create.
            var items = new List<Terapija>();
            items.Add(new Terapija("Bromazepan", "12.12.1222.", "14.13.1223.", "Dva puta dnevno"));
            items.Add(new Terapija("Bromazepan", "12.12.1222.", "14.13.1223.", "Dva puta dnevno"));
            items.Add(new Terapija("Bromazepan", "12.12.1222.", "14.13.1223.", "Dva puta dnevno"));
            items.Add(new Terapija("Bromazepan", "12.12.1222.", "14.13.1223.", "Dva puta dnevno"));
            items.Add(new Terapija("Bromazepan", "12.12.1222.", "14.13.1223.", "Dva puta dnevno"));


            // ... Assign.
            var grid = sender as DataGrid;
            grid.ItemsSource = items;
        }
        private void DataGrid_SelectionChanged3(object sender,
            SelectionChangedEventArgs e)
        {
            // ... Get SelectedItems from DataGrid.
            var grid = sender as DataGrid;
            var selected = grid.SelectedItems;

            // ... Add all Names to a List.
            List<string> names = new List<string>();
            foreach (var item in selected)
            {
                var termin = item as Terapija;
                names.Add(termin.Ime_leka);
            }
            // ... Set Title to selected names.
            this.Title = string.Join(", ", names);
        }
        private void UnesiIme(object sender, RoutedEventArgs e)
        {
            Ime.Visibility = Visibility.Hidden;
            
            Ime.IsReadOnly = false;
        }
        private void UnesiJMBG(object sender, RoutedEventArgs e)
        {
            JMBG.Visibility = Visibility.Hidden;
            JMBGUnos.Visibility = Visibility.Visible;
        }

        private void UnesiDrzavu(object sender, RoutedEventArgs e)
        {
            Drzava.Visibility = Visibility.Hidden;
            DrzavaUnos.Visibility = Visibility.Visible;
        }
        private void UnesiUlicu(object sender, RoutedEventArgs e)
        {
            Ulica.Visibility = Visibility.Hidden;
            UlicaUnos.Visibility = Visibility.Visible;
        }
        private void UnesiEmail(object sender, RoutedEventArgs e)
        {
            Email.Visibility = Visibility.Hidden;
            EmailUnos.Visibility = Visibility.Visible;
        }
        private void UnesiPrezime(object sender, RoutedEventArgs e)
        {
            Prezime.Visibility = Visibility.Hidden;
            PrezimeUnos.Visibility = Visibility.Visible;
        }
        private void UnesiGrad(object sender, RoutedEventArgs e)
        {
            Grad.Visibility = Visibility.Hidden;
            GradUnos.Visibility = Visibility.Visible;
            
        }
        private void UnesiBroj(object sender, RoutedEventArgs e)
        {
            Broj.Visibility = Visibility.Hidden;
            BrojUnos.Visibility = Visibility.Visible;
        }
        private void UnesiTelefon(object sender, RoutedEventArgs e)
        {
            //Telefon.Visibility = Visibility.Hidden;
            //TelefonUnos.Visibility = Visibility.Visible;
        }
      
      /*  private void DataGrid_SelectionChanged5(object sender,
            SelectionChangedEventArgs e)
        {
            // ... Get SelectedItems from DataGrid.
            var grid = sender as DataGrid;
            var selected = grid.SelectedItems;

            // ... Add all Names to a List.
            List<string> names = new List<string>();
            foreach (var item in selected)
            {
                var termin = item as Termin2;
                names.Add(termin.Lekar);
            }
            // ... Set Title to selected names.
            this.Title = string.Join(", ", names);
        }*/

        public class Person
        {

            private string nameValue;

            public string Name
            {
                get { return nameValue; }
                set { nameValue = value; }
            }

            private double ageValue;

            public double Age
            {
                get { return ageValue; }

                set
                {
                    if (value != ageValue)
                    {
                        ageValue = value;
                    }
                }
            }

        }


        private void PotvrdaIzmena(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Da li ste potvrđujete izmene?",
         "Potvrda", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                
                PatientDTO patient = new PatientDTO(EmailUnos.Text, logininfo.Username, logininfo.Password, logininfo.ID, ImeUnos.Text, PrezimeUnos.Text, JMBGUnos.Text, logininfo.JMBG, logininfo.Adress);
                userController.UpdateMyAccPatient(patient);
                PregledKartona.Visibility = Visibility.Collapsed;
                Početna.Visibility = Visibility.Visible;
                OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Collapsed;
                PromenaŠifre.Visibility = Visibility.Collapsed;
                IzmenaLičnihPodataka.Visibility = Visibility.Collapsed;
                PromenaŠifre.Visibility = Visibility.Collapsed;
                Članci.Visibility = Visibility.Collapsed;
                PregledLekara.Visibility = Visibility.Collapsed;
                GreškeSoftvera.Visibility = Visibility.Collapsed;
                PregledA.Visibility = Visibility.Collapsed;
                PregledTerapije.Visibility = Visibility.Collapsed;
                QandAA.Visibility = Visibility.Collapsed;
                ZakazivanjeTerminaa.Visibility = Visibility.Collapsed;
                PregledLekara.Visibility = Visibility.Collapsed;
            }
            else
            {
                PregledKartona.Visibility = Visibility.Collapsed;
                Početna.Visibility = Visibility.Visible;
                OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Collapsed;
                PromenaŠifre.Visibility = Visibility.Collapsed;
                IzmenaLičnihPodataka.Visibility = Visibility.Collapsed;
                PromenaŠifre.Visibility = Visibility.Collapsed;
                Članci.Visibility = Visibility.Collapsed;
                PregledLekara.Visibility = Visibility.Collapsed;
                GreškeSoftvera.Visibility = Visibility.Collapsed;
                PregledA.Visibility = Visibility.Collapsed;
                PregledTerapije.Visibility = Visibility.Collapsed;
                QandAA.Visibility = Visibility.Collapsed;
                ZakazivanjeTerminaa.Visibility = Visibility.Collapsed;
                PregledLekara.Visibility = Visibility.Collapsed;
            }
           
        }

        private void LozinkaKlik(object sender, RoutedEventArgs e)
        {
            if (p2.Password.Equals(p3.Password) && p1.Password.ToString().Equals(logininfo.Password.ToString()))
            {
                proba.Visibility = Visibility.Hidden;
                proba2.Visibility = Visibility.Hidden;

                if (MessageBox.Show("Da li ste sigurni da želite da promenite lozinku?",
         "Potvrda", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    PatientDTO patientDTO = new PatientDTO(logininfo.EMail, logininfo.Username, p2.Password.ToString(), logininfo.ID, logininfo.FirstName, logininfo.LastName, logininfo.TelephoneNumber, logininfo.JMBG, logininfo.Adress);
                    userController.UpdateMyAccPatient(patientDTO);

                    PregledKartona.Visibility = Visibility.Collapsed;
                    Početna.Visibility = Visibility.Visible;
                    OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Collapsed;
                    PromenaŠifre.Visibility = Visibility.Collapsed;
                    IzmenaLičnihPodataka.Visibility = Visibility.Collapsed;
                    PromenaŠifre.Visibility = Visibility.Collapsed;
                    Članci.Visibility = Visibility.Collapsed;
                    PregledLekara.Visibility = Visibility.Collapsed;
                    GreškeSoftvera.Visibility = Visibility.Collapsed;
                    PregledA.Visibility = Visibility.Collapsed;
                    PregledTerapije.Visibility = Visibility.Collapsed;
                    QandAA.Visibility = Visibility.Collapsed;
                    ZakazivanjeTerminaa.Visibility = Visibility.Collapsed;
                    PregledLekara.Visibility = Visibility.Collapsed;
                }


                else
                {
                    PregledKartona.Visibility = Visibility.Collapsed;
                    Početna.Visibility = Visibility.Visible;
                    OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Collapsed;
                    PromenaŠifre.Visibility = Visibility.Collapsed;
                    IzmenaLičnihPodataka.Visibility = Visibility.Collapsed;
                    PromenaŠifre.Visibility = Visibility.Collapsed;
                    Članci.Visibility = Visibility.Collapsed;
                    PregledLekara.Visibility = Visibility.Collapsed;
                    GreškeSoftvera.Visibility = Visibility.Collapsed;
                    PregledA.Visibility = Visibility.Collapsed;
                    PregledTerapije.Visibility = Visibility.Collapsed;
                    QandAA.Visibility = Visibility.Collapsed;
                    ZakazivanjeTerminaa.Visibility = Visibility.Collapsed;
                    PregledLekara.Visibility = Visibility.Collapsed;
                }
            }
            else
            {
                if (!p1.Password.Equals("Admin"))
                {
                    proba2.Visibility = Visibility.Visible;
                    proba.Visibility = Visibility.Hidden;
                }
                else
                {
                    proba.Visibility = Visibility.Visible;
                    proba2.Visibility = Visibility.Hidden;
                }
            }

        }

        /*private void PreporukaKlik(object sender, RoutedEventArgs e)
        {
            
            var win = new Window5();
            win.Show();

        }*/

        private void PostaviPitanje(object sender, RoutedEventArgs e)
        {

            var win = new Window6(logininfo);
            win.Show();

        }

        private void Otkazi(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Da li ste sigurni da želite da otkažete termin?",
           "Potvrda", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                Container3.dataizabrana = (Termin2)TerminProba.SelectedItem;
                if (TerminProba.SelectedItem != null)
                {
                    Container.termini.Remove((Termin2)TerminProba.SelectedItem);
                   

                    List<Termin2> zakazan = new List<Termin2>();

                    foreach (var el in Container.termini)
                        if (el.zauzet.Equals(true) &&  el.Datum_termina > DateTime.Now)
                        {
                            zakazan.Add(el);
                        }


                    List<Termin2> slobodni = new List<Termin2>();

                    foreach (var el in Container.termini)
                        if (el.zauzet.Equals(false))
                        {
                            slobodni.Add(el);
                        }

                    List<Termin2> zakazani = new List<Termin2>();
                    foreach (var el in appointment)
                    {

                        Termin2 novi = new Termin2 { Id = el.ID, Datum_termina = el.DateTime, Vrsta_pregleda = el.AppointmentType.ToString(), Lekar = el.DoctorDTO.FirstName + " " + el.DoctorDTO.LastName };
                            zakazani.Add(novi);
                    }
                    ZakazivanjeData.ItemsSource = slobodni;
                    ZakazivanjeData2.ItemsSource = slobodni;
                    TerminProba.ItemsSource = zakazan;
                    Container3.dataizabrana = (Termin2)TerminProba.SelectedItem;

                }
                else
                {
                    // Do not close the window  
                }
            }
        }

        private void IzmeniL(object sender, RoutedEventArgs e)
        {

            
            List<Termin2> zakazani = new List<Termin2>();
            foreach (var el in appointment)
            {

                Termin2 novi = new Termin2 { Id = el.ID, Datum_termina = el.DateTime, Vrsta_pregleda = el.AppointmentType.ToString(), Lekar = el.DoctorDTO.FirstName + " " + el.DoctorDTO.LastName };
                    zakazani.Add(novi);
            }
            if (TerminProba.SelectedItem != null)
            {
                
                Termin2 termin_izabran = (Termin2)TerminProba.SelectedItem;
                Container3.dataizabrana = termin_izabran;
            }
            Container3.dataizabrana = (Termin2)TerminProba.SelectedItem;

            LekarChange.Visibility=Visibility.Visible;
            OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Hidden;
            TimeChange.Visibility = Visibility.Hidden;





        }
        private void IzmeniT(object sender, RoutedEventArgs e)
        {
            if (TerminProba.SelectedItem != null)
            {
                Termin2 termin_izabran = (Termin2)TerminProba.SelectedItem;
                Container3.dataizabrana = termin_izabran;
            }

            TimeChange.Visibility = Visibility.Visible;
            OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Hidden;
            LekarChange.Visibility = Visibility.Hidden;


        }

        private void Dijagnoza(object sender, RoutedEventArgs e)
        {
            Termin2 izabran = (Termin2)KartonData1.SelectedItem;
            
           
            int medicalId = izabran.Id;
             
            var win = new Window8(medicalId);
            win.Show();

        }
        private void Uput(object sender, RoutedEventArgs e)
        {
            Termin2 izabran = (Termin2)KartonData1.SelectedItem;


            int medicalId = izabran.Id;

            var win = new Window9(medicalId);
            win.Show();

        }

        private void Pretraga(object sender, RoutedEventArgs e)
        {
            string ime = LekariNovo2.Text;
            
            string vrsta = Vrsta2.Text;
            
            //DateTime date = (DateTime)Picker3.SelectedDate;
            List<Termin2> zakazani = new List<Termin2>();
            foreach (var el in appointment)
            {

                Termin2 novi = new Termin2 { Datum_termina = el.DateTime, Vrsta_pregleda = el.AppointmentType.ToString(), Lekar = el.DoctorDTO.FirstName + " " + el.DoctorDTO.LastName };
                zakazani.Add(novi);
            }
            TerminProba.ItemsSource = zakazani;
            Container3.dataizabrana = (Termin2)TerminProba.SelectedItem;

            List<Termin2> zakazanifilter = new List<Termin2>();
            List<Termin2> zakazanifilter2 = new List<Termin2>();

            foreach (var el in zakazani)
                if (ime.Length == 0)
                {
                    if (el.Vrsta_pregleda.Equals(vrsta))
                    {
                        zakazanifilter.Add(el);
                    }
                }
                else
                {
                    if(vrsta.Length == 0)
                    {
                        if (el.Lekar.Equals(ime))
                        {
                            zakazanifilter.Add(el);
                        }

                    }
                    else
                    {
                        if (el.Lekar.Equals(ime) && el.Vrsta_pregleda.Equals(vrsta))
                        {
                            zakazanifilter.Add(el);
                        }
                    }
                }

            foreach (var el in zakazanifilter)
            {
                if (Picker3.SelectedDate == null)
                {
                    TerminProba.ItemsSource = zakazanifilter;
                    Container3.dataizabrana = (Termin2)TerminProba.SelectedItem;
                    break;
                }
                else
                {
                    if (el.Datum_termina.Date.Equals(Picker3.SelectedDate))
                    {
                        zakazanifilter2.Add(el);
                    }
                }

                TerminProba.ItemsSource = zakazanifilter2;
                Container3.dataizabrana = (Termin2)TerminProba.SelectedItem;
            }

                

        }

        private void Pretraga_lekar(object sender, RoutedEventArgs e)
        {
            List<Termin2> slobodni = new List<Termin2>();
            foreach (var el in Container.termini)
                if (el.Lekar.Equals("Kristijan Savić") && el.Datum_termina > DateTime.Now && el.zauzet.Equals(true) )
                {
                    slobodni.Add(el);
                }
            TerminProba.ItemsSource = slobodni;

        }

        private void PretragaTerapije(object sender, RoutedEventArgs e)
        {
            String text= Terapijaa.Text;
            List<Author> slobodni = new List<Author>();
            foreach (var el in Container4.lekovi2)
                if (el.Ime_leka.Equals(text) )
                {
                    slobodni.Add(el);
                }
            TerapijaData.ItemsSource = slobodni;

        }

        private void PreporukaTermina1(object sender, RoutedEventArgs e)
        {
            List<Termin2> slobodni = new List<Termin2>();
            foreach (var el in Container.termini)
                if (el.Lekar.Equals("Kristijan Savić") && el.Datum_termina > DateTime.Now && el.zauzet.Equals(false))
                {
                    slobodni.Add(el);
                }
            ZakazivanjeData.ItemsSource = slobodni;

        }
        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            //Create a new PDF document
            using (PdfDocument document = new PdfDocument())
            {

                PdfDocument doc = new PdfDocument();
                //Add a page.
                PdfPage page = doc.Pages.Add();
                //Create a PdfGrid.
                PdfGrid pdfGrid = new PdfGrid();

                /*PdfFont font = new PdfStandardFont(PdfFontFamily.Helvetica, 20);

                //Draw the text
                PdfGraphics graphics = page.Graphics;
                graphics.DrawString(" Izveštaj o rasporedu terapije na sedmičnom nivou", font, PdfBrushes.Black, new PointF(0, 0));*/
                //Create a DataTable
                DataTable dataTable = new DataTable();
                //Add columns to the DataTable
                dataTable.Columns.Add("Ime terapije");
                dataTable.Columns.Add("Vreme uzimanja terapije");
                //Add rows to the DataTable.
                //string pomocni = "";
                List<Author> prva = new List<Author>();

                foreach (var el in Container4.lekovi2)
                {
                    prva.Add(el);
                  
                }

                List<Author> prva2 = new List<Author>();

                foreach (var el in prva)
                {

                    double debag = Convert.ToDouble((DateTime.Now - el.Pocetak_terapije).TotalHours);

                    if (debag <= (7 * 24))
                    {
                        prva2.Add(el);
                    }
                    else
                    {

                        double pomoc = Convert.ToInt64((DateTime.Now - el.Pocetak_terapije).TotalDays) - 7;
                        el.Pocetak_terapije = el.Pocetak_terapije.AddDays(pomoc);




                        prva2.Add(el);

                    }
                   
                    

                }


                List<Author> prva3 = new List<Author>();

                foreach (var el in prva2)
                {

                    while (el.Pocetak_terapije <= DateTime.Now)
                    {
                        el.Pocetak_terapije = el.Pocetak_terapije.AddMinutes(el.Nacin_uzimanja_terapije * 60);

                        dataTable.Rows.Add(new object[] { el.Ime_leka, el.Pocetak_terapije });
                        prva3.Add(el);
                    }
                }






                //Assign data source.
                pdfGrid.DataSource = dataTable;
                //Draw grid to the page of PDF document.
                pdfGrid.Draw(page, new PointF(10, 10));
                //Save the document.
                doc.Save("Output.pdf");
                //close the document
                doc.Close(true);
               
                //Launching the Excel file using the default Application.[MS Excel Or Free ExcelViewer]
                System.Diagnostics.Process.Start("Output.pdf");

                        
               
            }

           
        }

        private void PotvrdiNovogLekara(object sender, RoutedEventArgs e)
        {
            
            string str = "";
            str = Lekar4.Text;
         
            int k = 0;
            List<Termin2> zakazan = new List<Termin2>();
            foreach (var el in Container.termini)
                {

                    if (el.Equals(Container3.dataizabrana))
                    {
                        if (str.Equals("")) { }
                        else { Container.termini[k].Lekar = str; }                                           
                }
                k++;
                }
            foreach (var el in Container.termini)
            {

                if (el.zauzet.Equals(true) && el.Datum_termina>DateTime.Now)
                {
                    zakazan.Add(el);


                }
               
            }

            TerminProba.ItemsSource = zakazan;
            Container3.dataizabrana = (Termin2)TerminProba.SelectedItem;

            OtkazivanjeIzmenaPregledTermina.Visibility= Visibility.Visible;
            
            LekarChange.Visibility = Visibility.Hidden;
           
        }

        private void OdustaniNovogLekara(object sender, RoutedEventArgs e)
        {


        }
        private void PotvrdiNoviTermin(object sender, RoutedEventArgs e)
        {

            DateTime pomoc = (DateTime)Picker.SelectedDate;
            DateTime pomoc2 = (DateTime)Picker2.SelectedTime;
            TimeSpan s = pomoc2.Subtract(pomoc);
            DateTime nov = pomoc.AddHours(s.Hours).AddMinutes(s.Minutes);

            int k = 0;
            

            List<Termin2> zakazani = new List<Termin2>();
            foreach (var el in appointment)
            {

                Termin2 novi = new Termin2 { Id = el.ID, Datum_termina = el.DateTime, Vrsta_pregleda = el.AppointmentType.ToString(), Lekar = el.DoctorDTO.FirstName + " " + el.DoctorDTO.LastName };
                zakazani.Add(novi);
            }

            //AppointmentDTO pomocni = appointmentController.GetAppointmentByID(Container3.dataizabrana.Id);
            //Console.WriteLine("{0}", pomocni.DateTime);*/
            //AppointmentDTO nov = new AppointmentDTO(Container3.dataizabrana.Id, Container3.dataizabrana.Datum_termina, 30, Container3.dataizabrana.Vrsta_pregleda, logininfo.ID, 1, new ExaminationReportDTO());
            /*foreach (var el in Container.termini)
            {

                if (el.Equals(Container3.dataizabrana))
                {
                   
                   Container.termini[k].Datum_termina = nov; 


                }

                k++;
            }*/


         


            TerminProba.ItemsSource = zakazani;
            Container3.dataizabrana = (Termin2)TerminProba.SelectedItem;

            OtkazivanjeIzmenaPregledTermina.Visibility = Visibility.Visible;

            TimeChange.Visibility = Visibility.Hidden;

        }
        private void OdustaniNoviTermin(object sender, RoutedEventArgs e)
        {


        }

        private void LekoviData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void KartonData1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }


   


}

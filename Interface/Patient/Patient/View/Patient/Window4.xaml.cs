﻿using Bolnica.Controller;
using Bolnica.Model;
using Bolnica.Model.ModelDTO;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Patient.View.Patient
{
    /// <summary>
    /// Interaction logic for Window4.xaml
    /// </summary>
    public partial class Window4 : Window
    {
        private PatientController patientController = new PatientController();

        private DoctorFeedbackDTO feedback = new DoctorFeedbackDTO();
        public Window4(DoctorDTO doc)
        {
            //patientController.DoctorFeedback(feedback);
            feedback.Doctor = doc;
            
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;

        }

        private void Potvrda(object sender, RoutedEventArgs e)
        {
            String ocena = Ocena.Text;
            feedback.ID = 1;
            feedback.NumberOfRatings = 3;
            feedback.Rating = int.Parse(Ocena.Text);

            patientController.DoctorFeedback(feedback);
            Close();
                

        }

       



            

            












         



           






          






          



          



        
    }
}


// File:    LogInContoller.cs
// Author:  lazar
// Created: Monday, June 22, 2020 5:08:58 PM
// Purpose: Definition of Class LogInContoller

using System;

namespace Controller
{
   public class LogInContoller
   {
      public Model.ModelDTO.UserDTO SignIn(Model.ModelDTO.UserDTO user)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.UserDTO SignOut(Model.ModelDTO.UserDTO user)
      {
         throw new NotImplementedException();
      }
      
      public System.Collections.Generic.List<LogInService> logInService;
      
      /// <summary>
      /// Property for collection of Service.LogInService
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<LogInService> LogInService
      {
         get
         {
            if (logInService == null)
               logInService = new System.Collections.Generic.List<LogInService>();
            return logInService;
         }
         set
         {
            RemoveAllLogInService();
            if (value != null)
            {
               foreach (Service.LogInService oLogInService in value)
                  AddLogInService(oLogInService);
            }
         }
      }
      
      /// <summary>
      /// Add a new Service.LogInService in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddLogInService(Service.LogInService newLogInService)
      {
         if (newLogInService == null)
            return;
         if (this.logInService == null)
            this.logInService = new System.Collections.Generic.List<LogInService>();
         if (!this.logInService.Contains(newLogInService))
            this.logInService.Add(newLogInService);
      }
      
      /// <summary>
      /// Remove an existing Service.LogInService from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveLogInService(Service.LogInService oldLogInService)
      {
         if (oldLogInService == null)
            return;
         if (this.logInService != null)
            if (this.logInService.Contains(oldLogInService))
               this.logInService.Remove(oldLogInService);
      }
      
      /// <summary>
      /// Remove all instances of Service.LogInService from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllLogInService()
      {
         if (logInService != null)
            logInService.Clear();
      }
   
   }
}
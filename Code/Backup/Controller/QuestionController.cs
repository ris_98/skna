// File:    QuestionController.cs
// Author:  lazar
// Created: Sunday, June 21, 2020 10:28:24 PM
// Purpose: Definition of Class QuestionController

using System;

namespace Controller
{
   public class QuestionController
   {
      public Model.ModelDTO.QuestionDTO AskQuestion(Model.Question question)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.QuestionDTO DeleteQuestion(Model.Question question)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.QuestionDTO AnswerQuestion(Model.Question question)
      {
         throw new NotImplementedException();
      }
      
      public List<Question> ShowQuestions()
      {
         throw new NotImplementedException();
      }
      
      public System.Collections.Generic.List<QuestionService> questionService;
      
      /// <summary>
      /// Property for collection of Service.QuestionService
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<QuestionService> QuestionService
      {
         get
         {
            if (questionService == null)
               questionService = new System.Collections.Generic.List<QuestionService>();
            return questionService;
         }
         set
         {
            RemoveAllQuestionService();
            if (value != null)
            {
               foreach (Service.QuestionService oQuestionService in value)
                  AddQuestionService(oQuestionService);
            }
         }
      }
      
      /// <summary>
      /// Add a new Service.QuestionService in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddQuestionService(Service.QuestionService newQuestionService)
      {
         if (newQuestionService == null)
            return;
         if (this.questionService == null)
            this.questionService = new System.Collections.Generic.List<QuestionService>();
         if (!this.questionService.Contains(newQuestionService))
            this.questionService.Add(newQuestionService);
      }
      
      /// <summary>
      /// Remove an existing Service.QuestionService from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveQuestionService(Service.QuestionService oldQuestionService)
      {
         if (oldQuestionService == null)
            return;
         if (this.questionService != null)
            if (this.questionService.Contains(oldQuestionService))
               this.questionService.Remove(oldQuestionService);
      }
      
      /// <summary>
      /// Remove all instances of Service.QuestionService from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllQuestionService()
      {
         if (questionService != null)
            questionService.Clear();
      }
   
   }
}
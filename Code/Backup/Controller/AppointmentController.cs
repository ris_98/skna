// File:    AppointmentController.cs
// Author:  Korisnik
// Created: Saturday, May 30, 2020 9:40:44 AM
// Purpose: Definition of Class AppointmentController

using System;

namespace Controller
{
   public class AppointmentController
   {
      public Model.ModelDTO.AppointmentDTO ScheduleAppointment(Model.ModelDTO.AppointmentDTO appointment)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.AppointmentDTO ModifyAppointmentDate(Model.ModelDTO.AppointmentDTO appointment)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.AppointmentDTO ModifyAppointmentDoctor(Model.ModelDTO.AppointmentDTO appointment)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.AppointmentDTO CancelAppointment(Model.ModelDTO.AppointmentDTO appointment)
      {
         throw new NotImplementedException();
      }
      
      public List<AppointmentDTO> ShowScheduledAppointments(Model.ModelDTO.UserDTO user)
      {
         throw new NotImplementedException();
      }
      
      public List<AppointmentDTO> RecommendAppointment(int dataRange, Model.ModelDTO.DoctorDTO doctor)
      {
         throw new NotImplementedException();
      }
      
      public System.Collections.Generic.List<AppointmentService> appointmentService;
      
      /// <summary>
      /// Property for collection of Service.AppointmentService
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<AppointmentService> AppointmentService
      {
         get
         {
            if (appointmentService == null)
               appointmentService = new System.Collections.Generic.List<AppointmentService>();
            return appointmentService;
         }
         set
         {
            RemoveAllAppointmentService();
            if (value != null)
            {
               foreach (Service.AppointmentService oAppointmentService in value)
                  AddAppointmentService(oAppointmentService);
            }
         }
      }
      
      /// <summary>
      /// Add a new Service.AppointmentService in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddAppointmentService(Service.AppointmentService newAppointmentService)
      {
         if (newAppointmentService == null)
            return;
         if (this.appointmentService == null)
            this.appointmentService = new System.Collections.Generic.List<AppointmentService>();
         if (!this.appointmentService.Contains(newAppointmentService))
            this.appointmentService.Add(newAppointmentService);
      }
      
      /// <summary>
      /// Remove an existing Service.AppointmentService from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveAppointmentService(Service.AppointmentService oldAppointmentService)
      {
         if (oldAppointmentService == null)
            return;
         if (this.appointmentService != null)
            if (this.appointmentService.Contains(oldAppointmentService))
               this.appointmentService.Remove(oldAppointmentService);
      }
      
      /// <summary>
      /// Remove all instances of Service.AppointmentService from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllAppointmentService()
      {
         if (appointmentService != null)
            appointmentService.Clear();
      }
   
   }
}
// File:    DoctorController.cs
// Author:  RIS
// Created: Friday, May 29, 2020 2:56:33 PM
// Purpose: Definition of Class DoctorController

using System;

namespace Controller
{
   public class DoctorController
   {
      public Model.ModelDTO.PrescriptionDTO GivePrescription(Model.ModelDTO.PrescriptionDTO perscription, Model.ModelDTO.DoctorDTO doctor)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.DiagnosisDTO GiveDiagnosis(Model.ModelDTO.DiagnosisDTO diagnosis, Model.ModelDTO.DoctorDTO doctor)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.EquipmentDTO SpentEquipment(Model.ModelDTO.EquipmentDTO equipment, Model.ModelDTO.DoctorDTO doctor)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.ExaminationReportDTO TakeAnamnesis(Model.ModelDTO.ExaminationReportDTO report, Model.ModelDTO.DoctorDTO doctor)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.ExaminationReportDTO EnterParametars(Model.ModelDTO.ExaminationReportDTO parametars, Model.ModelDTO.DoctorDTO doctor)
      {
         throw new NotImplementedException();
      }
      
      public Service.DoctorService doctorService;
   
   }
}
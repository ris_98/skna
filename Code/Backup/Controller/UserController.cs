// File:    UserController.cs
// Author:  RIS
// Created: Friday, May 29, 2020 2:56:31 PM
// Purpose: Definition of Class UserController

using System;

namespace Controller
{
   public class UserController
   {
      public Model.ModelDTO.ReportDTO LeaveReport(Model.ModelDTO.ReportDTO report)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.FeedbackDTO LeaveFeedback(Model.ModelDTO.FeedbackDTO feedback)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.UserDTO ChangePassword(Model.ModelDTO.UserDTO user, String password)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.UserDTO UpdateUser(Model.ModelDTO.UserDTO user)
      {
         throw new NotImplementedException();
      }
      
      public Service.UserService userService;
   
   }
}
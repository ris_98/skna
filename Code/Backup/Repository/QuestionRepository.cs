// File:    QuestionRepository.cs
// Author:  RIS
// Created: Friday, May 29, 2020 8:51:23 PM
// Purpose: Definition of Class QuestionRepository

using System;

namespace Repository
{
   public class QuestionRepository
   {
      private String path;
      
      public Model.Question GetQuestionById(int id)
      {
         throw new NotImplementedException();
      }
      
      public List<Question> GetAllQuestion()
      {
         throw new NotImplementedException();
      }
      
      public Model.Question DeleteQuestion(int id)
      {
         throw new NotImplementedException();
      }
      
      public Model.Question AddQuestion(Model.Question question)
      {
         throw new NotImplementedException();
      }
      
      public List<Question> GetByPatientId(int patientId)
      {
         throw new NotImplementedException();
      }
      
      public Model.Question UpdateQuestion(Model.Question question)
      {
         throw new NotImplementedException();
      }
   
   }
}
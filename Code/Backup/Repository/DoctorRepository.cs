// File:    DoctorRepository.cs
// Author:  RIS
// Created: Friday, May 29, 2020 4:08:58 PM
// Purpose: Definition of Class DoctorRepository

using System;

namespace Repository
{
   public class DoctorRepository
   {
      private String path;
      
      public Model.Doctor GetDoctorById(int id)
      {
         throw new NotImplementedException();
      }
      
      public List<Doctor> GetDoctorBySpecialization(String specialization)
      {
         throw new NotImplementedException();
      }
      
      public List<Doctor> GetDoctorByName(String name)
      {
         throw new NotImplementedException();
      }
      
      public List<Doctor> GetDoctorBySurname(String surname)
      {
         throw new NotImplementedException();
      }
      
      public List<Doctor> GetAllDoctor()
      {
         throw new NotImplementedException();
      }
      
      public Model.Doctor AddDoctor()
      {
         throw new NotImplementedException();
      }
      
      public Model.Doctor DeleteDoctor()
      {
         throw new NotImplementedException();
      }
   
   }
}
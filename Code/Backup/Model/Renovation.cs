// File:    Renovation.cs
// Author:  RIS
// Created: Tuesday, June 2, 2020 9:39:27 PM
// Purpose: Definition of Class Renovation

using System;

namespace Model
{
   public class Renovation
   {
      private int iD;
      
      public DataRange dataRange;
      public Room room;
      
      /// <summary>
      /// Property for Room
      /// </summary>
      /// <pdGenerated>Default opposite class property</pdGenerated>
      public Room Room
      {
         get
         {
            return room;
         }
         set
         {
            if (this.room == null || !this.room.Equals(value))
            {
               if (this.room != null)
               {
                  Room oldRoom = this.room;
                  this.room = null;
                  oldRoom.RemoveRenovation(this);
               }
               if (value != null)
               {
                  this.room = value;
                  this.room.AddRenovation(this);
               }
            }
         }
      }
   
   }
}
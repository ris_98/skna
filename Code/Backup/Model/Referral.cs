// File:    Referral.cs
// Author:  RIS
// Created: Friday, May 29, 2020 2:52:11 PM
// Purpose: Definition of Class Referral

using System;

namespace Model
{
   public class Referral
   {
      private int iD;
      private String type;
      
      public Doctor doctor;
      public MedicalRecord medicalRecord;
      
      /// <summary>
      /// Property for MedicalRecord
      /// </summary>
      /// <pdGenerated>Default opposite class property</pdGenerated>
      public MedicalRecord MedicalRecord
      {
         get
         {
            return medicalRecord;
         }
         set
         {
            if (this.medicalRecord == null || !this.medicalRecord.Equals(value))
            {
               if (this.medicalRecord != null)
               {
                  MedicalRecord oldMedicalRecord = this.medicalRecord;
                  this.medicalRecord = null;
                  oldMedicalRecord.RemoveReferral(this);
               }
               if (value != null)
               {
                  this.medicalRecord = value;
                  this.medicalRecord.AddReferral(this);
               }
            }
         }
      }
      public Appointment appointment;
      
      /// <summary>
      /// Property for Appointment
      /// </summary>
      /// <pdGenerated>Default opposite class property</pdGenerated>
      public Appointment Appointment
      {
         get
         {
            return appointment;
         }
         set
         {
            if (this.appointment == null || !this.appointment.Equals(value))
            {
               if (this.appointment != null)
               {
                  Appointment oldAppointment = this.appointment;
                  this.appointment = null;
                  oldAppointment.RemoveReferral(this);
               }
               if (value != null)
               {
                  this.appointment = value;
                  this.appointment.AddReferral(this);
               }
            }
         }
      }
   
   }
}
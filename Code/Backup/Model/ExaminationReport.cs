// File:    ExaminationReport.cs
// Author:  RIS
// Created: Friday, May 29, 2020 2:52:11 PM
// Purpose: Definition of Class ExaminationReport

using System;

namespace Model
{
   public class ExaminationReport
   {
      public System.Collections.Generic.List<Prescription> prescription;
      
      /// <summary>
      /// Property for collection of Prescription
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<Prescription> Prescription
      {
         get
         {
            if (prescription == null)
               prescription = new System.Collections.Generic.List<Prescription>();
            return prescription;
         }
         set
         {
            RemoveAllPrescription();
            if (value != null)
            {
               foreach (Prescription oPrescription in value)
                  AddPrescription(oPrescription);
            }
         }
      }
      
      /// <summary>
      /// Add a new Prescription in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddPrescription(Prescription newPrescription)
      {
         if (newPrescription == null)
            return;
         if (this.prescription == null)
            this.prescription = new System.Collections.Generic.List<Prescription>();
         if (!this.prescription.Contains(newPrescription))
            this.prescription.Add(newPrescription);
      }
      
      /// <summary>
      /// Remove an existing Prescription from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemovePrescription(Prescription oldPrescription)
      {
         if (oldPrescription == null)
            return;
         if (this.prescription != null)
            if (this.prescription.Contains(oldPrescription))
               this.prescription.Remove(oldPrescription);
      }
      
      /// <summary>
      /// Remove all instances of Prescription from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllPrescription()
      {
         if (prescription != null)
            prescription.Clear();
      }
      public System.Collections.Generic.List<Diagnosis> diagnosis;
      
      /// <summary>
      /// Property for collection of Diagnosis
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<Diagnosis> Diagnosis
      {
         get
         {
            if (diagnosis == null)
               diagnosis = new System.Collections.Generic.List<Diagnosis>();
            return diagnosis;
         }
         set
         {
            RemoveAllDiagnosis();
            if (value != null)
            {
               foreach (Diagnosis oDiagnosis in value)
                  AddDiagnosis(oDiagnosis);
            }
         }
      }
      
      /// <summary>
      /// Add a new Diagnosis in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddDiagnosis(Diagnosis newDiagnosis)
      {
         if (newDiagnosis == null)
            return;
         if (this.diagnosis == null)
            this.diagnosis = new System.Collections.Generic.List<Diagnosis>();
         if (!this.diagnosis.Contains(newDiagnosis))
            this.diagnosis.Add(newDiagnosis);
      }
      
      /// <summary>
      /// Remove an existing Diagnosis from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveDiagnosis(Diagnosis oldDiagnosis)
      {
         if (oldDiagnosis == null)
            return;
         if (this.diagnosis != null)
            if (this.diagnosis.Contains(oldDiagnosis))
               this.diagnosis.Remove(oldDiagnosis);
      }
      
      /// <summary>
      /// Remove all instances of Diagnosis from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllDiagnosis()
      {
         if (diagnosis != null)
            diagnosis.Clear();
      }
      public LabTestRequisition labTestRequisition;
      public Appointment appointment;
   
   }
}
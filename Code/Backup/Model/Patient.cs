// File:    Patient.cs
// Author:  RIS
// Created: Friday, May 29, 2020 2:52:11 PM
// Purpose: Definition of Class Patient

using System;

namespace Model
{
   public class Patient : User
   {
      public System.Collections.Generic.List<Appointment> appointment;
      
      /// <summary>
      /// Property for collection of Appointment
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<Appointment> Appointment
      {
         get
         {
            if (appointment == null)
               appointment = new System.Collections.Generic.List<Appointment>();
            return appointment;
         }
         set
         {
            RemoveAllAppointment();
            if (value != null)
            {
               foreach (Appointment oAppointment in value)
                  AddAppointment(oAppointment);
            }
         }
      }
      
      /// <summary>
      /// Add a new Appointment in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddAppointment(Appointment newAppointment)
      {
         if (newAppointment == null)
            return;
         if (this.appointment == null)
            this.appointment = new System.Collections.Generic.List<Appointment>();
         if (!this.appointment.Contains(newAppointment))
         {
            this.appointment.Add(newAppointment);
            newAppointment.Patient = this;
         }
      }
      
      /// <summary>
      /// Remove an existing Appointment from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveAppointment(Appointment oldAppointment)
      {
         if (oldAppointment == null)
            return;
         if (this.appointment != null)
            if (this.appointment.Contains(oldAppointment))
            {
               this.appointment.Remove(oldAppointment);
               oldAppointment.Patient = null;
            }
      }
      
      /// <summary>
      /// Remove all instances of Appointment from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllAppointment()
      {
         if (appointment != null)
         {
            System.Collections.ArrayList tmpAppointment = new System.Collections.ArrayList();
            foreach (Appointment oldAppointment in appointment)
               tmpAppointment.Add(oldAppointment);
            appointment.Clear();
            foreach (Appointment oldAppointment in tmpAppointment)
               oldAppointment.Patient = null;
            tmpAppointment.Clear();
         }
      }
      public System.Collections.Generic.List<Question> question;
      
      /// <summary>
      /// Property for collection of Question
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<Question> Question
      {
         get
         {
            if (question == null)
               question = new System.Collections.Generic.List<Question>();
            return question;
         }
         set
         {
            RemoveAllQuestion();
            if (value != null)
            {
               foreach (Question oQuestion in value)
                  AddQuestion(oQuestion);
            }
         }
      }
      
      /// <summary>
      /// Add a new Question in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddQuestion(Question newQuestion)
      {
         if (newQuestion == null)
            return;
         if (this.question == null)
            this.question = new System.Collections.Generic.List<Question>();
         if (!this.question.Contains(newQuestion))
         {
            this.question.Add(newQuestion);
            newQuestion.Patient = this;
         }
      }
      
      /// <summary>
      /// Remove an existing Question from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveQuestion(Question oldQuestion)
      {
         if (oldQuestion == null)
            return;
         if (this.question != null)
            if (this.question.Contains(oldQuestion))
            {
               this.question.Remove(oldQuestion);
               oldQuestion.Patient = null;
            }
      }
      
      /// <summary>
      /// Remove all instances of Question from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllQuestion()
      {
         if (question != null)
         {
            System.Collections.ArrayList tmpQuestion = new System.Collections.ArrayList();
            foreach (Question oldQuestion in question)
               tmpQuestion.Add(oldQuestion);
            question.Clear();
            foreach (Question oldQuestion in tmpQuestion)
               oldQuestion.Patient = null;
            tmpQuestion.Clear();
         }
      }
      public Room room;
   
   }
}
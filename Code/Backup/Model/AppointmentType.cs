// File:    AppointmentType.cs
// Author:  RIS
// Created: Friday, May 29, 2020 2:52:11 PM
// Purpose: Definition of Enum AppointmentType

using System;

namespace Model
{
   public enum AppointmentType
   {
      Examination,
      SpecialistExamination,
      Operation
   }
}
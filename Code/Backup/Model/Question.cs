// File:    Question.cs
// Author:  RIS
// Created: Friday, May 29, 2020 2:52:11 PM
// Purpose: Definition of Class Question

using System;

namespace Model
{
   public class Question
   {
      private int iD;
      private string questionContent;
      private int responderID;
      
      public Patient patient;
      
      /// <summary>
      /// Property for Patient
      /// </summary>
      /// <pdGenerated>Default opposite class property</pdGenerated>
      public Patient Patient
      {
         get
         {
            return patient;
         }
         set
         {
            if (this.patient == null || !this.patient.Equals(value))
            {
               if (this.patient != null)
               {
                  Patient oldPatient = this.patient;
                  this.patient = null;
                  oldPatient.RemoveQuestion(this);
               }
               if (value != null)
               {
                  this.patient = value;
                  this.patient.AddQuestion(this);
               }
            }
         }
      }
   
   }
}
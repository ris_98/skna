// File:    RoomType.cs
// Author:  RIS
// Created: Friday, May 29, 2020 2:52:11 PM
// Purpose: Definition of Enum RoomType

using System;

namespace Model
{
   public enum RoomType
   {
      Operation,
      Nursing,
      Recovery
   }
}
// File:    Headmaster.cs
// Author:  RIS
// Created: Friday, May 29, 2020 2:52:11 PM
// Purpose: Definition of Class Headmaster

using System;

namespace Model
{
   public class Headmaster : Employees
   {
      public System.Collections.Generic.List<Procurement> procurement;
      
      /// <summary>
      /// Property for collection of Procurement
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<Procurement> Procurement
      {
         get
         {
            if (procurement == null)
               procurement = new System.Collections.Generic.List<Procurement>();
            return procurement;
         }
         set
         {
            RemoveAllProcurement();
            if (value != null)
            {
               foreach (Procurement oProcurement in value)
                  AddProcurement(oProcurement);
            }
         }
      }
      
      /// <summary>
      /// Add a new Procurement in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddProcurement(Procurement newProcurement)
      {
         if (newProcurement == null)
            return;
         if (this.procurement == null)
            this.procurement = new System.Collections.Generic.List<Procurement>();
         if (!this.procurement.Contains(newProcurement))
            this.procurement.Add(newProcurement);
      }
      
      /// <summary>
      /// Remove an existing Procurement from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveProcurement(Procurement oldProcurement)
      {
         if (oldProcurement == null)
            return;
         if (this.procurement != null)
            if (this.procurement.Contains(oldProcurement))
               this.procurement.Remove(oldProcurement);
      }
      
      /// <summary>
      /// Remove all instances of Procurement from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllProcurement()
      {
         if (procurement != null)
            procurement.Clear();
      }
      public Hospital hospital;
   
   }
}
// File:    MedicalRecord.cs
// Author:  RIS
// Created: Friday, May 29, 2020 2:52:11 PM
// Purpose: Definition of Class MedicalRecord

using System;

namespace Model
{
   public class MedicalRecord
   {
      public System.Collections.Generic.List<ExaminationReport> examinationReport;
      
      /// <summary>
      /// Property for collection of ExaminationReport
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<ExaminationReport> ExaminationReport
      {
         get
         {
            if (examinationReport == null)
               examinationReport = new System.Collections.Generic.List<ExaminationReport>();
            return examinationReport;
         }
         set
         {
            RemoveAllExaminationReport();
            if (value != null)
            {
               foreach (ExaminationReport oExaminationReport in value)
                  AddExaminationReport(oExaminationReport);
            }
         }
      }
      
      /// <summary>
      /// Add a new ExaminationReport in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddExaminationReport(ExaminationReport newExaminationReport)
      {
         if (newExaminationReport == null)
            return;
         if (this.examinationReport == null)
            this.examinationReport = new System.Collections.Generic.List<ExaminationReport>();
         if (!this.examinationReport.Contains(newExaminationReport))
            this.examinationReport.Add(newExaminationReport);
      }
      
      /// <summary>
      /// Remove an existing ExaminationReport from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveExaminationReport(ExaminationReport oldExaminationReport)
      {
         if (oldExaminationReport == null)
            return;
         if (this.examinationReport != null)
            if (this.examinationReport.Contains(oldExaminationReport))
               this.examinationReport.Remove(oldExaminationReport);
      }
      
      /// <summary>
      /// Remove all instances of ExaminationReport from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllExaminationReport()
      {
         if (examinationReport != null)
            examinationReport.Clear();
      }
      public System.Collections.Generic.List<Referral> referral;
      
      /// <summary>
      /// Property for collection of Referral
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<Referral> Referral
      {
         get
         {
            if (referral == null)
               referral = new System.Collections.Generic.List<Referral>();
            return referral;
         }
         set
         {
            RemoveAllReferral();
            if (value != null)
            {
               foreach (Referral oReferral in value)
                  AddReferral(oReferral);
            }
         }
      }
      
      /// <summary>
      /// Add a new Referral in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddReferral(Referral newReferral)
      {
         if (newReferral == null)
            return;
         if (this.referral == null)
            this.referral = new System.Collections.Generic.List<Referral>();
         if (!this.referral.Contains(newReferral))
         {
            this.referral.Add(newReferral);
            newReferral.MedicalRecord = this;
         }
      }
      
      /// <summary>
      /// Remove an existing Referral from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveReferral(Referral oldReferral)
      {
         if (oldReferral == null)
            return;
         if (this.referral != null)
            if (this.referral.Contains(oldReferral))
            {
               this.referral.Remove(oldReferral);
               oldReferral.MedicalRecord = null;
            }
      }
      
      /// <summary>
      /// Remove all instances of Referral from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllReferral()
      {
         if (referral != null)
         {
            System.Collections.ArrayList tmpReferral = new System.Collections.ArrayList();
            foreach (Referral oldReferral in referral)
               tmpReferral.Add(oldReferral);
            referral.Clear();
            foreach (Referral oldReferral in tmpReferral)
               oldReferral.MedicalRecord = null;
            tmpReferral.Clear();
         }
      }
      public Person person;
   
   }
}
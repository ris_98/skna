// File:    EquipmentDTO.cs
// Author:  lazar
// Created: Sunday, June 21, 2020 8:55:45 PM
// Purpose: Definition of Class EquipmentDTO

using System;

namespace Model.ModelDTO
{
   public class EquipmentDTO
   {
      private int iD;
      private String name;
      private String description;
      private int quantity;
      private EquipmentType type;
   
   }
}
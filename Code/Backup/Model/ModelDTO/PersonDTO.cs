// File:    PersonDTO.cs
// Author:  lazar
// Created: Sunday, June 21, 2020 8:55:53 PM
// Purpose: Definition of Class PersonDTO

using System;

namespace Model.ModelDTO
{
   public class PersonDTO
   {
      protected int iD;
      protected String firstName;
      protected String lastName;
      protected String telephoneNumber;
      protected String jMBG;
   
   }
}
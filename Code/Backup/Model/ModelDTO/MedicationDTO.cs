// File:    MedicationDTO.cs
// Author:  lazar
// Created: Sunday, June 21, 2020 8:56:23 PM
// Purpose: Definition of Class MedicationDTO

using System;

namespace Model.ModelDTO
{
   public class MedicationDTO
   {
      private String iD;
      private String name;
      private Boolean approved;
      private int quantity;
   
   }
}
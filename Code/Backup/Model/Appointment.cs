// File:    Appointment.cs
// Author:  RIS
// Created: Friday, May 29, 2020 2:52:11 PM
// Purpose: Definition of Class Appointment

using System;

namespace Model
{
   public class Appointment
   {
      private DateTime dateTime;
      private AppointmentType appointmentType;
      
      public System.Collections.Generic.List<Referral> referral;
      
      /// <summary>
      /// Property for collection of Referral
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<Referral> Referral
      {
         get
         {
            if (referral == null)
               referral = new System.Collections.Generic.List<Referral>();
            return referral;
         }
         set
         {
            RemoveAllReferral();
            if (value != null)
            {
               foreach (Referral oReferral in value)
                  AddReferral(oReferral);
            }
         }
      }
      
      /// <summary>
      /// Add a new Referral in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddReferral(Referral newReferral)
      {
         if (newReferral == null)
            return;
         if (this.referral == null)
            this.referral = new System.Collections.Generic.List<Referral>();
         if (!this.referral.Contains(newReferral))
         {
            this.referral.Add(newReferral);
            newReferral.Appointment = this;
         }
      }
      
      /// <summary>
      /// Remove an existing Referral from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveReferral(Referral oldReferral)
      {
         if (oldReferral == null)
            return;
         if (this.referral != null)
            if (this.referral.Contains(oldReferral))
            {
               this.referral.Remove(oldReferral);
               oldReferral.Appointment = null;
            }
      }
      
      /// <summary>
      /// Remove all instances of Referral from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllReferral()
      {
         if (referral != null)
         {
            System.Collections.ArrayList tmpReferral = new System.Collections.ArrayList();
            foreach (Referral oldReferral in referral)
               tmpReferral.Add(oldReferral);
            referral.Clear();
            foreach (Referral oldReferral in tmpReferral)
               oldReferral.Appointment = null;
            tmpReferral.Clear();
         }
      }
      public ExaminationReport examinationReport;
      public Patient patient;
      
      /// <summary>
      /// Property for Patient
      /// </summary>
      /// <pdGenerated>Default opposite class property</pdGenerated>
      public Patient Patient
      {
         get
         {
            return patient;
         }
         set
         {
            if (this.patient == null || !this.patient.Equals(value))
            {
               if (this.patient != null)
               {
                  Patient oldPatient = this.patient;
                  this.patient = null;
                  oldPatient.RemoveAppointment(this);
               }
               if (value != null)
               {
                  this.patient = value;
                  this.patient.AddAppointment(this);
               }
            }
         }
      }
      public Doctor doctor;
      
      /// <summary>
      /// Property for Doctor
      /// </summary>
      /// <pdGenerated>Default opposite class property</pdGenerated>
      public Doctor Doctor
      {
         get
         {
            return doctor;
         }
         set
         {
            if (this.doctor == null || !this.doctor.Equals(value))
            {
               if (this.doctor != null)
               {
                  Doctor oldDoctor = this.doctor;
                  this.doctor = null;
                  oldDoctor.RemoveAppointment(this);
               }
               if (value != null)
               {
                  this.doctor = value;
                  this.doctor.AddAppointment(this);
               }
            }
         }
      }
   
   }
}
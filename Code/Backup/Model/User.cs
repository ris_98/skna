// File:    User.cs
// Author:  RIS
// Created: Friday, May 29, 2020 2:52:11 PM
// Purpose: Definition of Class User

using System;

namespace Model
{
   public class User : Person
   {
      private Boolean IsEmailValid()
      {
         throw new NotImplementedException();
      }
      
      private Boolean IsPasswordVaild()
      {
         throw new NotImplementedException();
      }
      
      protected String eMail;
      protected String password;
      
      public Boolean IsUserValid()
      {
         throw new NotImplementedException();
      }
      
      public System.Collections.Generic.List<Report> report;
      
      /// <summary>
      /// Property for collection of Report
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<Report> Report
      {
         get
         {
            if (report == null)
               report = new System.Collections.Generic.List<Report>();
            return report;
         }
         set
         {
            RemoveAllReport();
            if (value != null)
            {
               foreach (Report oReport in value)
                  AddReport(oReport);
            }
         }
      }
      
      /// <summary>
      /// Add a new Report in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddReport(Report newReport)
      {
         if (newReport == null)
            return;
         if (this.report == null)
            this.report = new System.Collections.Generic.List<Report>();
         if (!this.report.Contains(newReport))
            this.report.Add(newReport);
      }
      
      /// <summary>
      /// Remove an existing Report from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveReport(Report oldReport)
      {
         if (oldReport == null)
            return;
         if (this.report != null)
            if (this.report.Contains(oldReport))
               this.report.Remove(oldReport);
      }
      
      /// <summary>
      /// Remove all instances of Report from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllReport()
      {
         if (report != null)
            report.Clear();
      }
      public System.Collections.Generic.List<Feedback> feedback;
      
      /// <summary>
      /// Property for collection of Feedback
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<Feedback> Feedback
      {
         get
         {
            if (feedback == null)
               feedback = new System.Collections.Generic.List<Feedback>();
            return feedback;
         }
         set
         {
            RemoveAllFeedback();
            if (value != null)
            {
               foreach (Feedback oFeedback in value)
                  AddFeedback(oFeedback);
            }
         }
      }
      
      /// <summary>
      /// Add a new Feedback in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddFeedback(Feedback newFeedback)
      {
         if (newFeedback == null)
            return;
         if (this.feedback == null)
            this.feedback = new System.Collections.Generic.List<Feedback>();
         if (!this.feedback.Contains(newFeedback))
            this.feedback.Add(newFeedback);
      }
      
      /// <summary>
      /// Remove an existing Feedback from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveFeedback(Feedback oldFeedback)
      {
         if (oldFeedback == null)
            return;
         if (this.feedback != null)
            if (this.feedback.Contains(oldFeedback))
               this.feedback.Remove(oldFeedback);
      }
      
      /// <summary>
      /// Remove all instances of Feedback from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllFeedback()
      {
         if (feedback != null)
            feedback.Clear();
      }
   
   }
}
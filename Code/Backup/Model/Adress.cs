// File:    Adress.cs
// Author:  RIS
// Created: Friday, May 29, 2020 2:52:11 PM
// Purpose: Definition of Class Adress

using System;

namespace Model
{
   public class Adress
   {
      private String country;
      private String city;
      private String zipCode;
      private String street;
      
      public System.Collections.Generic.List<Person> person;
      
      /// <summary>
      /// Property for collection of Person
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<Person> Person
      {
         get
         {
            if (person == null)
               person = new System.Collections.Generic.List<Person>();
            return person;
         }
         set
         {
            RemoveAllPerson();
            if (value != null)
            {
               foreach (Person oPerson in value)
                  AddPerson(oPerson);
            }
         }
      }
      
      /// <summary>
      /// Add a new Person in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddPerson(Person newPerson)
      {
         if (newPerson == null)
            return;
         if (this.person == null)
            this.person = new System.Collections.Generic.List<Person>();
         if (!this.person.Contains(newPerson))
            this.person.Add(newPerson);
      }
      
      /// <summary>
      /// Remove an existing Person from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemovePerson(Person oldPerson)
      {
         if (oldPerson == null)
            return;
         if (this.person != null)
            if (this.person.Contains(oldPerson))
               this.person.Remove(oldPerson);
      }
      
      /// <summary>
      /// Remove all instances of Person from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllPerson()
      {
         if (person != null)
            person.Clear();
      }
      public Hospital hospital;
   
   }
}
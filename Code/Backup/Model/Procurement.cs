// File:    Procurement.cs
// Author:  RIS
// Created: Friday, May 29, 2020 2:52:11 PM
// Purpose: Definition of Class Procurement

using System;

namespace Model
{
   public class Procurement
   {
      private int iD;
      private DateTime date;
      private String note;
      
      public System.Collections.Generic.List<Medication> medication;
      
      /// <summary>
      /// Property for collection of Medication
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<Medication> Medication
      {
         get
         {
            if (medication == null)
               medication = new System.Collections.Generic.List<Medication>();
            return medication;
         }
         set
         {
            RemoveAllMedication();
            if (value != null)
            {
               foreach (Medication oMedication in value)
                  AddMedication(oMedication);
            }
         }
      }
      
      /// <summary>
      /// Add a new Medication in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddMedication(Medication newMedication)
      {
         if (newMedication == null)
            return;
         if (this.medication == null)
            this.medication = new System.Collections.Generic.List<Medication>();
         if (!this.medication.Contains(newMedication))
            this.medication.Add(newMedication);
      }
      
      /// <summary>
      /// Remove an existing Medication from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveMedication(Medication oldMedication)
      {
         if (oldMedication == null)
            return;
         if (this.medication != null)
            if (this.medication.Contains(oldMedication))
               this.medication.Remove(oldMedication);
      }
      
      /// <summary>
      /// Remove all instances of Medication from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllMedication()
      {
         if (medication != null)
            medication.Clear();
      }
      public System.Collections.Generic.List<Equipment> equipment;
      
      /// <summary>
      /// Property for collection of Equipment
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<Equipment> Equipment
      {
         get
         {
            if (equipment == null)
               equipment = new System.Collections.Generic.List<Equipment>();
            return equipment;
         }
         set
         {
            RemoveAllEquipment();
            if (value != null)
            {
               foreach (Equipment oEquipment in value)
                  AddEquipment(oEquipment);
            }
         }
      }
      
      /// <summary>
      /// Add a new Equipment in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddEquipment(Equipment newEquipment)
      {
         if (newEquipment == null)
            return;
         if (this.equipment == null)
            this.equipment = new System.Collections.Generic.List<Equipment>();
         if (!this.equipment.Contains(newEquipment))
            this.equipment.Add(newEquipment);
      }
      
      /// <summary>
      /// Remove an existing Equipment from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveEquipment(Equipment oldEquipment)
      {
         if (oldEquipment == null)
            return;
         if (this.equipment != null)
            if (this.equipment.Contains(oldEquipment))
               this.equipment.Remove(oldEquipment);
      }
      
      /// <summary>
      /// Remove all instances of Equipment from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllEquipment()
      {
         if (equipment != null)
            equipment.Clear();
      }
   
   }
}
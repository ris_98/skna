// File:    EquipmentType.cs
// Author:  lazar
// Created: Sunday, June 21, 2020 8:53:18 PM
// Purpose: Definition of Enum EquipmentType

using System;

namespace Model
{
   public enum EquipmentType
   {
      Stacionary,
      Consumable
   }
}
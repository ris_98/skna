// File:    Hospital.cs
// Author:  RIS
// Created: Friday, May 29, 2020 2:52:11 PM
// Purpose: Definition of Class Hospital

using System;

namespace Model
{
   public class Hospital
   {
      public Adress adress;
      public System.Collections.Generic.List<Person> person;
      
      /// <summary>
      /// Property for collection of Person
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<Person> Person
      {
         get
         {
            if (person == null)
               person = new System.Collections.Generic.List<Person>();
            return person;
         }
         set
         {
            RemoveAllPerson();
            if (value != null)
            {
               foreach (Person oPerson in value)
                  AddPerson(oPerson);
            }
         }
      }
      
      /// <summary>
      /// Add a new Person in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddPerson(Person newPerson)
      {
         if (newPerson == null)
            return;
         if (this.person == null)
            this.person = new System.Collections.Generic.List<Person>();
         if (!this.person.Contains(newPerson))
            this.person.Add(newPerson);
      }
      
      /// <summary>
      /// Remove an existing Person from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemovePerson(Person oldPerson)
      {
         if (oldPerson == null)
            return;
         if (this.person != null)
            if (this.person.Contains(oldPerson))
               this.person.Remove(oldPerson);
      }
      
      /// <summary>
      /// Remove all instances of Person from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllPerson()
      {
         if (person != null)
            person.Clear();
      }
      public System.Collections.Generic.List<Equipment> equipment;
      
      /// <summary>
      /// Property for collection of Equipment
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<Equipment> Equipment
      {
         get
         {
            if (equipment == null)
               equipment = new System.Collections.Generic.List<Equipment>();
            return equipment;
         }
         set
         {
            RemoveAllEquipment();
            if (value != null)
            {
               foreach (Equipment oEquipment in value)
                  AddEquipment(oEquipment);
            }
         }
      }
      
      /// <summary>
      /// Add a new Equipment in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddEquipment(Equipment newEquipment)
      {
         if (newEquipment == null)
            return;
         if (this.equipment == null)
            this.equipment = new System.Collections.Generic.List<Equipment>();
         if (!this.equipment.Contains(newEquipment))
            this.equipment.Add(newEquipment);
      }
      
      /// <summary>
      /// Remove an existing Equipment from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveEquipment(Equipment oldEquipment)
      {
         if (oldEquipment == null)
            return;
         if (this.equipment != null)
            if (this.equipment.Contains(oldEquipment))
               this.equipment.Remove(oldEquipment);
      }
      
      /// <summary>
      /// Remove all instances of Equipment from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllEquipment()
      {
         if (equipment != null)
            equipment.Clear();
      }
      public System.Collections.Generic.List<Question> question;
      
      /// <summary>
      /// Property for collection of Question
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<Question> Question
      {
         get
         {
            if (question == null)
               question = new System.Collections.Generic.List<Question>();
            return question;
         }
         set
         {
            RemoveAllQuestion();
            if (value != null)
            {
               foreach (Question oQuestion in value)
                  AddQuestion(oQuestion);
            }
         }
      }
      
      /// <summary>
      /// Add a new Question in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddQuestion(Question newQuestion)
      {
         if (newQuestion == null)
            return;
         if (this.question == null)
            this.question = new System.Collections.Generic.List<Question>();
         if (!this.question.Contains(newQuestion))
            this.question.Add(newQuestion);
      }
      
      /// <summary>
      /// Remove an existing Question from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveQuestion(Question oldQuestion)
      {
         if (oldQuestion == null)
            return;
         if (this.question != null)
            if (this.question.Contains(oldQuestion))
               this.question.Remove(oldQuestion);
      }
      
      /// <summary>
      /// Remove all instances of Question from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllQuestion()
      {
         if (question != null)
            question.Clear();
      }
   
   }
}
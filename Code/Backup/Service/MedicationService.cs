// File:    MedicationService.cs
// Author:  lazar
// Created: Sunday, June 21, 2020 10:42:22 PM
// Purpose: Definition of Class MedicationService

using System;

namespace Service
{
   public class MedicationService
   {
      private Model.Medication SendMedicationToVerify(Model.Medication medication)
      {
         throw new NotImplementedException();
      }
      
      public List<MedicationDTO> PrescribedMedication(Model.ModelDTO.PatientDTO patient)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.MedicationDTO AddMedication(Model.ModelDTO.MedicationDTO medication)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.MedicationDTO VerifyMedication(Model.ModelDTO.MedicationDTO medicine, Model.ModelDTO.DoctorDTO doctor)
      {
         throw new NotImplementedException();
      }
      
      public System.Collections.Generic.List<MedicationRepository> medicationRepository;
      
      /// <summary>
      /// Property for collection of Repository.MedicationRepository
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<MedicationRepository> MedicationRepository
      {
         get
         {
            if (medicationRepository == null)
               medicationRepository = new System.Collections.Generic.List<MedicationRepository>();
            return medicationRepository;
         }
         set
         {
            RemoveAllMedicationRepository();
            if (value != null)
            {
               foreach (Repository.MedicationRepository oMedicationRepository in value)
                  AddMedicationRepository(oMedicationRepository);
            }
         }
      }
      
      /// <summary>
      /// Add a new Repository.MedicationRepository in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddMedicationRepository(Repository.MedicationRepository newMedicationRepository)
      {
         if (newMedicationRepository == null)
            return;
         if (this.medicationRepository == null)
            this.medicationRepository = new System.Collections.Generic.List<MedicationRepository>();
         if (!this.medicationRepository.Contains(newMedicationRepository))
            this.medicationRepository.Add(newMedicationRepository);
      }
      
      /// <summary>
      /// Remove an existing Repository.MedicationRepository from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveMedicationRepository(Repository.MedicationRepository oldMedicationRepository)
      {
         if (oldMedicationRepository == null)
            return;
         if (this.medicationRepository != null)
            if (this.medicationRepository.Contains(oldMedicationRepository))
               this.medicationRepository.Remove(oldMedicationRepository);
      }
      
      /// <summary>
      /// Remove all instances of Repository.MedicationRepository from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllMedicationRepository()
      {
         if (medicationRepository != null)
            medicationRepository.Clear();
      }
   
   }
}
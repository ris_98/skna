// File:    LogInService.cs
// Author:  lazar
// Created: Monday, June 22, 2020 5:10:09 PM
// Purpose: Definition of Class LogInService

using System;

namespace Service
{
   public class LogInService
   {
      public Model.User SignIn(Model.User korisnik)
      {
         throw new NotImplementedException();
      }
      
      public Model.User SignOut(Model.User korisnik)
      {
         throw new NotImplementedException();
      }
      
      public System.Collections.Generic.List<UserRepository> userRepository;
      
      /// <summary>
      /// Property for collection of Repository.UserRepository
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<UserRepository> UserRepository
      {
         get
         {
            if (userRepository == null)
               userRepository = new System.Collections.Generic.List<UserRepository>();
            return userRepository;
         }
         set
         {
            RemoveAllUserRepository();
            if (value != null)
            {
               foreach (Repository.UserRepository oUserRepository in value)
                  AddUserRepository(oUserRepository);
            }
         }
      }
      
      /// <summary>
      /// Add a new Repository.UserRepository in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddUserRepository(Repository.UserRepository newUserRepository)
      {
         if (newUserRepository == null)
            return;
         if (this.userRepository == null)
            this.userRepository = new System.Collections.Generic.List<UserRepository>();
         if (!this.userRepository.Contains(newUserRepository))
            this.userRepository.Add(newUserRepository);
      }
      
      /// <summary>
      /// Remove an existing Repository.UserRepository from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveUserRepository(Repository.UserRepository oldUserRepository)
      {
         if (oldUserRepository == null)
            return;
         if (this.userRepository != null)
            if (this.userRepository.Contains(oldUserRepository))
               this.userRepository.Remove(oldUserRepository);
      }
      
      /// <summary>
      /// Remove all instances of Repository.UserRepository from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllUserRepository()
      {
         if (userRepository != null)
            userRepository.Clear();
      }
   
   }
}
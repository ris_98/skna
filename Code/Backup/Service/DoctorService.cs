// File:    DoctorService.cs
// Author:  RIS
// Created: Friday, May 29, 2020 3:48:51 PM
// Purpose: Definition of Class DoctorService

using System;

namespace Service
{
   public class DoctorService
   {
      public Model.ModelDTO.PrescriptionDTO GivePrescription(Model.ModelDTO.PrescriptionDTO perscription, Model.ModelDTO.DoctorDTO doctor)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.DiagnosisDTO GiveDiagnosis(Model.ModelDTO.DiagnosisDTO diagnosis, Model.ModelDTO.DoctorDTO doctor)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.EquipmentDTO SpentEquipment(Model.ModelDTO.EquipmentDTO equipment, Model.ModelDTO.DoctorDTO doctor)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.ExaminationReportDTO TakeAnamnesis(Model.ModelDTO.ExaminationReportDTO report, Model.ModelDTO.DoctorDTO doctor)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.ExaminationReportDTO EnterParametars(Model.ModelDTO.ExaminationReportDTO parametars, Model.ModelDTO.DoctorDTO doctor)
      {
         throw new NotImplementedException();
      }
      
      public Repository.DoctorRepository doctorRepository;
   
   }
}
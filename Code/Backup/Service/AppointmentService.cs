// File:    AppointmentService.cs
// Author:  Korisnik
// Created: Saturday, May 30, 2020 9:42:01 AM
// Purpose: Definition of Class AppointmentService

using System;

namespace Service
{
   public class AppointmentService
   {
      private Boolean IsFree(Model.Appointment appointment)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.AppointmentDTO ScheduleAppointment(Model.ModelDTO.AppointmentDTO appointment)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.AppointmentDTO ModifyAppointmentDate(Model.ModelDTO.AppointmentDTO appointment)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.AppointmentDTO ModifyAppointmentDoctor(Model.ModelDTO.AppointmentDTO appointment)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.AppointmentDTO CancelAppointment(Model.ModelDTO.AppointmentDTO appointment)
      {
         throw new NotImplementedException();
      }
      
      public List<AppointmentDTO> ScheduledAppointments(Model.ModelDTO.UserDTO user)
      {
         throw new NotImplementedException();
      }
      
      public Model.Appointment RecomendAppointment()
      {
         throw new NotImplementedException();
      }
      
      public System.Collections.Generic.List<ApoinmentRepository> apoinmentRepository;
      
      /// <summary>
      /// Property for collection of Repository.ApoinmentRepository
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<ApoinmentRepository> ApoinmentRepository
      {
         get
         {
            if (apoinmentRepository == null)
               apoinmentRepository = new System.Collections.Generic.List<ApoinmentRepository>();
            return apoinmentRepository;
         }
         set
         {
            RemoveAllApoinmentRepository();
            if (value != null)
            {
               foreach (Repository.ApoinmentRepository oApoinmentRepository in value)
                  AddApoinmentRepository(oApoinmentRepository);
            }
         }
      }
      
      /// <summary>
      /// Add a new Repository.ApoinmentRepository in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddApoinmentRepository(Repository.ApoinmentRepository newApoinmentRepository)
      {
         if (newApoinmentRepository == null)
            return;
         if (this.apoinmentRepository == null)
            this.apoinmentRepository = new System.Collections.Generic.List<ApoinmentRepository>();
         if (!this.apoinmentRepository.Contains(newApoinmentRepository))
            this.apoinmentRepository.Add(newApoinmentRepository);
      }
      
      /// <summary>
      /// Remove an existing Repository.ApoinmentRepository from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveApoinmentRepository(Repository.ApoinmentRepository oldApoinmentRepository)
      {
         if (oldApoinmentRepository == null)
            return;
         if (this.apoinmentRepository != null)
            if (this.apoinmentRepository.Contains(oldApoinmentRepository))
               this.apoinmentRepository.Remove(oldApoinmentRepository);
      }
      
      /// <summary>
      /// Remove all instances of Repository.ApoinmentRepository from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllApoinmentRepository()
      {
         if (apoinmentRepository != null)
            apoinmentRepository.Clear();
      }
   
   }
}
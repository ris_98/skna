// File:    QuestionService.cs
// Author:  lazar
// Created: Sunday, June 21, 2020 10:42:23 PM
// Purpose: Definition of Class QuestionService

using System;

namespace Service
{
   public class QuestionService
   {
      public Model.ModelDTO.QuestionDTO AskQuestion(Model.Question question)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.QuestionDTO DeleteQuestion(Model.Question question)
      {
         throw new NotImplementedException();
      }
      
      public Model.ModelDTO.QuestionDTO AnswerQuestion(Model.Question question)
      {
         throw new NotImplementedException();
      }
      
      public List<Question> ShowQuestions()
      {
         throw new NotImplementedException();
      }
      
      public Model.Question NotifyPatientWhenAnswered()
      {
         throw new NotImplementedException();
      }
      
      public System.Collections.Generic.List<QuestionRepository> questionRepository;
      
      /// <summary>
      /// Property for collection of Repository.QuestionRepository
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.Generic.List<QuestionRepository> QuestionRepository
      {
         get
         {
            if (questionRepository == null)
               questionRepository = new System.Collections.Generic.List<QuestionRepository>();
            return questionRepository;
         }
         set
         {
            RemoveAllQuestionRepository();
            if (value != null)
            {
               foreach (Repository.QuestionRepository oQuestionRepository in value)
                  AddQuestionRepository(oQuestionRepository);
            }
         }
      }
      
      /// <summary>
      /// Add a new Repository.QuestionRepository in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddQuestionRepository(Repository.QuestionRepository newQuestionRepository)
      {
         if (newQuestionRepository == null)
            return;
         if (this.questionRepository == null)
            this.questionRepository = new System.Collections.Generic.List<QuestionRepository>();
         if (!this.questionRepository.Contains(newQuestionRepository))
            this.questionRepository.Add(newQuestionRepository);
      }
      
      /// <summary>
      /// Remove an existing Repository.QuestionRepository from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveQuestionRepository(Repository.QuestionRepository oldQuestionRepository)
      {
         if (oldQuestionRepository == null)
            return;
         if (this.questionRepository != null)
            if (this.questionRepository.Contains(oldQuestionRepository))
               this.questionRepository.Remove(oldQuestionRepository);
      }
      
      /// <summary>
      /// Remove all instances of Repository.QuestionRepository from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllQuestionRepository()
      {
         if (questionRepository != null)
            questionRepository.Clear();
      }
   
   }
}
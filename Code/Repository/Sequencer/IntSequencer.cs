﻿using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository.Sequencer
{
    public class IntSequencer : ISequencer<int>
    {
        private int _nextId;

        public int GenerateId()
        {
            _nextId = _nextId + 2;
            return _nextId;
        }

        public void Initialize(int initId) => _nextId = initId;
    }
}

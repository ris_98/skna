using System.Collections.Generic;
using System.Linq;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository

{
    public class EquipmentRepository : CSVRepository<Equipment, int>, IEquipmentRepository, IEagerCSVRepository<Equipment, int>
    {
        private const string ENTITY_NAME = "Equipment";
        private const string NOT_UNIQUE_ERROR = "Equipment id {0} is not unique!";

        public EquipmentRepository(ICSVStream<Equipment> stream, ISequencer<int> sequencer)
            : base(ENTITY_NAME, stream, sequencer)
        {
        }
        public IEnumerable<Equipment> GetAll() 
        {
            return base.GetAll();
        }

        public Equipment GetById(int id) => Get(id);

        public new Equipment Create(Equipment equipment)
        {
            return base.Create(equipment);
        }

        public void Delete(Equipment equipment)
        {
            base.Delete(equipment);
        }

        public void Update(Equipment equipment)
        {
            base.Update(equipment);
        }

        public IEnumerable<Equipment> GetAllStacionaryEquipment()
            => (_stream.ReadAll().Where(equipment => equipment.Type == EquipmentType.Stacionary)).ToList();

        public IEnumerable<Equipment> GetAllConsumableEquipment()
         => (_stream.ReadAll().Where(equipment => equipment.Type == EquipmentType.Consumable)).ToList();

    }
}
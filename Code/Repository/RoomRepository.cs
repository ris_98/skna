using System.Collections.Generic;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository
{
    public class RoomRepository : CSVRepository<Room, int>, IRoomRepository, IEagerCSVRepository<Room, int>
    {
        private const string ENTITY_NAME = "Room";
        private const string NOT_UNIQUE_ERROR = "Room id {0} is not unique!";

        public RoomRepository(ICSVStream<Room> stream, ISequencer<int> sequencer)
            : base(ENTITY_NAME, stream, sequencer)
        {
        }
        public IEnumerable<Room> GetAll()
        {
            return base.GetAll();
        }

        public Room GetById(int id) => Get(id);

        public new Room Create(Room room)
        {
            return base.Create(room);
        }

        public void Delete(Room room)
        {
            base.Delete(room);
        }

        public void Update(Room room)
        {
            base.Update(room);
        }
    }
}
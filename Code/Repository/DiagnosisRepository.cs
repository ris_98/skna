﻿using System.Collections.Generic;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository

{
    public class DiagnosisRepository : CSVRepository<Diagnosis, int>, IDiagnosisRepository, IEagerCSVRepository<Diagnosis, int>
    {
        private const string ENTITY_NAME = "Diagnosis";
        private const string NOT_UNIQUE_ERROR = "Diagnosis id {0} is not unique!";

        public DiagnosisRepository(ICSVStream<Diagnosis> stream, ISequencer<int> sequencer)
            : base(ENTITY_NAME, stream, sequencer)
        {
        }
        public IEnumerable<Diagnosis> GetAll() => GetAll();

        public Diagnosis GetById(int id) => Get(id);

        public new Diagnosis Create(Diagnosis diagnosis)
        {
            return base.Create(diagnosis);
        }

        public void Delete(Diagnosis diagnosis)
        {
            base.Delete(diagnosis);
        }

        public void Update(Diagnosis diagnosis)
        {
            base.Update(diagnosis);
        }


    }
}
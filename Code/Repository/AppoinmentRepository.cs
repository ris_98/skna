using System;
using System.Collections.Generic;
using System.Linq;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository

{
    public class AppointmentRepository : CSVRepository<Appointment, int>, IAppointmentRepository, IEagerCSVRepository<Appointment, int>
    {
        private const string ENTITY_NAME = "Appointment";
        private const string NOT_UNIQUE_ERROR = "Appointment id {0} is not unique!";

        public AppointmentRepository(ICSVStream<Appointment> stream, ISequencer<int> sequencer)
            : base(ENTITY_NAME, stream, sequencer)
        {
        }
        public IEnumerable<Appointment> GetAll()
        {
            return base.GetAll();
        }

        public Appointment GetById(int id) => Get(id);

        public new Appointment Create(Appointment appointment)
        {
            return base.Create(appointment);
        }

        public void Delete(Appointment appointment)
        {
            base.Delete(appointment);
        }

        public void Update(Appointment appointment)
        {
            base.Update(appointment);
        }

        public Appointment GetAppointmentByDoctorAndTime(int  id, DateTime dateTime)
            => _stream.ReadAll().SingleOrDefault(appointment => appointment.Doctor.ID == id & CompareDate(appointment.DateTime,dateTime));

        public List<Appointment> GetAppointmentByDoctor(int id)
            => (_stream.ReadAll().Where(appointment => appointment.Doctor.ID == id)).ToList();
        public List<Appointment> GetAppointmentByPatient(int id)
            => (_stream.ReadAll().Where(appointment => appointment.Patient.ID == id)).ToList();

        public List<Appointment> GetAppointmentByDay(DateTime dateTime)
            => (_stream.ReadAll().Where(appointment => !CompareDate(appointment.DateTime.Date, dateTime.Date))).ToList();

        private bool CompareDate(DateTime d1, DateTime d2)
        {
            if(DateTime.Compare(d1,d2)!=0)
                return true;
            return false;
        }

    }
}
using System.Collections.Generic;
using System.Linq;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository

{
    public class PatientRepository : CSVRepository<Patient, int>, IPatientRepository, IEagerCSVRepository<Patient, int>
    {
        private const string ENTITY_NAME = "Patient";
        private const string NOT_UNIQUE_ERROR = "Patient id {0} is not unique!";

        public PatientRepository(ICSVStream<Patient> stream, ISequencer<int> sequencer)
            : base(ENTITY_NAME, stream, sequencer)
        {
        }
        public IEnumerable<Patient> GetAll()
        {
            return base.GetAll();

        }

        public Patient GetById(int id) => Get(id);

        public new Patient Create(Patient patient)
        {
            IsEmailUnique(patient.EMail);
            IsUsernameUnique(patient.Username);
            return base.Create(patient);
        }

        public new DoctorFeedback CreateDoctorFeedback(DoctorFeedback doctorFeedback)
        {
            return doctorFeedback;
            //return base.CreateDoctorFeedback(doctorFeedback);
        }
        private bool IsEmailUnique(string eMail)
            => GetPatientByeMail(eMail) == null;

        private Patient GetPatientByeMail(string eMail)
             => _stream.ReadAll().SingleOrDefault(patinet => patinet.EMail.Equals(eMail));

        private bool IsUsernameUnique(string username)
            => GetPatientByUsername(username) == null;

        private Patient GetPatientByUsername(string username)
             => _stream.ReadAll().SingleOrDefault(patinet => patinet.Username.Equals(username));



        public void Delete(Patient patient)
        {
            base.Delete(patient);
        }

        public void Update(Patient patient)
        {
            base.Update(patient);
        }

        public Patient GetPatinetByUserNameAndPassword(string username, string password)
    => _stream.ReadAll().SingleOrDefault(patient => patient.Username.Equals(username) & patient.Password.Equals(password));



    }
}
﻿using System.Collections.Generic;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository

{
    public class LabTestRequisitionRepository : CSVRepository<LabTestRequisition, int>, ILabTestRequisitionRepository, IEagerCSVRepository<LabTestRequisition, int>
    {
        private const string ENTITY_NAME = "LabTestRequisition";
        private const string NOT_UNIQUE_ERROR = "LabTestRequisition id {0} is not unique!";

        public LabTestRequisitionRepository(ICSVStream<LabTestRequisition> stream, ISequencer<int> sequencer)
            : base(ENTITY_NAME, stream, sequencer)
        {
        }
        public IEnumerable<LabTestRequisition> GetAll() => GetAll();

        public LabTestRequisition GetById(int id) => Get(id);

        public new LabTestRequisition Create(LabTestRequisition labTestRequisition)
        {
            return base.Create(labTestRequisition);
        }

        public void Delete(LabTestRequisition labTestRequisition)
        {
            base.Delete(labTestRequisition);
        }

        public void Update(LabTestRequisition labTestRequisition)
        {
            base.Update(labTestRequisition);
        }


    }
}
using System.Collections.Generic;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository
{
    public class QuestionRepository : CSVRepository<Question, int>, IQuestionRepository, IEagerCSVRepository<Question, int>
    {
        private const string ENTITY_NAME = "Question";
        private const string NOT_UNIQUE_ERROR = "Question id {0} is not unique!";

        public QuestionRepository(ICSVStream<Question> stream, ISequencer<int> sequencer)
            : base(ENTITY_NAME, stream, sequencer)
        {
        }
        public IEnumerable<Question> GetAll()
        {
            return base.GetAll();
        }

        public Question GetById(int id) => Get(id);

        public new Question Create(Question question)
        {
            return base.Create(question);
        }

        public void Delete(Question question)
        {
            base.Delete(question);
        }

        public void Update(Question question)
        {
            base.Update(question);
        }
    }
}
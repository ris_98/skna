using System.Collections.Generic;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository
{
    public class ProcurementRepository : CSVRepository<Procurement,int>, IProcurementRepository, IEagerCSVRepository<Procurement, int>
    {
        private const string ENTITY_NAME = "Procurement";
        private const string NOT_UNIQUE_ERROR = "Procurment id {0} is not unique!";

        public ProcurementRepository(ICSVStream<Procurement> stream, ISequencer<int> sequencer)
            : base(ENTITY_NAME, stream, sequencer)
        {
        }
        public IEnumerable<Procurement> GetAll() => GetAll();

        public Procurement GetById(int id) => Get(id);

        public new Procurement Create(Procurement procurement)
        {
            return base.Create(procurement);
        }

        public void Delete(Procurement procurement)
        {
            base.Delete(procurement);
        }

        public void Update(Procurement procurement)
        {
            base.Update(procurement);
        }
    }
}
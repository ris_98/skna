using System;
using System.Collections.Generic;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository

{
    public class PersonRepository : CSVRepository<Person, int>, IPersonRepository, IEagerCSVRepository<Person, int>
    {
        private const string ENTITY_NAME = "Person";
        private const string NOT_UNIQUE_ERROR = "Person id {0} is not unique!";

        public PersonRepository(ICSVStream<Person> stream, ISequencer<int> sequencer)
            : base(ENTITY_NAME, stream, sequencer)
        {
        }
        public IEnumerable<Person> GetAll()
        {
            return base.GetAll();
        }

        public Person GetById(int id) => Get(id);

        public new Person Create(Person person)
        {
            return base.Create(person);
        }

        public void Delete(Person person)
        {
            base.Delete(person);
        }

        public void Update(Person person)
        {
            base.Update(person);
        }

        Person IRepository<Person, int>.Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Person> Find(Func<Person, bool> predicate)
        {
            throw new NotImplementedException();
        }
    }
}
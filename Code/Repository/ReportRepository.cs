using System.Collections.Generic;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository
{
    public class ReportRepository : CSVRepository<Report, int>, IReportRepository, IEagerCSVRepository<Report, int>
    {
        private const string ENTITY_NAME = "Report";
        private const string NOT_UNIQUE_ERROR = "Report id {0} is not unique!";

        public ReportRepository(ICSVStream<Report> stream, ISequencer<int> sequencer)
            : base(ENTITY_NAME, stream, sequencer)
        {
        }
        public IEnumerable<Report> GetAll() => GetAll();

        public Report GetById(int id) => Get(id);

        public new Report Create(Report report)
        {
            return base.Create(report);
        }

        public void Delete(Report report)
        {
            base.Delete(report);
        }

        public void Update(Report report)
        {
            base.Update(report);
        }
    }
}
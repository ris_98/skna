﻿using System.Collections.Generic;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository

{
    public class DoctorFeedbackRepository : CSVRepository<DoctorFeedback, int>, IDoctorFeedbackRepository, IEagerCSVRepository<DoctorFeedback, int>
    {
        private const string ENTITY_NAME = "DoctorFeedback";
        private const string NOT_UNIQUE_ERROR = "DoctorFeedback id {0} is not unique!";

        public DoctorFeedbackRepository(ICSVStream<DoctorFeedback> stream, ISequencer<int> sequencer)
            : base(ENTITY_NAME, stream, sequencer)
        {
        }
        public IEnumerable<DoctorFeedback> GetAll() => GetAll();

        public DoctorFeedback GetById(int id) => Get(id);

        public new DoctorFeedback Create(DoctorFeedback doctorFeedback)
        {
            return base.Create(doctorFeedback);
        }

        public void Delete(DoctorFeedback doctorFeedback)
        {
            base.Delete(doctorFeedback);
        }

        public void Update(DoctorFeedback doctorFeedback)
        {
            base.Update(doctorFeedback);
        }


    }
}
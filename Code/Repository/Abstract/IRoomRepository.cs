﻿using Bolnica.Model;
using Bolnica.Repository.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolnica.Repository.Abstract
{
    public interface IRoomRepository : IRepository<Room, int>
    {

    }
}

﻿using Bolnica.Model;
using Bolnica;

namespace Bolnica.Repository.Abstract
{
    public interface IHeadmasterRepository : IRepository<Headmaster, int>
    {

    }
}


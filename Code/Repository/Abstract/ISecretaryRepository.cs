﻿using Bolnica.Model;
using Bolnica;

namespace Bolnica.Repository.Abstract
{
    public interface ISecretaryRepository : IRepository<Secretary, int>
    {

    }
}

﻿using Bolnica.Model;

namespace Bolnica.Repository.Abstract
{
    public interface IEquipmentRepository : IRepository<Equipment, int>
    {

    }
}

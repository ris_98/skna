﻿using Bolnica.Model;
using Bolnica;

namespace Bolnica.Repository.Abstract
{
    public interface IProcurementRepository : IRepository<Procurement, int>
    {

    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bolnica.Model;

namespace Bolnica.Repository.Abstract
{
    public interface IDoctorRepository : IRepository<Doctor, int>
    {

    }
}

﻿using Bolnica.Model;
using Bolnica;

namespace Bolnica.Repository.Abstract
{
    public interface IPrescriptionRepository : IRepository<Prescription, int>
    {

    }
}

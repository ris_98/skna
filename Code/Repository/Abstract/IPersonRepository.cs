﻿using Bolnica.Model;
using Bolnica;

namespace Bolnica.Repository.Abstract
{
    public interface IPersonRepository : IRepository<Person, int>
    {

    }
}
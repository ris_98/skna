using System;
using System.Collections.Generic;
using System.Linq;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository
{
    public class SecretaryRepository : CSVRepository<Secretary, int>, ISecretaryRepository, IEagerCSVRepository<Secretary, int>
    {
        private const string ENTITY_NAME = "Procurement";
        private const string NOT_UNIQUE_ERROR = "Procurment id {0} is not unique!";

        public SecretaryRepository(ICSVStream<Secretary> stream, ISequencer<int> sequencer)
            : base(ENTITY_NAME, stream, sequencer)
        {
        }
        public IEnumerable<Secretary> GetAll() => GetAll();

        public Secretary GetById(int id) => Get(id);

        public new Secretary Create(Secretary secretary)
        {
            IsEmailUnique(secretary.EMail);
            IsUsernameUnique(secretary.Username);
            return base.Create(secretary);
        }
        private bool IsEmailUnique(string eMail)
            => GetSecretaryByeMail(eMail) == null;

        private Secretary GetSecretaryByeMail(string eMail)
             => _stream.ReadAll().SingleOrDefault(patinet => patinet.EMail.Equals(eMail));

        private bool IsUsernameUnique(string username)
            => GetSecretaryByUsername(username) == null;

        private Secretary GetSecretaryByUsername(string username)
             => _stream.ReadAll().SingleOrDefault(secretary => secretary.Username.Equals(username));

        public void Delete(Secretary secretary)
        {
            base.Delete(secretary);
        }

        public void Update(Secretary secretary)
        {
            base.Update(secretary);
        }


        public new IEnumerable<Secretary> Find(Func<Secretary, bool> predicate) => GetAll().Where(predicate);

        public Secretary GetSecretaryByUserNameAndPassword(string username, string password)
            => _stream.ReadAll().SingleOrDefault(secretary => secretary.Username.Equals(username) & secretary.Password.Equals(password));

    }
}
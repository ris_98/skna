using System.Collections.Generic;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository
{
    public class AdressRepository : CSVRepository<Adress, int>, IAdressRepository, IEagerCSVRepository<Adress, int>
    {
        private const string ENTITY_NAME = "Adress";
        private const string NOT_UNIQUE_ERROR = "Adress id {0} is not unique!";

        public AdressRepository(ICSVStream<Adress> stream, ISequencer<int> sequencer)
            : base(ENTITY_NAME, stream, sequencer)
        {
        }
        public IEnumerable<Adress> GetAll() => GetAll();

        public Adress GetById(int id) => Get(id);

        public new Adress Create(Adress adress)
        {
            return base.Create(adress);
        }

        public void Delete(Adress adress)
        {
            base.Delete(adress);
        }

        public void Update(Adress adress)
        {
            base.Update(adress);
        }
    }
}
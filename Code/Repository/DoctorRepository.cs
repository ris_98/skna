using System.Collections.Generic;
using System.Linq;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository

{
    public class DoctorRepository : CSVRepository<Doctor, int>, IDoctorRepository, IEagerCSVRepository<Doctor, int>
    {
        private const string ENTITY_NAME = "Doctor";
        private const string NOT_UNIQUE_ERROR = "Doctor id {0} is not unique!";

        public DoctorRepository(ICSVStream<Doctor> stream, ISequencer<int> sequencer)
            : base(ENTITY_NAME, stream, sequencer)
        {
        }
        public IEnumerable<Doctor> GetAlls() => GetAll();

        public IEnumerable<Doctor> GetAllBySpecialization(string specialization)
            => (_stream.ReadAll().Where(doctor => doctor.specialization == specialization)).ToList();

        public Doctor GetById(int id) => Get(id);

        public new Doctor Create(Doctor patient)
        {
            IsEmailUnique(patient.EMail);
            IsUsernameUnique(patient.Username);
            return base.Create(patient);
        }
        private bool IsEmailUnique(string eMail)
            => GetDoctorByeMail(eMail) == null;

        private Doctor GetDoctorByeMail(string eMail)
             => _stream.ReadAll().SingleOrDefault(doctor => doctor.EMail.Equals(eMail));

        private bool IsUsernameUnique(string username)
            => GetDoctorByUsername(username) == null;

        private Doctor GetDoctorByUsername(string username)
             => _stream.ReadAll().SingleOrDefault(doctor => doctor.Username.Equals(username));

        public void Delete(Doctor doctor)
        {
            base.Delete(doctor);
        }

        public void Update(Doctor doctor)
        {
            base.Update(doctor);
        }

        public Doctor GetDoctorByUserNameAndPassword(string username, string password)
    => _stream.ReadAll().SingleOrDefault(doctor => doctor.Username.Equals(username) & doctor.Password.Equals(password));
    }
}
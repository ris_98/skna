using System.Collections.Generic;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository

{
    public class PrescriptionRepository : CSVRepository<Prescription, int>, IPrescriptionRepository, IEagerCSVRepository<Prescription, int>
    {
        private const string ENTITY_NAME = "Prescription";
        private const string NOT_UNIQUE_ERROR = "Prescription id {0} is not unique!";

        public PrescriptionRepository(ICSVStream<Prescription> stream, ISequencer<int> sequencer)
            : base(ENTITY_NAME, stream, sequencer)
        {
        }
        public IEnumerable<Prescription> GetAlls() => GetAll();

        public Prescription GetById(int id) => Get(id);

        public new Prescription Create(Prescription prescription)
        {
            return base.Create(prescription);
        }

        public void Delete(Prescription prescription)
        {
            base.Delete(prescription);
        }

        public void Update(Prescription prescription)
        {
            base.Update(prescription);
        }


    }
}
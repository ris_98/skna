﻿using System.Collections.Generic;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository

{
    public class ExaminationReportRepository : CSVRepository<ExaminationReport, int>, IExaminationReportRepository, IEagerCSVRepository<ExaminationReport, int>
    {
        private const string ENTITY_NAME = "Examination";
        private const string NOT_UNIQUE_ERROR = "Examination id {0} is not unique!";

        public ExaminationReportRepository(ICSVStream<ExaminationReport> stream, ISequencer<int> sequencer)
            : base(ENTITY_NAME, stream, sequencer)
        {
        }
        public IEnumerable<ExaminationReport> GetAll() => GetAll();

        public ExaminationReport GetById(int id) => Get(id);

        public new ExaminationReport Create(ExaminationReport examinationReport)
        {
            return base.Create(examinationReport);
        }

        public void Delete(ExaminationReport examinationReport)
        {
            base.Delete(examinationReport);
        }

        public void Update(ExaminationReport examinationReport)
        {
            base.Update(examinationReport);
        }


    }
}
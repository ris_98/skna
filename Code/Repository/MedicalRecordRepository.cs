﻿using System.Collections.Generic;
using System.Linq;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository

{
    public class MedicalRecordRepository : CSVRepository<MedicalRecord, int>, IMedicalRecordRepository, IEagerCSVRepository<MedicalRecord, int>
    {
        private const string ENTITY_NAME = "MedicalRecord";
        private const string NOT_UNIQUE_ERROR = "MedicalRecord id {0} is not unique!";

        public MedicalRecordRepository(ICSVStream<MedicalRecord> stream, ISequencer<int> sequencer)
            : base(ENTITY_NAME, stream, sequencer)
        {
        }
        public IEnumerable<MedicalRecord> GetAll() => GetAll();

        public MedicalRecord GetById(int id) => Get(id);

        public new MedicalRecord Create(MedicalRecord medicalRecord)
        {
            return base.Create(medicalRecord);
        }

        public void Delete(MedicalRecord medicalRecord)
        {
            base.Delete(medicalRecord);
        }

        public void Update(MedicalRecord medicalRecord)
        {
            base.Update(medicalRecord);
        }

        public List<MedicalRecord> GetMedicalRecordByPatient(int id)
            => (_stream.ReadAll().Where(medicalRecord => medicalRecord.Appointment.Patient.ID == id)).ToList();


    }
}
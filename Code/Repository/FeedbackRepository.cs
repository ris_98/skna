using System.Collections.Generic;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository

{
    public class FeedbackRepository : CSVRepository<Feedback, int>, IFeedbackRepository, IEagerCSVRepository<Feedback, int>
    {
        private const string ENTITY_NAME = "Feedback";
        private const string NOT_UNIQUE_ERROR = "Feedback id {0} is not unique!";

        public FeedbackRepository(ICSVStream<Feedback> stream, ISequencer<int> sequencer)
            : base(ENTITY_NAME, stream, sequencer)
        {
        }
        public IEnumerable<Feedback> GetAll() => GetAll();

        public Feedback GetById(int id) => Get(id);

        public new Feedback Create(Feedback feedback)
        {
            return base.Create(feedback);
        }

        public void Delete(Feedback feedback)
        {
            base.Delete(feedback);
        }

        public void Update(Feedback feedback)
        {
            base.Update(feedback);
        }


    }
}
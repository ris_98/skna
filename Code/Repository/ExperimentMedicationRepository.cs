﻿using System.Collections.Generic;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository

{
    public class ExperimentMedicationRepository : CSVRepository<NotVerifiedMedication, int>, IExperimentMedicationRepository, IEagerCSVRepository<NotVerifiedMedication, int>
    {
        private const string ENTITY_NAME = "ExperimentMedication";
        private const string NOT_UNIQUE_ERROR = "ExperimentMedication id {0} is not unique!";

        public ExperimentMedicationRepository(ICSVStream<NotVerifiedMedication> stream, ISequencer<int> sequencer)
            : base(ENTITY_NAME, stream, sequencer)
        {
        }
        public IEnumerable<NotVerifiedMedication> GetAlls() => GetAll();


        public NotVerifiedMedication GetById(int id) => Get(id);

        public new NotVerifiedMedication Create(NotVerifiedMedication medication)
        {
            return base.Create(medication);
        }

        public void Delete(NotVerifiedMedication medication)
        {
            base.Delete(medication);
        }

        public void Update(NotVerifiedMedication medication)
        {
            base.Update(medication);
        }


    }
}
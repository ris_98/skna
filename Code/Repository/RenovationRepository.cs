using System.Collections.Generic;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository
{
    public class RenovationRepository : CSVRepository<Renovation, int>, IRenovationRepository, IEagerCSVRepository<Renovation, int>
    {
        private const string ENTITY_NAME = "Renovation";
        private const string NOT_UNIQUE_ERROR = "Renovation id {0} is not unique!";

        public RenovationRepository(ICSVStream<Renovation> stream, ISequencer<int> sequencer)
            : base(ENTITY_NAME, stream, sequencer)
        {
        }
        public IEnumerable<Renovation> GetAll() => GetAll();

        public Renovation GetById(int id) => Get(id);

        public new Renovation Create(Renovation renovation)
        {
            return base.Create(renovation);
        }

        public void Delete(Renovation renovation)
        {
            base.Delete(renovation);
        }

        public void Update(Renovation renovation)
        {
            base.Update(renovation);
        }
    }
}
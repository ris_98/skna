using System.Collections.Generic;
using System.Linq;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository

{
    public class HeadmasterRepository : CSVRepository<Headmaster, int>, IHeadmasterRepository, IEagerCSVRepository<Headmaster, int>
    {
        private const string ENTITY_NAME = "Headmaster";
        private const string NOT_UNIQUE_ERROR = "Headmaster id {0} is not unique!";

        public HeadmasterRepository(ICSVStream<Headmaster> stream, ISequencer<int> sequencer)
            : base(ENTITY_NAME, stream, sequencer)
        {
        }
        public IEnumerable<Headmaster> GetAll() => GetAll();

        public Headmaster GetById(int id) => Get(id);

        public new Headmaster Create(Headmaster patient)
        {
            IsEmailUnique(patient.EMail);
            IsUsernameUnique(patient.Username);
            return base.Create(patient);
        }
        private bool IsEmailUnique(string eMail)
            => GetHeadmasterByeMail(eMail) == null;

        private Headmaster GetHeadmasterByeMail(string eMail)
             => _stream.ReadAll().SingleOrDefault(headmaster => headmaster.EMail.Equals(eMail));

        private bool IsUsernameUnique(string username)
            => GetHeadmasterByUsername(username) == null;

        private Headmaster GetHeadmasterByUsername(string username)
             => _stream.ReadAll().SingleOrDefault(headmaster => headmaster.Username.Equals(username));

        public void Delete(Headmaster headmaster)
        {
            base.Delete(headmaster);
        }

        public void Update(Headmaster headmaster)
        {
            base.Update(headmaster);
        }

        public Headmaster GetHeadmasterByUserNameAndPassword(string username, string password)
    => _stream.ReadAll().SingleOrDefault(headmaster => headmaster.Username.Equals(username) & headmaster.Password.Equals(password));

    }
}
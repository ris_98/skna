﻿using Bolnica.Repository.CSV.Converter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Linq;

namespace Bolnica.Repository.CSV.Stream
{
    public class CSVStream<E> : ICSVStream<E> where E : class
    {
        private readonly string Path;
        private readonly ICSVConverter<E> Converter;

        public CSVStream(string path, ICSVConverter<E> converter)
        {
            Path = path;
            Converter = converter;
        }

        public void AppendToFIle(E entity) 
            => File.AppendAllText(Path,Converter.ConvertEntityToCSVFormat(entity) + Environment.NewLine);

        public IEnumerable<E> ReadAll()
            => File.ReadAllLines(Path).Select(Converter.ConvertCSVFormatToEntity).ToList();

        public void SaveAll(IEnumerable<E> entities)
            => WriteAllLinesToFile(entities.Select(Converter.ConvertEntityToCSVFormat).ToList());

        public void WriteAllLinesToFile(IEnumerable<string> content) => File.WriteAllLines(Path, content.ToArray());
    }
}

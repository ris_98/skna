﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolnica.Repository.CSV.Stream
{
    public interface ICSVStream<E>
    {
        void SaveAll(IEnumerable<E> entities);
        IEnumerable<E> ReadAll();
        void AppendToFIle(E entity);
    }
}

﻿using Bolnica.Model;
using System;

namespace Bolnica.Repository.CSV.Converter
{
    public class EquipmentCSVConverter : ICSVConverter<Equipment>
    {
        private readonly string Delimiter;

        public EquipmentCSVConverter(string delimiter) => Delimiter = delimiter;

        public string ConvertEntityToCSVFormat(Equipment equipment)
           => string.Join(Delimiter, equipment.ID.ToString(), equipment.Name, equipment.Description, equipment.Quantity.ToString(), equipment.Type.ToString(), equipment.AlertAmount.ToString());
        
        public Equipment ConvertCSVFormatToEntity(string equipmentCSVFormat)
        {
            string[] tokens = equipmentCSVFormat.Split(Delimiter.ToCharArray());
            EquipmentType a;
            Enum.TryParse<EquipmentType>(tokens[5], out a);
            return new Equipment(int.Parse(tokens[0]), tokens[3], tokens[4], int.Parse(tokens[1]),a,int.Parse(tokens[2]));
        }

    }
}
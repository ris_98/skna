﻿using Bolnica.Model;
using System;

namespace Bolnica.Repository.CSV.Converter
{
    public class RenovationCSVConverter : ICSVConverter<Renovation>
    {
        private readonly string Delimiter;
        private readonly string DatetimeFormat;


        public RenovationCSVConverter(string delimiter, string datetimeFormat)
        {
            Delimiter = delimiter;
            DatetimeFormat = datetimeFormat;
        }

        public string ConvertEntityToCSVFormat(Renovation renovation)
           => string.Join(Delimiter, renovation.ID.ToString(), renovation.DataRange.StartDate.ToString(DatetimeFormat), renovation.DataRange.EndDate.ToString(DatetimeFormat), renovation.Room.ID.ToString());
        public Renovation ConvertCSVFormatToEntity(string renovationCSVFormat)
        {
            string[] tokens = renovationCSVFormat.Split(Delimiter.ToCharArray());

            //ROOM HARDKODIRAN

            return new Renovation(int.Parse(tokens[0]), new DataRange(DateTime.Parse(tokens[1]), DateTime.Parse(tokens[2])), new Room());
        }

    }
}
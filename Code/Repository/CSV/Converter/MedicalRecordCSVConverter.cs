﻿using Bolnica.Model;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository.CSV.Converter
{
    public class MedicalRecordCSVConverter : ICSVConverter<MedicalRecord>
    {
        private const string APPOINTMENT_FILE = "../../../../../Files/appointment.csv";
        private const string EXAMINATIONREPORT_FILE = "../../../../../Files/examination.csv";
        private const string CSV_DELIMITER = ",";
        private const string DATETIME_FORMAT = "dd.MM.yyyy.";

        public AppointmentRepository appointmentRepository = new AppointmentRepository(
            new CSVStream<Appointment>(APPOINTMENT_FILE, new AppointmentCSVConverter(CSV_DELIMITER, DATETIME_FORMAT)),
            new IntSequencer());

        public ExaminationReportRepository examinationReportRepository = new ExaminationReportRepository(
            new CSVStream<ExaminationReport>(EXAMINATIONREPORT_FILE, new ExaminationReportCSVConverter(CSV_DELIMITER)),
            new IntSequencer());

        private readonly string Delimiter;

        public MedicalRecordCSVConverter(string delimiter)
        {
            Delimiter = delimiter;
        }

        public string ConvertEntityToCSVFormat(MedicalRecord medicalRecord)
           => string.Join(Delimiter, medicalRecord.ID, medicalRecord.ExaminationReport.ID.ToString(),medicalRecord.Appointment.ID.ToString());
        public MedicalRecord ConvertCSVFormatToEntity(string medicalRecordCSVFormat)
        {
            string[] tokens = medicalRecordCSVFormat.Split(Delimiter.ToCharArray());

            Appointment appointment = appointmentRepository.GetById(int.Parse(tokens[2]));
            ExaminationReport examinationReport = examinationReportRepository.GetById(int.Parse(tokens[1]));

            return new MedicalRecord(int.Parse(tokens[0]),examinationReport,appointment);
        }

    }
}
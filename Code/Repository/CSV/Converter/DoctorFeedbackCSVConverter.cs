﻿using Bolnica.Model;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository.CSV.Converter
{
    public class DoctorFeedbackCSVConverter : ICSVConverter<DoctorFeedback>
    {
        private const string DoctorFeedback_FILE = "../../../../../Files/doctorfeedback.csv";
        private const string DOCTOR_FILE = "../../../../../Files/doctor.csv";
        private const string CSV_DELIMITER = ",";

        public DoctorRepository doctorRepository = new DoctorRepository(
            new CSVStream<Doctor>(DOCTOR_FILE, new DoctorCSVConverter(CSV_DELIMITER)),
            new IntSequencer());

        private readonly string Delimiter;


        public DoctorFeedbackCSVConverter(string delimiter)
        {
            Delimiter = delimiter;

        }

        public string ConvertEntityToCSVFormat(DoctorFeedback doctorFeedback)
           => string.Join(Delimiter, doctorFeedback.ID.ToString(), doctorFeedback.Rating.ToString(),doctorFeedback.Doctor.ID.ToString(), doctorFeedback.NumberOfRatings.ToString());

        public DoctorFeedback ConvertCSVFormatToEntity(string doctorFeedbackCSVFormat)
        {
            string[] tokens = doctorFeedbackCSVFormat.Split(Delimiter.ToCharArray());

            Doctor doctor = doctorRepository.GetById(int.Parse(tokens[2]));


            return new DoctorFeedback(int.Parse(tokens[0]), float.Parse(tokens[1]), doctor, int.Parse(tokens[3]));
        }


    }
}
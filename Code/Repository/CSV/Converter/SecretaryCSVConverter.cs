﻿using Bolnica.Model;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository.CSV.Converter
{
    public class SecretaryCSVConverter : ICSVConverter<Secretary>
    {
        private const string ADRESS_FILE = "../../../../../Files/adress.csv";
        private const string CSV_DELIMITER = ",";

        public AdressRepository adressRepository = new AdressRepository(
            new CSVStream<Adress>(ADRESS_FILE, new AdressCSVConverter(CSV_DELIMITER)),
            new IntSequencer());
        private readonly string Delimiter;

        public SecretaryCSVConverter(string delimiter)
        {
            Delimiter = delimiter;
        }

        public string ConvertEntityToCSVFormat(Secretary secretary)
           => string.Join(Delimiter, secretary.EMail,secretary.Username, secretary.Password, secretary.ID.ToString(), secretary.FirstName, secretary.LastName, secretary.TelephoneNumber, secretary.JMBG, secretary.Adress.ID.ToString());

        public Secretary ConvertCSVFormatToEntity(string clientCSVFormat)
        {
            string[] tokens = clientCSVFormat.Split(Delimiter.ToCharArray());

            Adress adresa = adressRepository.GetById(int.Parse(tokens[8]));

            return new Secretary(tokens[0], tokens[1], tokens[2], int.Parse(tokens[3]), tokens[4], tokens[5], tokens[6], tokens[7], adresa);
        }

    }
}

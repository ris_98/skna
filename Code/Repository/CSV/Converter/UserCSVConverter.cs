﻿using Bolnica.Model;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository.CSV.Converter
{
    public class UserCSVConverter : ICSVConverter<User>
    {
        private const string ADRESS_FILE = "../../../../../Files/adress.csv";
        private const string CSV_DELIMITER = ",";

        public AdressRepository adressRepository = new AdressRepository(
            new CSVStream<Adress>(ADRESS_FILE, new AdressCSVConverter(CSV_DELIMITER)), new IntSequencer());

        private readonly string Delimiter;

        public UserCSVConverter(string delimiter) => Delimiter = delimiter;

        public string ConvertEntityToCSVFormat(User user)
           => string.Join(Delimiter, user.EMail,user,user.Username,user.Password,user.ID.ToString(),user.FirstName,user.LastName,user.TelephoneNumber,user.JMBG,user.Adress.ID.ToString());
       
        public User ConvertCSVFormatToEntity(string clientCSVFormat)
        {
            string[] tokens = clientCSVFormat.Split(Delimiter.ToCharArray());
            Adress adresa = adressRepository.GetById(int.Parse(tokens[8]));
            return new User(tokens[0], tokens[1], tokens[2], int.Parse(tokens[3]),tokens[4], tokens[5], tokens[6], tokens[7], adresa);
        }

    }
}

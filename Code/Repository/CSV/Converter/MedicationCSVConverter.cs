﻿using Bolnica.Model;
using System;

namespace Bolnica.Repository.CSV.Converter
{
    public class MedicationCSVConverter : ICSVConverter<Medication>
    {
        private readonly string Delimiter;
        

        public MedicationCSVConverter(string delimiter)
        {
            Delimiter = delimiter;
        }

        public string ConvertEntityToCSVFormat(Medication medication)
           => string.Join(Delimiter, medication.ID.ToString(), medication.Name, medication.Approved.ToString(), medication.Quantity.ToString(), medication.NumberOfVerification.ToString(), medication.AlertAmount.ToString());
        public Medication ConvertCSVFormatToEntity(string medicationCSVFormat)
        {
            string[] tokens = medicationCSVFormat.Split(Delimiter.ToCharArray());
            return new Medication(int.Parse(tokens[0]), tokens[1], Boolean.Parse(tokens[2]), int.Parse(tokens[3]), int.Parse(tokens[4]),int.Parse(tokens[5]));
        }


    }
}
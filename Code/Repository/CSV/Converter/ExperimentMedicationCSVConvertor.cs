﻿using Bolnica.Model;

namespace Bolnica.Repository.CSV.Converter
{
    public class ExperimentMedicationCSVConverter : ICSVConverter<NotVerifiedMedication>
    {
        private readonly string Delimiter;

        public ExperimentMedicationCSVConverter(string delimiter) => Delimiter = delimiter;


        public string ConvertEntityToCSVFormat(NotVerifiedMedication medication)
           => string.Join(Delimiter, medication.ID.ToString(), medication.Name, medication.Note);
        public NotVerifiedMedication ConvertCSVFormatToEntity(string experimentMedicationCSVFormat)
        {
            string[] tokens = experimentMedicationCSVFormat.Split(Delimiter.ToCharArray());
            return new NotVerifiedMedication(int.Parse(tokens[0]), tokens[1], tokens[2]);
        }

    }
}
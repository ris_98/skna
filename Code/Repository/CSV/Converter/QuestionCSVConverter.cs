﻿using Bolnica.Model;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository.CSV.Converter
{
    public class QuestionCSVConverter : ICSVConverter<Question>
    {
        private readonly string Delimiter;
        
        private const string Patient_FILE = "../../../../../Files/patient.csv";
        private const string CSV_DELIMITER = ",";

        public PatientRepository patientRepository = new PatientRepository(
            new CSVStream<Patient>(Patient_FILE, new PatientCSVConverter(CSV_DELIMITER)),new IntSequencer());

        public QuestionCSVConverter(string delimiter) => Delimiter = delimiter;

        public string ConvertEntityToCSVFormat(Question question)
           => string.Join(Delimiter, question.ID.ToString(), question.QuestionContent, question.QuestionAnswer,question.Patient.ID.ToString());
                
        public Question ConvertCSVFormatToEntity(string questionCSVFormat)
        {
            string[] tokens = questionCSVFormat.Split(Delimiter.ToCharArray());

            Patient patient = patientRepository.GetById(int.Parse(tokens[3]));
            
            return new Question(int.Parse(tokens[0]), tokens[1],tokens[2], patient);
        }

    }
}
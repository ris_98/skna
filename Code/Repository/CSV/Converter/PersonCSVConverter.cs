﻿using Bolnica.Model;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository.CSV.Converter
{
    public class PersonCSVConverter : ICSVConverter<Person>
    {
        private const string ADRESS_FILE = "../../../../../Files/adress.csv";
        private const string CSV_DELIMITER = ",";

        public AdressRepository adressRepository = new AdressRepository(
            new CSVStream<Adress>(ADRESS_FILE, new AdressCSVConverter(CSV_DELIMITER)),
            new IntSequencer());
        private readonly string Delimiter;


        public PersonCSVConverter(string delimiter)
        {
            Delimiter = delimiter;
        }

        public string ConvertEntityToCSVFormat(Person person)
           => string.Join(Delimiter, person.ID, person.FirstName, person.LastName, person.TelephoneNumber, person.JMBG, person.Adress.ID.ToString());
        
        //Nema medical record 

        public Person ConvertCSVFormatToEntity(string personCSVFormat)
        {
            string[] tokens = personCSVFormat.Split(Delimiter.ToCharArray());

            Adress adresa = adressRepository.GetById(int.Parse(tokens[5]));

            return new Person(int.Parse(tokens[0]), tokens[1], tokens[2], tokens[3], tokens[4], adresa);
        }


    }
}
﻿using Bolnica.Model;

namespace Bolnica.Repository.CSV.Converter
{
    public class FeedbackCSVConverter : ICSVConverter<Feedback>
    {
        private readonly string Delimiter;

        public FeedbackCSVConverter(string delimiter)
        {
            Delimiter = delimiter;
        }

        public string ConvertEntityToCSVFormat(Feedback feedback)
           => string.Join(Delimiter,feedback.ID.ToString(),feedback.Rating.ToString(),feedback.Message);

        public Feedback ConvertCSVFormatToEntity(string clientCSVFormat)
        {
            string[] tokens = clientCSVFormat.Split(Delimiter.ToCharArray());
            return new Feedback(int.Parse(tokens[0]), int.Parse(tokens[1]), tokens[2]);
        }

    }
}

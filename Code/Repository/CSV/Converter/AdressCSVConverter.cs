﻿using Bolnica.Model;

namespace Bolnica.Repository.CSV.Converter
{
    public class AdressCSVConverter : ICSVConverter<Adress>
    {
        private readonly string Delimiter;

        public AdressCSVConverter(string delimiter)
        {
            Delimiter = delimiter;
        }

        public string ConvertEntityToCSVFormat(Adress adress)
           => string.Join(Delimiter,adress.ID.ToString(), adress.Country, adress.City, adress.ZipCode, adress.Street);

        public Adress ConvertCSVFormatToEntity(string adressCSVFormat)
        {
            string[] tokens = adressCSVFormat.Split(Delimiter.ToCharArray());
            return new Adress(int.Parse(tokens[0]),tokens[1], tokens[2], tokens[3], tokens[4]);
        }

    }
}
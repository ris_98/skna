﻿using Bolnica.Model;
using System;

namespace Bolnica.Repository.CSV.Converter
{
    public class ProcurementCSVConverter : ICSVConverter<Procurement>
    {
        private readonly string Delimiter;
        private readonly string DatetimeFormat;

        public ProcurementCSVConverter(string delimiter, string datetimeFormat)
        {
            Delimiter = delimiter;
            DatetimeFormat = datetimeFormat;
        }

        public string ConvertEntityToCSVFormat(Procurement procurement)
           => string.Join(Delimiter, procurement.ID.ToString(), procurement.Date.ToString(DatetimeFormat), procurement.Note);
        public Procurement ConvertCSVFormatToEntity(string procurementCSVFormat)
        {
            string[] tokens = procurementCSVFormat.Split(Delimiter.ToCharArray());
            return new Procurement(int.Parse(tokens[0]), DateTime.Parse(tokens[1]), tokens[2]);
        }

    }
}
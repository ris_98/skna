﻿using Bolnica.Model;

namespace Bolnica.Repository.CSV.Converter
{
    public class DiagnosisCSVConverter : ICSVConverter<Diagnosis>
    {
        private const string DIAGNOSIS_FILE = "../../../../../Files/diagnosis.csv";
        private const string CSV_DELIMITER = ",";

        

        private readonly string Delimiter;
      

        public DiagnosisCSVConverter(string delimiter)
        {
            Delimiter = delimiter;
            
        }

        public string ConvertEntityToCSVFormat(Diagnosis diagnosis)
           => string.Join(Delimiter, diagnosis.ID.ToString(),diagnosis.Name);

        public Diagnosis ConvertCSVFormatToEntity(string diagnosisCSVFormat)
        {
            string[] tokens = diagnosisCSVFormat.Split(Delimiter.ToCharArray());
           

            return new Diagnosis(int.Parse(tokens[0]), tokens[1]);
        }


    }
}
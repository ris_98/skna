﻿using Bolnica.Model;

namespace Bolnica.Repository.CSV.Converter
{
    public class ReportCSVConverter : ICSVConverter<Report>
    {
        private const string REPORT_FILE = "../../../../../Files/report.csv";
        private const string CSV_DELIMITER = ",";


        private readonly string Delimiter;


        public ReportCSVConverter(string delimiter)
        {
            Delimiter = delimiter;

        }

        public string ConvertEntityToCSVFormat(Report report)
           => string.Join(Delimiter, report.ID.ToString(), report.Data);

        public Report ConvertCSVFormatToEntity(string labTestRequisitionCSVFormat)
        {
            string[] tokens = labTestRequisitionCSVFormat.Split(Delimiter.ToCharArray());


            return new Report(int.Parse(tokens[0]), tokens[1]);
        }
    }
}
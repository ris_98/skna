﻿using Bolnica.Model;

namespace Bolnica.Repository.CSV.Converter
{
    public class LabTestRequisitionCSVConverter : ICSVConverter<LabTestRequisition>
    {
        private const string LABTESTREQUISITION_FILE = "../../../../../Files/labTestRequisition.csv";
        private const string CSV_DELIMITER = ",";

       

        private readonly string Delimiter;


        public LabTestRequisitionCSVConverter(string delimiter)
        {
            Delimiter = delimiter;

        }

        public string ConvertEntityToCSVFormat(LabTestRequisition labTestRequisition)
           => string.Join(Delimiter, labTestRequisition.ID.ToString(), labTestRequisition.Note);

        public LabTestRequisition ConvertCSVFormatToEntity(string labTestRequisitionCSVFormat)
        {
            string[] tokens = labTestRequisitionCSVFormat.Split(Delimiter.ToCharArray());


            return new LabTestRequisition(int.Parse(tokens[0]), tokens[1]);
        }


    }
}
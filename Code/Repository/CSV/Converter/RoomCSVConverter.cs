﻿using Bolnica.Model;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;
using System;
using System.Collections.Generic;

namespace Bolnica.Repository.CSV.Converter
{
    public class RoomCSVConverter : ICSVConverter<Room>
    {
        private const string Patient_FILE = "../../../../../Files/patient.csv";
        private const string EQUIPMENT_FILE = "../../../../../Files/equipment.csv";
        private const string CSV_DELIMITER = ",";

        private EquipmentRepository equipmentRepository = new EquipmentRepository(
           new CSVStream<Equipment>(EQUIPMENT_FILE, new EquipmentCSVConverter(CSV_DELIMITER)), new IntSequencer());
        private PatientRepository patientRepository = new PatientRepository(
            new CSVStream<Patient>(Patient_FILE, new PatientCSVConverter(CSV_DELIMITER)),
            new IntSequencer());

        private readonly string Delimiter;

        public RoomCSVConverter(string delimiter)
        {
            Delimiter = delimiter;
        }

        public string ConvertEntityToCSVFormat(Room room)
        {
            string patientsIDs="";
            foreach (Patient patient in room.Patient)
            {
                patientsIDs += " " + patient.ID.ToString();
            }
            string equipmentIDAndNumbers = "";
            foreach(RoomEquipment equipment in room.RoomEquipment)
            {
                equipmentIDAndNumbers += " " + equipment.equipment.ID.ToString()+ "|"+equipment.Quantity; 
            }
            return string.Join(Delimiter, room.ID.ToString(), room.RoomType.ToString(), patientsIDs,equipmentIDAndNumbers);
        }
            
        public Room ConvertCSVFormatToEntity(string roomCSVFormat)
        {
            const string split = " ";
            string[] tokens = roomCSVFormat.Split(Delimiter.ToCharArray());
            RoomType a;
            Enum.TryParse<RoomType>(tokens[1], out a);
            string[] patients = tokens[2].Split(split.ToCharArray());
            string[] equipment= tokens[3].Split(split.ToCharArray());
            List<Patient> patientList = new List<Patient>();
            List<RoomEquipment> roomEquipment = new List<RoomEquipment>();
            foreach(string s in patients)
            {
                Patient patinet = patientRepository.GetById(int.Parse(s));
                patientList.Add(patinet);
            }
            foreach(string s in equipment)
            {
                string[] er = s.Split("|".ToCharArray());
                Equipment e = equipmentRepository.GetById(int.Parse(er[0]));
                RoomEquipment roomE = new RoomEquipment(e, int.Parse(er[1]));
                roomEquipment.Add(roomE);

            }

            return new Room(int.Parse(tokens[0]),a,patientList, roomEquipment);
        }


    }
}
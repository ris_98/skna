﻿using Bolnica.Model;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository.CSV.Converter
{
    public class DoctorCSVConverter : ICSVConverter<Doctor>
    {
        private const string ADRESS_FILE = "../../../../../Files/adress.csv";
        private const string CSV_DELIMITER = ",";

        public AdressRepository adressRepository = new AdressRepository(
            new CSVStream<Adress>(ADRESS_FILE, new AdressCSVConverter(CSV_DELIMITER)),
            new IntSequencer());

        private readonly string Delimiter;

        public DoctorCSVConverter(string delimiter)
        {
            Delimiter = delimiter;
        }

        public string ConvertEntityToCSVFormat(Doctor doctor)
           => string.Join(Delimiter, doctor.EMail,doctor.Username, doctor.Password, doctor.ID.ToString(), doctor.FirstName, doctor.LastName, doctor.TelephoneNumber, doctor.JMBG, doctor.Adress.ID.ToString(), doctor.specialization, doctor.rating);
        public Doctor ConvertCSVFormatToEntity(string doctorCSVFormat)
        {
            string[] tokens = doctorCSVFormat.Split(Delimiter.ToCharArray());

            Adress adresa = adressRepository.GetById(int.Parse(tokens[8]));

            return new Doctor(tokens[0],tokens[1], tokens[2], int.Parse(tokens[3]), tokens[4], tokens[5], tokens[6], tokens[7], adresa,tokens[9], float.Parse(tokens[10]));
        }

    }
}
﻿using Bolnica.Model;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository.CSV.Converter
{
    public class ExaminationReportCSVConverter : ICSVConverter<ExaminationReport>
    {
        private const string Diagnosis_FILE = "../../../../../Files/diagnosis.csv";
        private const string Prescription_FILE = "../../../../../Files/prescription.csv";
        private const string LabTestRequisition_FILE = "../../../../../Files/labtestrequisition.csv";


        private const string CSV_DELIMITER = ",";
        private const string DATETIME_FORMAT = "dd.MM.yyyy.";

        public DiagnosisRepository diagnosisRepository = new DiagnosisRepository(
            new CSVStream<Diagnosis>(Diagnosis_FILE, new DiagnosisCSVConverter(CSV_DELIMITER)),
            new IntSequencer());
        public PrescriptionRepository prescriptionRepository = new PrescriptionRepository(
            new CSVStream<Prescription>(Prescription_FILE, new PrescriptionCSVConverter(CSV_DELIMITER, DATETIME_FORMAT)),
            new IntSequencer());
        public LabTestRequisitionRepository labTestRequisitionRepository = new LabTestRequisitionRepository(
            new CSVStream<LabTestRequisition>(LabTestRequisition_FILE, new LabTestRequisitionCSVConverter(CSV_DELIMITER)),
            new IntSequencer());
        private readonly string Delimiter;

        public ExaminationReportCSVConverter(string delimiter)
        {
            Delimiter = delimiter;
        }

        public string ConvertEntityToCSVFormat(ExaminationReport examinationReport)
           => string.Join(Delimiter, examinationReport.ID.ToString(), examinationReport.Prescription.ID.ToString(),examinationReport.Diagnosis.ID.ToString(), examinationReport.LabTestRequisition.ID.ToString());

        public ExaminationReport ConvertCSVFormatToEntity(string clientCSVFormat)
        {
            string[] tokens = clientCSVFormat.Split(Delimiter.ToCharArray());
            Prescription prescription = prescriptionRepository.GetById(int.Parse(tokens[1]));
            Diagnosis diagnosis = diagnosisRepository.GetById(int.Parse(tokens[2]));
            LabTestRequisition labTestRequisition = labTestRequisitionRepository.GetById(int.Parse(tokens[3]));
            return new ExaminationReport(int.Parse(tokens[0]),prescription,diagnosis,labTestRequisition);
        }

    }
}

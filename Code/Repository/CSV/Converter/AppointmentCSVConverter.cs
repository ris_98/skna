﻿using Bolnica.Model;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;
using System;

namespace Bolnica.Repository.CSV.Converter
{
    public class AppointmentCSVConverter : ICSVConverter<Appointment>
    {
        private const string Patient_FILE = "../../../../../Files/patient.csv";
        private const string DOCTOR_FILE = "../../../../../Files/doctor.csv";
        private const string EXAMINATION_FILE = "../../../../../Files/examination.csv";
        private const string Person_FILE = "../../../../../Files/person.csv";

        private const string CSV_DELIMITER = ",";

        private PatientRepository patientRepository = new PatientRepository(
            new CSVStream<Patient>(Patient_FILE, new PatientCSVConverter(CSV_DELIMITER)),
            new IntSequencer());

        private DoctorRepository doctorRepository = new DoctorRepository(
            new CSVStream<Doctor>(DOCTOR_FILE, new DoctorCSVConverter(CSV_DELIMITER)),
            new IntSequencer());

        public PersonRepository personRepository = new PersonRepository(
            new CSVStream<Person>(Person_FILE, new PersonCSVConverter(CSV_DELIMITER)), new IntSequencer());


        private readonly string Delimiter;
        private readonly string DatetimeFormat;



        public AppointmentCSVConverter(string delimiter, string datetimeFormat)
        {
            Delimiter = delimiter;
            DatetimeFormat = datetimeFormat;
        }

        public string ConvertEntityToCSVFormat(Appointment appointment)
           => string.Join(Delimiter,appointment.ID.ToString(), appointment.DateTime.ToString(DatetimeFormat),appointment.DurationInMinuts, appointment.AppointmentType.ToString(), appointment.Patient.ID.ToString(), appointment.Doctor.ID.ToString());
       

        public Appointment ConvertCSVFormatToEntity(string appointmentCSVFormat)
        {
            string[] tokens = appointmentCSVFormat.Split(Delimiter.ToCharArray());
            Patient patient = new Patient();
            if (int.Parse(tokens[4]) < 0)
            {
                Person person = personRepository.GetById(-int.Parse(tokens[4]));
                patient.ID = person.ID;
                patient.FirstName = person.FirstName;
                patient.LastName = person.LastName;
                patient.JMBG = person.JMBG;
                patient.TelephoneNumber = person.TelephoneNumber;
                patient.Adress = person.Adress;
            }
            else
            {
                patient = patientRepository.GetById(int.Parse(tokens[4]));
            }
            
            Doctor doctor = doctorRepository.GetById(int.Parse(tokens[5]));
            AppointmentType a;
            Enum.TryParse<AppointmentType>(tokens[3],out a);

            return new Appointment(int.Parse(tokens[0]),DateTime.Parse(tokens[1]),int.Parse(tokens[2]) ,a, patient, doctor);
        }


    }
}
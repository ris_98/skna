﻿using Bolnica.Model;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;
using System;

namespace Bolnica.Repository.CSV.Converter
{
    public class PrescriptionCSVConverter : ICSVConverter<Prescription>
    {
        private const string Medication_FILE = "../../../../../Files/medications.csv";
        private const string Prescription_FILE = "../../../../../Files/prescription.csv";

        private const string CSV_DELIMITER = ",";

        public MedicationRepository medicationRepository = new MedicationRepository(
            new CSVStream<Medication>(Medication_FILE, new MedicationCSVConverter(CSV_DELIMITER)), new IntSequencer());


        private readonly string Delimiter;
        private readonly string DatetimeFormat;

        public PrescriptionCSVConverter(string delimiter,string datetimeFormat )
        {
            Delimiter = delimiter;
            DatetimeFormat = datetimeFormat;
        }

        public string ConvertEntityToCSVFormat(Prescription prescription)
           => string.Join(Delimiter, prescription.ID.ToString(), prescription.ExpirationDate.ToString(DatetimeFormat), prescription.Dosage, prescription.Medication.ID.ToString());
        
        
        public Prescription ConvertCSVFormatToEntity(string prescriptionCSVFormat)
        {
            string[] tokens = prescriptionCSVFormat.Split(Delimiter.ToCharArray());
            Medication medication = medicationRepository.GetById(int.Parse(tokens[3]));
            return new Prescription(int.Parse(tokens[0]), DateTime.Parse(tokens[1]), tokens[2], medication);
        }

    }
}
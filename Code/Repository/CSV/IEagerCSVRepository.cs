﻿using Bolnica.Repository.Abstract;
using System;
using System.Collections.Generic;

namespace Bolnica.Repository.CSV
{
    public interface IEagerCSVRepository<E, ID>
        where E : IIdentifiable<ID>
        where ID : IComparable
    {
        E GetById(ID id);
        IEnumerable<E> GetAll();
    }
}

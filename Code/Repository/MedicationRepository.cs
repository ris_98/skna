using System.Collections.Generic;
using System.Linq;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository

{
  
        public class MedicationRepository : CSVRepository<Medication, int>, IMedicationRepository, IEagerCSVRepository<Medication, int>
        {
        private const string ENTITY_NAME = "Medication";
        private const string NOT_UNIQUE_ERROR = "Medication id {0} is not unique!";

        //private readonly IEagerCSVRepository<Medication, long> _medicationRepository;

        public MedicationRepository(ICSVStream<Medication> stream, ISequencer<int> sequencer )
            : base(ENTITY_NAME, stream, sequencer)
        {
            
        }
        public IEnumerable<Medication> GetAlls() => GetAll();


        public IEnumerable<Medication> GetAllApproved() =>
            (_stream.ReadAll().Where(medication => medication.Approved == true)).ToList();


      

        public IEnumerable<Medication> GetAllNotApproved()
            =>( _stream.ReadAll().Where(medication => medication.Approved == false)).ToList();



        public Medication GetById(int id) => Get(id);

        public new Medication Create(Medication medication)
        {
            return base.Create(medication);
        }

        public void Delete(Medication medication)
        {
            base.Delete(medication);
        }

        public void Update(Medication medication)
        {
            base.Update(medication);
        }


    }
}
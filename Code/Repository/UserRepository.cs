using System.Collections.Generic;
using Bolnica.Model;
using Bolnica.Repository.Abstract;
using Bolnica.Repository.CSV;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Repository
{
    public class UserRepository : CSVRepository<User, int>, IUserRepository, IEagerCSVRepository<User, int>
    {
        private const string ENTITY_NAME = "User";
        private const string NOT_UNIQUE_ERROR = "User id {0} is not unique!";

        public UserRepository(ICSVStream<User> stream, ISequencer<int> sequencer)
            : base(ENTITY_NAME, stream, sequencer)
        {
        }
        public IEnumerable<User> GetAll()
        {
            return base.GetAll();
        }


        public User GetById(int id) => Get(id);

        public new User Create(User user)
        {
            return base.Create(user);
        }

        public void Delete(User user)
        {
            base.Delete(user);
        }

        public void Update(User user)
        {
            base.Update(user);
        }
    }
}
using System;
using Bolnica.Repository.Abstract;

namespace Bolnica.Model
{
   public class Appointment : IIdentifiable<int>
    {
        public int ID;
        public int DurationInMinuts;
        public DateTime DateTime;
        public AppointmentType AppointmentType;
        public Patient Patient;
        public Doctor Doctor;

        public Appointment() {}

        public Appointment(int id,DateTime dateTime,int duration, AppointmentType appointmentType, Patient patient, Doctor doctor)
        {
            ID = id;
            DateTime = dateTime;
            AppointmentType = appointmentType;
            Patient = patient;
            Doctor = doctor;
            DurationInMinuts = duration;
        }

        public int GetId() => ID;

        public void SetId(int id) => ID = id;

   }
}
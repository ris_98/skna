using System;
using Bolnica.Repository.Abstract;


namespace Bolnica.Model
{
   public class Procurement : IIdentifiable<int>
    {
        public int ID;
        public DateTime Date;
        public String Note;

        public Procurement(int iD, DateTime date, string note)
        {
            ID = iD;
            Date = date;
            Note = note;
        }

        public int GetId() => ID;

        public void SetId(int id) => ID = id;
   
   }
}
using Bolnica.Repository.Abstract;
using System.Collections.Generic;

namespace Bolnica.Model
{
   public class Room : IIdentifiable<int>
    {
        public int ID;
        public RoomType RoomType;
        public List<Patient> Patient;
        public List<RoomEquipment> RoomEquipment;

        public Room(){}

        public Room(int iD, RoomType roomType, List<Patient> patient, List<RoomEquipment> roomEquipment)
        {
            ID = iD;
            RoomType = roomType;
            Patient = patient;
            RoomEquipment = roomEquipment;
        }

        public int GetId() => ID;

        public void SetId(int id) => ID = id;

   
   }
}
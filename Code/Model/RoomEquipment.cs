﻿namespace Bolnica.Model
{
    public class RoomEquipment
    {
        public Equipment equipment;
        public int Quantity;

        public RoomEquipment(Equipment equipment, int quantity)
        {
            this.equipment = equipment;
            Quantity = quantity;
        }
    }
}
using Bolnica.Repository.Abstract;
using System;

namespace Bolnica.Model
{
    public class Person :  IIdentifiable<int>
    {      
        public int ID;
        public String FirstName;
        public String LastName;
        public String TelephoneNumber;
        public String JMBG;
        public Adress Adress;


        public Person(){}

        public Person(int iD, string firstName, string lastName, string telephoneNumber, string jMBG, Adress adress)
        {
            ID = iD;
            FirstName = firstName;
            LastName = lastName;
            TelephoneNumber = telephoneNumber;
            JMBG = jMBG;
            Adress = adress;
        }


        public int GetId() => ID;

        public void SetId(int id) => ID = id;

        public Boolean IsPersonValid()
        {
            throw new NotImplementedException();
        }

        protected Boolean IsJMBGValid()
        {
            throw new NotImplementedException();
        }
    }
}
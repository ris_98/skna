namespace Bolnica.Model
{
    public class Employees : User
   {
        public WorkingHours WorkingHours;

        public Employees(): base(){}

        public Employees(string eMail,string username ,string password, int iD, string firstName, string lastName, string telephoneNumber, string jMBG, Adress adress)
            :base(eMail, username, password, iD, firstName, lastName, telephoneNumber, jMBG, adress)
        {
            WorkingHours = new WorkingHours();
        }

   }
}
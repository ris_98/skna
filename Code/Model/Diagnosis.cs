using System;
using Bolnica.Repository.Abstract;

namespace Bolnica.Model
{
   public class Diagnosis : IIdentifiable<int>
    {
        public int ID;
        public String Name;

        public Diagnosis(int iD, string name)
        {
            ID = iD;
            Name = name;
        }

        public Diagnosis() {}

        public int GetId() => ID;

        public void SetId(int id) => ID = id;
    }
}
using Bolnica.Repository.Abstract;

namespace Bolnica.Model
{
    public class LabTestRequisition : IIdentifiable<int>
    {
        public int ID;
        public string Note;

        public LabTestRequisition(int iD, string note)
        {
            this.ID = iD;
            this.Note = note;
        }

        public int GetId() => ID;

        public void SetId(int id) => ID = id;
    }
}
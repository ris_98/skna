﻿using System;
using Bolnica.Repository.Abstract;

namespace Bolnica.Model
{
    public class NotVerifiedMedication : IIdentifiable<int>
    {

        public int ID;
        public String Name;
        public String Note;

        public NotVerifiedMedication(){}

        public NotVerifiedMedication(int iD, string name, string note)
        {
            ID = iD;
            Name = name;
            Note = note;
        }
        public int GetId() => ID;

        public void SetId(int id) => ID = id;

    }
}
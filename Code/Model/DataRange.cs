using System;

namespace Bolnica.Model
{
   public class DataRange
   {
      public DateTime StartDate;
      public DateTime EndDate;

        public DataRange(DateTime startDate, DateTime endDate)
        {
            StartDate = startDate;
            EndDate = endDate;
        }
        public DataRange() {
            StartDate = new DateTime();
            EndDate = new DateTime();
        
        }

        public Boolean IsValid()
        {
            int offset = DateTime.Compare(StartDate, EndDate);
            if (offset >= 0)
                return false;
            return true;
        }
   }
}
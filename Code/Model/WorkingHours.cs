using System;

namespace Bolnica.Model
{
    public class WorkingHours
   {
        private const int NumberOfWorkingHoursRequiredForFreeDay = 300;
        public int NumberOfWorkingHours;
        public DataRange WorikingTime;


        public WorkingHours() 
        {
            NumberOfWorkingHours = 0;
            DataRange WorikingTime = new DataRange();
            WorikingTime.StartDate = new DateTime(2020, 1, 1, 10, 0,0);
            WorikingTime.EndDate= new DateTime(2020, 1, 1, 18,0, 0);
        }

        public WorkingHours(int numberOfWorkingHours, DataRange workingTime)
        {
            NumberOfWorkingHours = numberOfWorkingHours;
            WorikingTime = workingTime;
        }


        public Boolean IsFreeDayAvailable()
        {
            if (NumberOfWorkingHours >= NumberOfWorkingHoursRequiredForFreeDay)
                return true;
            return false;
        }

    }
}
using Bolnica.Repository.Abstract;

namespace Bolnica.Model
{
   public class MedicalRecord : IIdentifiable<int>
    {
        public int ID;
        public ExaminationReport ExaminationReport;
        public Appointment Appointment;

        public MedicalRecord(int iD, ExaminationReport examinationReport, Appointment appointment)
        {
            this.ID = iD;
            this.ExaminationReport = examinationReport;
            this.Appointment = appointment;
        }

        public int GetId() => ID;

        public void SetId(int id) => ID = id;
    }
}
using System;
using Bolnica.Repository.Abstract;

namespace Bolnica.Model
{
    public class Medication : IIdentifiable<int>
    {
     
        public int ID;
        public int Quantity;
        public int NumberOfVerification;
        public int AlertAmount;
        public String Name;
        public Boolean Approved;


        public Medication() {}

        public Medication(int iD, string name, bool approved, int quantity, int numberOfVerification, int alertAmount)
        {
            ID = iD;
            Name = name;
            Approved = approved;
            Quantity = quantity;
            NumberOfVerification = numberOfVerification;
            AlertAmount = alertAmount;
        }

        public int GetId() => ID;

        public void SetId(int id) => ID = id;

        private Boolean LowQuantityAllert()
        {
            if (AlertAmount <= Quantity)
                return true;
            return false;
        }
    }
}
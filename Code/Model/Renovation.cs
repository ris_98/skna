using Bolnica.Repository.Abstract;

namespace Bolnica.Model
{
   public class Renovation : IIdentifiable<int>
    {
        public int ID;
        public DataRange DataRange;
        public Room Room; 

        public Renovation(){}

        public Renovation(int iD, DataRange dataRange, Room room)
        {
            ID = iD;
            DataRange = dataRange;
            Room = room;
        }

        public int GetId() => ID;

        public void SetId(int id) => ID = id;

    }
}
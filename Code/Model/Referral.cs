using System;

namespace Bolnica.Model
{
    public class Referral
    {
        private int ID;
        private String Type;
        public Doctor Doctor;

        public Referral(int iD, string type, Doctor doctor)
        {
            ID = iD;
            Type = type;
            Doctor = doctor;
        }
    }
}
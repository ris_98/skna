using Bolnica.Repository.Abstract;

namespace Bolnica.Model
{
   public class ExaminationReport : IIdentifiable<int>
    {
        public int ID;
        public Prescription Prescription;
        public Diagnosis Diagnosis;
        public LabTestRequisition LabTestRequisition;

        public ExaminationReport(){}

        public ExaminationReport(int iD, Prescription prescription, Diagnosis diagnosis, LabTestRequisition labTestRequisition)
        {
            ID = iD;
            Prescription = prescription;
            Diagnosis = diagnosis;
            LabTestRequisition = labTestRequisition;
        }

        public int GetId() => ID;
        public void SetId(int id) => ID = id;

    }
}
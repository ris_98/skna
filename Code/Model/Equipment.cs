using System;
using Bolnica.Repository.Abstract;

namespace Bolnica.Model
{
   public class Equipment : IIdentifiable<int>
    {
        public int ID;
        public int Quantity;
        public int AlertAmount;
        public String Name;
        public String Description;
        public EquipmentType Type;


        public Equipment(int iD, string name, string description, int quantity, EquipmentType type, int alertAmount)
        {
            ID = iD;
            Name = name;
            Description = description;
            Quantity = quantity;
            Type = type;
            AlertAmount = alertAmount;
        }

        private Boolean LowQuantityAllert()
        {
            if (AlertAmount <= Quantity)
                return true;
            return false;
        }

        public int GetId() => ID;
        public void SetId(int id) => ID = id;

    }
}
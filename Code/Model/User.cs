using System;

namespace Bolnica.Model
{
   public class User : Person
   {

        public String EMail;
        public String Username;
        public String Password;

        public User() : base() {}

        public User(string username, string password)
        {
            Username = username;
            Password = password;
        }

        public User(string eMail,string username, string password, int iD, string firstName, string lastName, string telephoneNumber, string jMBG, Adress adress)
            : base(iD, firstName, lastName, telephoneNumber, jMBG, adress)
        {
            this.EMail = eMail;
            this.Username = username;
            this.Password = password;
        }
        public int GetId() => ID;
        public void SetId(int id) => ID = id;

        private Boolean IsEmailValid()
        {
         throw new NotImplementedException();
        }
        private Boolean IsPasswordVaild()
        {
            throw new NotImplementedException();
        }
        public Boolean IsUserValid()
        {
            throw new NotImplementedException();
        }
   
   }
}
using System;
using Bolnica.Repository.Abstract;

namespace Bolnica.Model
{
    public class Report : IIdentifiable<int>
    {
        public int ID;
        public String Data;

        public Report(int iD, string data)
        {
            ID = iD;
            Data = data;
        }

        public int GetId() => ID;

        public void SetId(int id) => ID = id;

    }
}
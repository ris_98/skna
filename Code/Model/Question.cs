using Bolnica.Repository.Abstract;

namespace Bolnica.Model
{
   public class Question : IIdentifiable<int>
    {
        public int ID;
        public string QuestionContent;
        public string QuestionAnswer;
        public Patient Patient;

        public Question(){}
        
        public Question(int iD, string questionContent, string questionAnswer, Patient patient)
        {
            this.ID = iD;
            this.QuestionContent = questionContent;
            this.QuestionAnswer = questionAnswer;
            this.Patient = patient;
        }

        public int GetId() => ID;

        public void SetId(int id) => ID = id;

   }
}
namespace Bolnica.Model
{
   public enum AppointmentType
   {
      Examination,
      SpecialistExamination,
      Operation
   }
}
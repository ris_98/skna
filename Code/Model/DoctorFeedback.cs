using Bolnica.Repository.Abstract;

namespace Bolnica.Model
{
   public class DoctorFeedback : IIdentifiable<int>
    {
        public int ID;
        public float Rating;
        public int NumberOfRatings;
        public Doctor Doctor;

        public DoctorFeedback(){}

        public DoctorFeedback(int iD, float rating, Doctor doctor, int numberOfRatings)
        {
            ID = iD;
            Rating = rating;
            Doctor = doctor;
            NumberOfRatings = numberOfRatings;
        }

        public int GetId() => ID;

        public void SetId(int id) => ID = id;
    }
}
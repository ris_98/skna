using System;
using Bolnica.Repository.Abstract;

namespace Bolnica.Model
{
    public class Adress : IIdentifiable<int>
    {
        public int ID;
        public String Country;
        public String City;
        public String ZipCode;
        public String Street;

        public int GetId() => ID;

        public void SetId(int id) => ID = id;
        
        public Adress() {}

        public Adress(int id, string country, string city, string zipCode, string street)
        {
            ID = id;
            Country = country;
            City = city;
            ZipCode = zipCode;
            Street = street;
        }

        public Adress(string country, string city, string zipCode, string street)
        {
            Country = country;
            City = city;
            ZipCode = zipCode;
            Street = street;
        }   
    }
}
namespace Bolnica.Model
{
   public enum RoomType
   {
      Operation,
      Nursing,
      Recovery
   }
}
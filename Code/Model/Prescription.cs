using System;
using Bolnica.Repository.Abstract;


namespace Bolnica.Model
{
   public class Prescription : IIdentifiable<int>
    {
        public int ID;
        public string Dosage;
        public DateTime ExpirationDate;
        public Medication Medication;

        public Prescription(int id, DateTime expirationDate, string dosage, Medication medication)
        {
            ID = id;
            ExpirationDate = expirationDate;
            Dosage = dosage;
            Medication = medication;
        }

        public int GetId() => ID;

        public void SetId(int id) => ID = id;

    }
}
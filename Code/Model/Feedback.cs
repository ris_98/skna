using System;
using Bolnica.Repository.Abstract;

namespace Bolnica.Model
{
    public class Feedback : IIdentifiable<int>
    {
        public int ID;
        public int Rating;
        public String Message;

        public Feedback(int iD, int rating, string message)
        {
            ID = iD;
            Rating = rating;
            Message = message;
        }

        public int GetId() => ID;

        public void SetId(int id) => ID = id;
    }
}
﻿namespace Bolnica.Model.ModelDTO
{
    public class HeadmasterDTO
    {
        public string EMail;
        public string Username;
        public string Password;
        public int ID;
        public string FirstName;
        public string LastName;
        public string TelephoneNumber;
        public string JMBG;
        public AdressDTO AdressDTO;

        public HeadmasterDTO(string eMail, string username, string password, int iD, string firstName, string lastName, string telephoneNumber, string jMBG, AdressDTO adressDTO)
        {
            EMail = eMail;
            Username = username;
            Password = password;
            ID = iD;
            FirstName = firstName;
            LastName = lastName;
            TelephoneNumber = telephoneNumber;
            JMBG = jMBG;
            AdressDTO = adressDTO;
        }
    }
}

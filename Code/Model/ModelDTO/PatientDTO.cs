using System;

namespace Bolnica.Model.ModelDTO
{
   public class PatientDTO
   {
        public int ID;
        public string EMail;
        public string Username;
        public string Password;
        public string FirstName;
        public string LastName;
        public string TelephoneNumber;
        public string JMBG;
        public AdressDTO Adress;

        public PatientDTO()
        {
        }

        public PatientDTO(string eMail, string username, string password, int iD, string firstName, string lastName, string telephoneNumber, string jMBG, AdressDTO adress)
        {
            EMail = eMail;
            Username = username;
            Password = password;
            ID = iD;
            FirstName = firstName;
            LastName = lastName;
            TelephoneNumber = telephoneNumber;
            JMBG = jMBG;
            Adress = adress;
        }
    }
}
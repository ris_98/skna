using System;

namespace Bolnica.Model.ModelDTO
{
   public class AppointmentDTO
   {
        public int ID;
        public int Duration;
        public DateTime DateTime;
        public AppointmentType AppointmentType;
        public PatientDTO PatientDTO;
        public DoctorDTO DoctorDTO;

        public AppointmentDTO(){}

        public AppointmentDTO(int id, DateTime dateTime,int duration, AppointmentType appointmentType, PatientDTO patientDTO, DoctorDTO doctorDTO)
        {
            ID = id;
            DateTime = dateTime;
            AppointmentType = appointmentType;
            DoctorDTO = doctorDTO;
            PatientDTO = patientDTO;
            Duration = duration;
        }
        
    }
}
namespace Bolnica.Model.ModelDTO
{
    public class ExaminationReportDTO 
    {
        public int ID;
        public PrescriptionDTO Prescription;
        public DiagnosisDTO Diagnosis;
        public LabTestRequisitionDTO LabTestRequisition;

        public ExaminationReportDTO() {}
        public ExaminationReportDTO(int iD)
        {
            ID = iD;
        }

        public ExaminationReportDTO(int iD, PrescriptionDTO prescription, DiagnosisDTO diagnosis, LabTestRequisitionDTO labTestRequisition)
        {
            ID = iD;
            Prescription = prescription;
            Diagnosis = diagnosis;
            LabTestRequisition = labTestRequisition;
        }

    }
}
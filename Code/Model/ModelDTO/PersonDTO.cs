namespace Bolnica.Model.ModelDTO
{
   public class PersonDTO
   {
        public int ID;
        public string FirstName;
        public string LastName;
        public string TelephoneNumber;
        public string JMBG;
        public AdressDTO Adress;

        public PersonDTO() {}

        public PersonDTO(int iD, string firstName, string lastName, string telephoneNumber, string jMBG, AdressDTO adress)
        {
            ID = iD;
            FirstName = firstName;
            LastName = lastName;
            TelephoneNumber = telephoneNumber;
            JMBG = jMBG;
            Adress = adress;
        }
    }
}
namespace Bolnica.Model.ModelDTO
{
    public class QuestionDTO
    {

        public int ID;
        public string QuestionContent;
        public string QuestionAnswer;
        public PatientDTO Patient;

        public QuestionDTO(int iD, string questionContent)
        {
            ID = iD;
            QuestionContent = questionContent;
        }

        public QuestionDTO(int iD, string questionContent, string questionAnswer, PatientDTO patient)
        {
            ID = iD;
            QuestionContent = questionContent;
            Patient = patient;
            QuestionAnswer = questionAnswer;
        }

    }
}
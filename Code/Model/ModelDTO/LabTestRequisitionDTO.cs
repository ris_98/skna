﻿namespace Bolnica.Model.ModelDTO
{
    public class LabTestRequisitionDTO 
    {
        public int ID;
        public string Note;

        public LabTestRequisitionDTO() {}
        public LabTestRequisitionDTO(int iD, string note)
        {
            ID = iD;
            Note = note;
        }

    }
}
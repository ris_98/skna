namespace Bolnica.Model.ModelDTO
{
   public class UserDTO
   {

        public string EMail;
        public string Username;
        public string Password;
        public int ID;
        public string FirstName;
        public string LastName;
        public string TelephoneNumber;
        public string JMBG;
        public AdressDTO Adress;

        public UserDTO()
        {
            Adress = new AdressDTO();
        }

        public UserDTO(string username, string password)
        {
            Username = username;
            Password = password;
        }

        public UserDTO(string eMail, string username, string password)
        {
            EMail = eMail;
            Username = username;
            Password = password;
        }

        public UserDTO(int iD, string eMail, string username, string password, string firstName, string lastName, string telephoneNumber, string jMBG, AdressDTO adress)
        {
            ID = iD;
            EMail = eMail;
            Username = username;
            Password = password;
            FirstName = firstName;
            LastName = lastName;
            TelephoneNumber = telephoneNumber;
            JMBG = jMBG;
            Adress = adress;
        }
    }
}
using System;

namespace Bolnica.Model.ModelDTO
{
   public class PrescriptionDTO
   {
        public int ID;
        public string Dosage;
        public DateTime ExpirationDate;
        public MedicationDTO Medication;

        public PrescriptionDTO(int iD, DateTime expirationDate, string dosage, MedicationDTO medication)
        {
            ID = iD;
            ExpirationDate = expirationDate;
            Dosage = dosage;
            Medication = medication;
        }

    }
}
namespace Bolnica.Model.ModelDTO
{
    public class DoctorFeedbackDTO
    {
        public int ID;
        public int Rating;
        public int NumberOfRatings;
        public DoctorDTO Doctor;

        public DoctorFeedbackDTO(int iD, int rating, int numberOfRatings, DoctorDTO doctor)
        {
            ID = iD;
            Rating = rating;
            NumberOfRatings = numberOfRatings;
            Doctor = doctor;
        }

        public DoctorFeedbackDTO() { }
        
    }
}
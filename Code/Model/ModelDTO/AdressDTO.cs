﻿using System;

namespace Bolnica.Model.ModelDTO
{
    public class AdressDTO
    {
        public int ID;
        public String Country;
        public String City;
        public String ZipCode;
        public String Street;

        public AdressDTO(){}
        public AdressDTO(int id,string country, string city, string zipCode, string street)            
        {
            ID = id;
            Country = country;
            City = city;
            ZipCode = zipCode;
            Street = street;
        }
    }
}
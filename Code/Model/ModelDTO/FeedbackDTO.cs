using System;

namespace Bolnica.Model.ModelDTO
{
   public class FeedbackDTO
   {
        public int ID;
        public int Rating;
        public String Message;

        public FeedbackDTO(int iD, int rating, string message)
        {
            ID = iD;
            Rating = rating;
            Message = message;
        }

    }
}
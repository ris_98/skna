using System.Collections.Generic;

namespace Bolnica.Model.ModelDTO
{
   public class RoomDTO
   {
        public int ID;
        public RoomType RoomType;
        public List<PatientDTO> Patient;
        public List<RoomEquipmentDTO> RoomEquipment;

        public RoomDTO()
        {
        }

        public RoomDTO(int iD, RoomType roomType, List<PatientDTO> patient, List<RoomEquipmentDTO> roomEquipment)
        {
            ID = iD;
            RoomType = roomType;
            Patient = patient;
            RoomEquipment = roomEquipment;
        }
    }
}
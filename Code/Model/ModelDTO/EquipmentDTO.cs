using System;

namespace Bolnica.Model.ModelDTO
{
    public class EquipmentDTO
    {
        public int ID;
        public int Quantity;
        public int AlertAmount;
        public String Name;
        public String Description;
        public EquipmentType Type;

        public EquipmentDTO(int iD, int quantity, int alertAmount, string name, string description, EquipmentType type)
        {
            ID = iD;
            Quantity = quantity;
            AlertAmount = alertAmount;
            Name = name;
            Description = description;
            Type = type;
        }
    }
}
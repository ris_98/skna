using System;

namespace Bolnica.Model.ModelDTO
{
   public class DoctorDTO
   {
        public int ID;
        public float Rating;
        public String EMail;
        public String Username;
        public String Password;
        public String FirstName;
        public String LastName;
        public String JMBG;
        public String TelephoneNumber;
        public String Specialization;
        public AdressDTO Adress;

        public DoctorDTO(){}

        public DoctorDTO(string eMail, string username, string password, int iD, string firstName, string lastName, string telephoneNumber, string jMBG, AdressDTO adress, string specialization, float rating)
        {
            EMail = eMail;
            Username = username;
            Password = password;
            ID = iD;
            FirstName = firstName;
            LastName = lastName;
            TelephoneNumber = telephoneNumber;
            JMBG = jMBG;
            Adress = adress;
            Specialization = specialization;
            Rating = rating;
        }
    }
}
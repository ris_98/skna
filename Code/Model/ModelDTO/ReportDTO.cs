namespace Bolnica.Model.ModelDTO
{
    public class ReportDTO
    {
        public int ID;
        public string Data;

        public ReportDTO(int iD, string data)
        {
            ID = iD;
            Data = data;
        }
    }
}
﻿namespace Bolnica.Model.ModelDTO
{
    public class MedicalRecordDTO
    {
        public int ID;
        public ExaminationReportDTO ExaminationReport;
        public AppointmentDTO Appointment;

        public MedicalRecordDTO() {}

        public MedicalRecordDTO(int iD, ExaminationReportDTO examinationReport, AppointmentDTO appointment)
        {
            ID = iD;
            ExaminationReport = examinationReport;
            Appointment = appointment;
        }

    }
}
﻿using System;

namespace Bolnica.Model.ModelDTO
{
    public class RoomEquipmentDTO
    {
        public EquipmentDTO equipment;
        public int Quantity;

        public RoomEquipmentDTO(EquipmentDTO equipment, int quantity)
        {
            this.equipment = equipment;
            Quantity = quantity;
        }
    }
}
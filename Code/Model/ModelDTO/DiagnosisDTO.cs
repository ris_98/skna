using System;

namespace Bolnica.Model.ModelDTO
{
    public class DiagnosisDTO
    {
        public int ID;
        public String Name;

        public DiagnosisDTO() { }
        public DiagnosisDTO(int iD, string name)
        {
            ID = iD;
            Name = name;
        }

    }
}
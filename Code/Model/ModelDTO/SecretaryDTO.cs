namespace Bolnica.Model.ModelDTO
{
    public class SecretaryDTO
    {
        public string EMail;
        public string Username;
        public string Password;
        public int ID;
        public string FirstName;
        public string LastName;
        public string TelephoneNumber;
        public string JMBG;
        public AdressDTO Adress;

        public SecretaryDTO(string eMail, string username, string password, int iD, string firstName, string lastName, string telephoneNumber, string jMBG, AdressDTO adress)
        {
            ID = iD;
            EMail = eMail;
            Username = username;
            Password = password;
            FirstName = firstName;
            LastName = lastName;
            TelephoneNumber = telephoneNumber;
            JMBG = jMBG;
            Adress = adress;
        }
    }
}
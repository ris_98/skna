using System;

namespace Bolnica.Model.ModelDTO
{
    public class MedicationDTO
    {
        public int ID;
        public int Quantity;
        public int AlertAmount;
        public int NumberOfVerification;
        public String Name;
        public Boolean Approved;

        public MedicationDTO(int iD, string name, bool approved, int quantity, int numberOfVerification, int alertAmount)
        {
            ID = iD;
            Name = name;
            Approved = approved;
            Quantity = quantity;
            NumberOfVerification = numberOfVerification;
            AlertAmount = alertAmount;
        }


    }
}
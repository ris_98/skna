﻿namespace Bolnica.Model.ModelDTO
{
    public class NotVerifiedMedicationDTO
    {

        public int ID;
        public string Name;
        public string Note;


        public NotVerifiedMedicationDTO(int iD, string name, string note)
        {
            ID = iD;
            Name = name;
            Note = note;
        }
    }
}
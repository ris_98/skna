namespace Bolnica.Model
{
   public class Secretary : Employees
   {

        public Secretary(string eMail,string username ,string password, int iD, string firstName, string lastName, string telephoneNumber, string jMBG, Adress adress)
        : base(eMail,username, password, iD, firstName, lastName, telephoneNumber, jMBG, adress){}

        public int GetId() => ID;

        public void SetId(int id) => ID = id;
    }
}
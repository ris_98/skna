using System;

namespace Bolnica.Model
{
    public class Doctor : Employees
    {
        public String specialization;
        public float rating;

        public int GetId() => ID;

        public void SetId(int id) => ID = id;

        public Doctor() :base(){}

        public Doctor(string eMail,string username, string password, int iD, string firstName, string lastName, string telephoneNumber, string jMBG, Adress adress, String specialization, float rating)
            : base(eMail,username, password, iD, firstName, lastName, telephoneNumber, jMBG, adress)
        {
            this.specialization = specialization;
            this.rating = rating;
        }
   
    }
}
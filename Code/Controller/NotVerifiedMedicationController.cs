﻿using Bolnica.Model.ModelDTO;
using Bolnica.Service;
using System.Collections.Generic;

namespace Bolnica.Controller
{
    public class NotVerifiedMedicationController
    {
        public NotVerifiedMedicationService notVerifiedMedicationService = new NotVerifiedMedicationService();

        public void SendToVerify(NotVerifiedMedicationDTO medicineForVerification) => notVerifiedMedicationService.SendMedicationToVerify(medicineForVerification);

        public List<NotVerifiedMedicationDTO> ShowExperimentMedication() => notVerifiedMedicationService.ShowExperimentMedication();

    }
}
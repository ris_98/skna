﻿using Bolnica.Model;
using Bolnica.Model.ModelDTO;
using Bolnica.Service;
using System.Collections.Generic;

namespace Bolnica.Controller
{
    public class MedicalRecordController
    {
        private MedicalRecordService medicalRecordService = new MedicalRecordService();
        public MedicalRecordDTO GetById(int id) => medicalRecordService.GetById(id);
        public List<MedicalRecordDTO> ShowMedicalRecords(UserDTO userDTO) => medicalRecordService.ShowMedicalRecords(userDTO);
    }
}
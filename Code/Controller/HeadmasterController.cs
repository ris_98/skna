using Bolnica.Model.ModelDTO;
using Service;

namespace Bolnca.Controller
{
   public class HeadmasterController
   {
        HeadmasterService headmasterService = new HeadmasterService();

        public void RegisterDoctor(DoctorDTO doctor) => headmasterService.RegisterDoctor(doctor);

        public void RegisterSecretary(SecretaryDTO secretary) => headmasterService.RegisterSecretary(secretary);

        public void MakeProcuremnt(ProcurementDTO procurment) => headmasterService.MakeProcuremnt(procurment);

        public void DeleteEmployee(UserDTO employee) => headmasterService.DeleteEmployee(employee);

        public void GiveDoctorSpecialization(DoctorDTO doctor) => headmasterService.GiveDoctorSpecialization(doctor);

    }
}
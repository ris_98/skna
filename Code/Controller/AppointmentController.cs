using Bolnica.Service;
using Bolnica.Model.ModelDTO;
using System;
using System.Collections.Generic;
using Bolnica.Model;

namespace Bolnica.Controller
{
    public class AppointmentController
    {
        private AppointmentService appointmentService = new AppointmentService();

        public void ScheduleAppointment(AppointmentDTO appointment) => appointmentService.ScheduleAppointment(appointment);

        public void ModifyAppointment(AppointmentDTO appointment) => appointmentService.ModifyAppointment(appointment);

        public void CancelAppointment(AppointmentDTO appointment) => appointmentService.CancelAppointment(appointment);

        public List<AppointmentDTO> ShowScheduledAppointments(UserDTO user) => appointmentService.ShowScheduledAppointments(user);

        public AppointmentDTO GetAppointmentByID(int id) => appointmentService.GetAppointmentByID(id);

        public List<AppointmentDTO> RecommendAppointmentPrirortyDoctor(DataRange dataRange, DoctorDTO doctor) => appointmentService.RecomendAppointmentDoctor(dataRange, doctor);

        public List<AppointmentDTO> RecommendAppointmentPrirortyDateRange(DataRange dataRange, DoctorDTO doctor) => appointmentService.RecomendAppointmentDateRange(dataRange, doctor);

        public List<AppointmentDTO> GetFreeAppointments(DateTime date) => appointmentService.GetFreeApointments(date);
    }
}
using Bolnca.Service;
using Bolnica.Model.ModelDTO;
using System.Collections.Generic;

namespace Controller
{
    public class MedicationController
    {
        public MedicationService medicationService = new MedicationService();

        public void AddMedication(MedicationDTO medication) => medicationService.AddMedication(medication);

        public void VerifyMedication(MedicationDTO medicine, DoctorDTO doctor) => medicationService.VerifyMedication(medicine, doctor);

        public List<MedicationDTO> ShowMedication() => medicationService.ShowMedication();

        public List<MedicationDTO> ShowApprovedMedication() => medicationService.ShowApprovedMedication();

        public List<MedicationDTO> ShowNotApprovedMedication() => medicationService.ShowNotApprovedMedication();

        public void UpdateQuantity(MedicationDTO medication) => medicationService.UpdateQuantity(medication);

        public MedicationDTO GetMedicationByID(int id) => medicationService.GetMedicationByID(id);

        public void DeleteMedication(MedicationDTO medication) => medicationService.DeleteMedication(medication);


    }
}
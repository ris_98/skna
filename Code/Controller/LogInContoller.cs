using Bolnica.Model.ModelDTO;
using Bolnica.Service;
using System;

namespace Bolnica.Controller
{
    public class LogInContoller
    {
        private LogInService logInService = new LogInService();

        public UserDTO SignIn(UserDTO user) => logInService.SignIn(user);

        public bool SignOut(UserDTO user) => logInService.SignOut(user);

    }
}
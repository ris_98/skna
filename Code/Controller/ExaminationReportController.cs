﻿using Bolnica.Model.ModelDTO;
using Bolnica.Service;

namespace Bolnica.Controller
{
    public class ExaminationReportController
    {
        private ExaminationReportService examinationReportService = new ExaminationReportService();

        public ExaminationReportDTO GetById(int id) => examinationReportService.GetById(id);

        public void AddPerscription(ExaminationReportDTO examinationReport, PrescriptionDTO prescription) 
            => examinationReportService.AddPerscription(examinationReport, prescription);

        public void AddLabTestRequistion(ExaminationReportDTO examinationReport, LabTestRequisitionDTO labTestRequisition) 
            => examinationReportService.AddLabTestRequistion(examinationReport, labTestRequisition);

        public void AddDiagnosis(ExaminationReportDTO examinationReport, DiagnosisDTO diagnosis) 
            => examinationReportService.AddDiagnosis(examinationReport, diagnosis);


    }
}
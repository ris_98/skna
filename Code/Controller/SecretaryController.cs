using Bolnica.Model.ModelDTO;
using Bolnica.Service;

namespace Bolnica.Controller
{
   public class SecretaryController
   {
        private SecretaryService secretaryService = new SecretaryService();

        public void RegisterUser(PatientDTO user) => secretaryService.RegisterUser(user);

        public void RegisterPerson(PersonDTO person) => secretaryService.RegisterPerson(person);

        public void UpdateUser(PatientDTO user) => secretaryService.UpdateUser(user); 

        public void UpdatePerson(PersonDTO person) => secretaryService.UpdatePerson(person);
      
        public void UpdateMYACC(SecretaryDTO secretaryDTO) => secretaryService.UpdateMYACC(secretaryDTO);

        public PatientDTO GetPatientByID(int id) =>  secretaryService.GetPatientByID(id);

        public PersonDTO GetPersonByID(int id) =>  secretaryService.GetPersonByID(id);
        
    }
}
using Bolnica.Model.ModelDTO;
using Bolnica.Service;
using System.Collections.Generic;

namespace Bolnca.Controller
{
    public class QuestionController
    {
        public QuestionService questionService = new QuestionService();

        public void AskQuestion(QuestionDTO question) => questionService.AskQuestion(question);

        public void DeleteQuestion(QuestionDTO question) => questionService.DeleteQuestion(question);

        public void AnswerQuestion(QuestionDTO question) => questionService.AnswerQuestion(question);

        public List<QuestionDTO> ShowQuestions() => questionService.ShowQuestions();

        public QuestionDTO GetQuestionByID(int id) => questionService.GetQuestionByID(id);

    }
}
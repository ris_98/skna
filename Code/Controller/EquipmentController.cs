﻿using Bolnica.Model.ModelDTO;
using Bolnica.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolnca.Controller
{
    public class EquipmentController
    {
        EquipmentService equipmentService = new EquipmentService();

        public List<EquipmentDTO> GetAllEquipment() => equipmentService.GetAllEquipment();

        public List<EquipmentDTO> GetAllStacionaryEquipment() => equipmentService.GetAllStacionaryEquipment();

        public List<EquipmentDTO> GetAllConsumableEquipment() => equipmentService.GetAllConsumableEquipment();

        public EquipmentDTO GetEquipmentByID(int id) => equipmentService.GetEquipmentByID(id);

        public void UpdateEquipmentQuantity(EquipmentDTO equipment) => equipmentService.UpdateEquipmentQuantity(equipment);

        public void AddEquipment(EquipmentDTO equipment) => equipmentService.AddEquipment(equipment);

        public void DeleteEquipment(EquipmentDTO equipment) => equipmentService.DeleteEquipment(equipment);

        public void UpdateEquipment(EquipmentDTO equipment) => equipmentService.UpdateEquipment(equipment);

    }
}

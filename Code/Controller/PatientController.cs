using Bolnica.Model.ModelDTO;
using Bolnica.Service;

namespace Bolnica.Controller
{
    public class PatientController
    {
        private PatientService patientService = new PatientService();

        public void DoctorFeedback(DoctorFeedbackDTO feedback) => patientService.DoctorFeedback(feedback);

        public void RegisterPatient(UserDTO user) => patientService.RegisterPatient(user);

    }
}
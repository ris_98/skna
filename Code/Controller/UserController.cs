using Bolnica.Model;
using Bolnica.Model.ModelDTO;
using Bolnica.Service;
using System;
using System.Collections.Generic;

namespace Controller
{
    public class UserController
    {
        private UserService userService = new UserService();

        public void LeaveFeedback(FeedbackDTO feedback) => userService.LeaveFeedback(feedback);

        public void LeaveBugReport(ReportDTO report) => userService.LeaveBugReport(report);
    
        public List<DoctorDTO> GetAllDoctors() =>  userService.GetAllDoctors();

        public DoctorDTO GetDoctorByID(int id) => userService.GetDoctorByID(id);

        public List<DoctorDTO> GetAllDoctorsBySpecialization(string specialization) => userService.GetAllDoctorsBySpecialization(specialization);

        public void UpdateMyAccDoctor(DoctorDTO doctor) => userService.UpdateMyAccDoctor(doctor);

        public void UpdateMyAccPatient(PatientDTO patient) => userService.UpdateMyAccPatient(patient);

        public void UpdateMyAccSecretary(SecretaryDTO secretary) => userService.UpdateMyAccSecretary(secretary);

        public void UpdateMyAccHeadmaster(HeadmasterDTO headmaster) => userService.UpdateMyAccHeadmaster(headmaster);

    }
}
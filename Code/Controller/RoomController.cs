﻿using Bolnica.Model.ModelDTO;
using Bolnica.Service;
using System.Collections.Generic;

namespace Bolnica.Controller
{
    public class RoomController
    {
        RoomService roomService = new RoomService();

        public void AddRoom(RoomDTO room) => roomService.AddRoom(room);

        public void DeleteRoom(RoomDTO room) => roomService.DeleteRoom(room);

        public void RenovateRoom(RoomDTO room) => roomService.RenovateRoom(room);

        public List<RoomDTO> getAllRooms() => roomService.getAllRooms();

        public RoomDTO GetRoomByID(int id) => roomService.GetRoomByID(id);

        public void AddEqupmentToRoom(RoomDTO room, RoomEquipmentDTO roomEquipmentDTO) => roomService.AddEqupmentToRoom(room, roomEquipmentDTO);

        public void AddPatientToRoom(RoomDTO room, PatientDTO patient) => roomService.AddPatientToRoom(room, patient);

        public void RemovePatientFromRoom(RoomDTO room, PatientDTO patient) => roomService.RemovePatientFromRoom(room, patient);
    }
}

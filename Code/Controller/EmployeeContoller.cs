using Bolnica.Model.ModelDTO;
using Bolnica.Service;
using System.Collections.Generic;

namespace Bolnica.Controller
{
    public class EmployeeContoller
    {

        private EmployeeService employeeService = new EmployeeService();

        public List<PatientDTO> ShowPatients() => employeeService.ShowPatients();

        public PatientDTO GetPatientByID(int id) => employeeService.GetPatientByID(id);

        public List<PersonDTO> GetAllPersons() => employeeService.GetPersons();

    }
}
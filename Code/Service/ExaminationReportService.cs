﻿using Bolnica.Model;
using Bolnica.Model.ModelDTO;
using Bolnica.Repository;
using Bolnica.Repository.CSV.Converter;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;
using System.Globalization;

namespace Bolnica.Service
{
    public class ExaminationReportService
    {
        private const string Diagnosis_FILE = "../../../../../Files/examination.csv";
        private const string Prescription_FILE = "../../../../../Files/prescription.csv";
        private const string LabTestRequisition_FILE = "../../../../../Files/labtestrequisition.csv";

        private const string CSV_DELIMITER = ",";
        private const string DATETIME_FORMAT = "dd.MM.yyyy. hh:mm";

        private LabTestRequisitionRepository labTestRequisitionRepository = new LabTestRequisitionRepository(
           new CSVStream<LabTestRequisition>(LabTestRequisition_FILE, new LabTestRequisitionCSVConverter(CSV_DELIMITER)), new IntSequencer());

        private ExaminationReportRepository examinationReportRepository = new ExaminationReportRepository(
           new CSVStream<ExaminationReport>(Diagnosis_FILE, new ExaminationReportCSVConverter(CSV_DELIMITER)), new IntSequencer());

        private PrescriptionRepository prescriptionRepository = new PrescriptionRepository(
            new CSVStream<Prescription>(Prescription_FILE, new PrescriptionCSVConverter(CSV_DELIMITER, DATETIME_FORMAT)), new IntSequencer());
       
        private DiagnosisRepository diagnosisRepository = new DiagnosisRepository(
           new CSVStream<Diagnosis>(Diagnosis_FILE, new DiagnosisCSVConverter(CSV_DELIMITER)), new IntSequencer());


        UtilityService utilityService = new UtilityService();

        public ExaminationReportDTO GetById(int id) => utilityService.ConvertExaminationReportToExaminationReportDTO(examinationReportRepository.GetById(id));


        public void AddPerscription(ExaminationReportDTO examinationReportDTO, PrescriptionDTO prescriptionDTO)
        {
            ExaminationReport examinationReport = utilityService.ConvertExaminationReportDTOToExaminationReport(examinationReportDTO);
            Prescription prescription = utilityService.ConvertPrescriptionDTOToPrescription(prescriptionDTO);
            examinationReport.Prescription = prescription;
            examinationReportRepository.Update(examinationReport);
            prescriptionRepository.Create(prescription);
        }

        public void AddLabTestRequistion(ExaminationReportDTO examinationReportDTO, LabTestRequisitionDTO labTestRequisitioDTO)
        {
            ExaminationReport examinationReport = utilityService.ConvertExaminationReportDTOToExaminationReport(examinationReportDTO);
            LabTestRequisition labTestRequisition = utilityService.ConvertLabTestRequisitionDTOToLabTestRequisition(labTestRequisitioDTO);
            examinationReport.LabTestRequisition = labTestRequisition;
            examinationReportRepository.Update(examinationReport);
            labTestRequisitionRepository.Create(labTestRequisition);       
        }

        public void AddDiagnosis(ExaminationReportDTO examinationReportDTO, DiagnosisDTO diagnosisDTO)
        {
            ExaminationReport examinationReport = utilityService.ConvertExaminationReportDTOToExaminationReport(examinationReportDTO);
            Diagnosis diagnosis = utilityService.ConvertDiagnosisDTOToDiagnosis(diagnosisDTO);
            examinationReport.Diagnosis = diagnosis;
            examinationReportRepository.Update(examinationReport);
            diagnosisRepository.Create(diagnosis);
        }

    }
}
using Bolnica.Model;
using Bolnica.Model.ModelDTO;
using Bolnica.Repository;
using Bolnica.Repository.CSV.Converter;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;
using System;
using System.Collections.Generic;

namespace Bolnica.Service
{
   public class QuestionService
   {
        private UtilityService utilityService = new UtilityService();
        private const string ACCOUNT_FILE = "../../../../../Files/questions.csv";
        private const string Patient_FILE = "../../../../../Files/patient.csv";
        private const string CSV_DELIMITER = ",";

        private PatientRepository patientRepository = new PatientRepository(
            new CSVStream<Patient>(Patient_FILE, new PatientCSVConverter(CSV_DELIMITER)),
            new IntSequencer());

        private QuestionRepository questionRepository = new QuestionRepository(
                new CSVStream<Question>(ACCOUNT_FILE, new QuestionCSVConverter(CSV_DELIMITER)),
                new IntSequencer());
        

        public void AskQuestion(QuestionDTO questionDTO)
        {
            Question question = utilityService.ConvertQuestionDTOToQuestion(questionDTO);
            questionRepository.Create(question);
        }

        public void DeleteQuestion(QuestionDTO questionDTO)
        {
            Question question = utilityService.ConvertQuestionDTOToQuestion(questionDTO);
            questionRepository.Delete(question);
        }
      
        public void AnswerQuestion(QuestionDTO questionDTO)
        {
            Question question = utilityService.ConvertQuestionDTOToQuestion(questionDTO);
            questionRepository.Update(question);
        }

        private List<QuestionDTO> ConvertQuestionToQuestionDTOList(List<Question> questions)
        {
            List<QuestionDTO> questionDTOs = new List<QuestionDTO>();
            foreach (Question q in questions)
            {
                QuestionDTO questionDTO = utilityService.ConvertQuestionToQuestionDTO(q);
                questionDTOs.Add(questionDTO);
            }
            return questionDTOs;
        }

        public List<QuestionDTO> ShowQuestions()
        {
            List<Question> questions = (List<Question>)questionRepository.GetAll();
            return ConvertQuestionToQuestionDTOList(questions);
        }
      

        public Question NotifyPatientWhenAnswered()
        {
            throw new NotImplementedException();
        }

        public QuestionDTO GetQuestionByID(int id)
        {
            Question question = questionRepository.GetById(id);
            QuestionDTO questionDTO = utilityService.ConvertQuestionToQuestionDTO(question);
            return questionDTO;
        }
    }
}
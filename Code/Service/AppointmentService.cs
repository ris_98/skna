using Bolnica.Model;
using Bolnica.Model.ModelDTO;
using Bolnica.Repository;
using Bolnica.Repository.CSV.Converter;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Bolnica.Service
{
   public class AppointmentService
   {
        private const string Patient_FILE = "../../../../../Files/patient.csv";
        private const string DOCTOR_FILE = "../../../../../Files/doctor.csv";
        private const string EXAMINATION_FILE = "../../../../../Files/examination.csv";
        private const string ADRESS_FILE = "../../../../../Files/adress.csv";
        private const string APPOINTMENT_FILE = "../../../../../Files/appointment.csv";
        private const string SECRATARY_FILE = "../../../../../Files/secretary.csv";

        private const string CSV_DELIMITER = ",";
        private const string DATETIME_FORMAT = "dd.MM.yyyy. hh:mm";
        private const int NumberOfDaysForWiderRecommendatationSearch = 2;

        private AdressRepository adressRepository = new AdressRepository(
            new CSVStream<Adress>(ADRESS_FILE, new AdressCSVConverter(CSV_DELIMITER)),
            new IntSequencer());

        private PatientRepository patientRepository = new PatientRepository(
            new CSVStream<Patient>(Patient_FILE, new PatientCSVConverter(CSV_DELIMITER)),
            new IntSequencer());

        private DoctorRepository doctorRepository = new DoctorRepository(
            new CSVStream<Doctor>(DOCTOR_FILE, new DoctorCSVConverter(CSV_DELIMITER)),
            new IntSequencer());

        private ExaminationReportRepository examinationReportRepository = new ExaminationReportRepository(
            new CSVStream<ExaminationReport>(EXAMINATION_FILE, new ExaminationReportCSVConverter(CSV_DELIMITER)),
            new IntSequencer());

        private AppointmentRepository appointmentRepository = new AppointmentRepository(
            new CSVStream<Appointment>(APPOINTMENT_FILE, new AppointmentCSVConverter(CSV_DELIMITER, DATETIME_FORMAT)),
            new IntSequencer());

        private SecretaryRepository secretaryRepository = new SecretaryRepository(
            new CSVStream<Secretary>(SECRATARY_FILE, new SecretaryCSVConverter(CSV_DELIMITER)),
            new IntSequencer());


        UtilityService utilitiService = new UtilityService();

      
        public void ScheduleAppointment(AppointmentDTO appointmentDTO)
        {
            Appointment appointment = ConvertAppointmetnDTOTOAppointment(appointmentDTO);
            appointmentRepository.Create(appointment);

        }
        private Appointment ConvertAppointmetnDTOTOAppointment(AppointmentDTO appointmentDTO)
        {
            Patient patient = patientRepository.GetById(appointmentDTO.PatientDTO.ID);
            Doctor doctor = doctorRepository.GetById(appointmentDTO.DoctorDTO.ID);
            return new Appointment(appointmentDTO.ID, appointmentDTO.DateTime,appointmentDTO.Duration ,appointmentDTO.AppointmentType, patient,doctor);

        }
      
        public void ModifyAppointment(AppointmentDTO appointmentDTO)
        {
            Appointment appointment = ConvertAppointmetnDTOTOAppointment(appointmentDTO);
            appointmentRepository.Update(appointment);

        }
      
        public void CancelAppointment(AppointmentDTO appointmentDTO)
        {
            Appointment appointment = ConvertAppointmetnDTOTOAppointment(appointmentDTO);
            appointmentRepository.Delete(appointment);
        }
      
        public List<AppointmentDTO> ShowScheduledAppointments(UserDTO user)
        {
            List<Appointment> appointments = getAppointments(user);
            return utilitiService.ConvertAppointmentToAppointmentDTOList(appointments);
        }

        private List<Appointment> getAppointments(UserDTO user)
        {
            string userType = utilitiService.findUserType(utilitiService.ConvertUserDTOToUser(user));
            if(userType.Equals("Patient"))  return (List<Appointment>) appointmentRepository.GetAppointmentByPatient(user.ID);
            else if (userType.Equals("Doctor")) return (List<Appointment>)appointmentRepository.GetAppointmentByDoctor(user.ID);
            else if (userType.Equals("Secretary")) return (List<Appointment>)appointmentRepository.GetAll();
            else return new List<Appointment>();
        }



        public AppointmentDTO GetAppointmentByID(int id)
        {
            Appointment appointment = appointmentRepository.GetById(id);
            return utilitiService.ConvertAppointmentToAppointmentDTO(appointment);
        }


        public List<AppointmentDTO> GetFreeApointments(DateTime date)
        {
            List<Appointment> scheduledAppointments = (List<Appointment>)appointmentRepository.GetAppointmentByDay(date);
            List<Doctor> doctors = (List<Doctor>)doctorRepository.GetAll();
            List<Appointment> freeAppointments = new List<Appointment>();

            foreach(Doctor doctor in doctors)
                GetFreeAppointmentsByDoctor(scheduledAppointments, freeAppointments, doctor);
            
            return utilitiService.ConvertAppointmentToAppointmentDTOList(freeAppointments);
        }

        private void GetFreeAppointmentsByDoctor(List<Appointment> scheduledAppointments, List<Appointment> freeAppointments, Doctor doctor)
        {
            List<Appointment> appointmentsForDoctor = new List<Appointment>();

            GetScheduledAppointmentsForDoctor(scheduledAppointments, doctor, appointmentsForDoctor);

            DateTime startTime = new DateTime(2020, 1, 1, 10, 0, 0);
            DateTime endTime = new DateTime(2020, 1, 1, 18, 0, 0);
            DateTime currentAppointmentTime = startTime;

            while (TimeSpan.Compare(currentAppointmentTime.TimeOfDay, endTime.TimeOfDay) > 0)
                currentAppointmentTime = FillFreeAppointments(freeAppointments, doctor, appointmentsForDoctor, currentAppointmentTime);

        }

        private static void GetScheduledAppointmentsForDoctor(List<Appointment> scheduledAppointments, Doctor doctor, List<Appointment> appointmentsForDoctor)
        {
            foreach (Appointment sap in scheduledAppointments)
                if (sap.Doctor.ID == doctor.ID)
                    appointmentsForDoctor.Add(sap);
        }

        private DateTime FillFreeAppointments(List<Appointment> freeAppointments, Doctor doctor, List<Appointment> appointmentsForDoctor, DateTime currentAppointmentTime)
        {
            int i = 0;
            int endI = appointmentsForDoctor.Count();
            foreach (Appointment currentAppointment in appointmentsForDoctor)
            {
                i++;
                if (TimeSpan.Compare(currentAppointment.DateTime.TimeOfDay, currentAppointmentTime.TimeOfDay) == 0)  break;
                if (i == endI)
                {
                    Appointment appointment = new Appointment(0, currentAppointmentTime, 30, AppointmentType.Examination, patientRepository.GetById(0), doctor);
                    freeAppointments.Add(appointment);
                }
            }
            return currentAppointmentTime.AddMinutes(30);
        }

        public List<AppointmentDTO> RecomendAppointmentDoctor(DataRange dataRange, DoctorDTO doctorDTO)
        {
            Doctor doctor = utilitiService.ConvertDoctorDTOToDoctor(doctorDTO);
            List<Appointment> freeAppointments = new List<Appointment>();

            while (DateTime.Compare(dataRange.StartDate.AddDays(NumberOfDaysForWiderRecommendatationSearch), dataRange.EndDate)<=0);
            {
                List<Appointment> scheduledAppointments = (List<Appointment>)appointmentRepository.GetAppointmentByDay(dataRange.StartDate);
                List<Appointment> appointmentsForDoctor = new List<Appointment>();
                DateTime currentAppointmentTime = dataRange.StartDate;

                GetScheduledAppointmentsForDoctor(scheduledAppointments, doctor, appointmentsForDoctor);

                while (TimeSpan.Compare(currentAppointmentTime.TimeOfDay, doctor.WorkingHours.WorikingTime.EndDate.TimeOfDay) > 0)
                    currentAppointmentTime = FillFreeAppointments(freeAppointments, doctor, appointmentsForDoctor, currentAppointmentTime);

                if(freeAppointments.Count!=0) return utilitiService.ConvertAppointmentToAppointmentDTOList(freeAppointments);
                dataRange.StartDate = dataRange.StartDate.AddDays(1);
            }
            throw new System.Exception();

        }

        public List<AppointmentDTO> RecomendAppointmentDateRange(DataRange dataRange, DoctorDTO doctor)
        {
            DateTime startDate = dataRange.StartDate;
            
            while (DateTime.Compare(dataRange.StartDate, dataRange.EndDate) != 0)
            {
                List<AppointmentDTO> freeAppointments = GetFreeApointments(dataRange.StartDate);
                foreach (AppointmentDTO appointment in freeAppointments)
                    if (appointment.DoctorDTO.ID != doctor.ID)
                        freeAppointments.Remove(appointment);
                if (freeAppointments.Count != 0)
                    return freeAppointments;
                dataRange.StartDate = dataRange.StartDate.AddDays(1);
            }

            while(DateTime.Compare(dataRange.StartDate, dataRange.EndDate) != 0)
            {
                List<AppointmentDTO> freeAppointmentsFromOtherDoctors = GetFreeApointments(startDate);
                if (freeAppointmentsFromOtherDoctors.Count != 0)
                    return freeAppointmentsFromOtherDoctors;
                startDate = startDate.AddDays(1);
            }

            throw new System.Exception();
        }

    }
}
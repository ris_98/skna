﻿using Bolnica.Model;
using Bolnica.Model.ModelDTO;
using Bolnica.Repository;
using Bolnica.Repository.CSV.Converter;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;
using System.Collections.Generic;

namespace Bolnica.Service
{
    public class MedicalRecordService
    {
        private const string MEDICALRECORD_FILE = "../../../../../Files/medicalrecord.csv";

        private const string CSV_DELIMITER = ",";
        private const string DATETIME_FORMAT = "dd.MM.yyyy. hh:mm";

        private MedicalRecordRepository medicalRecordRepository = new MedicalRecordRepository(
          new CSVStream<MedicalRecord>(MEDICALRECORD_FILE, new MedicalRecordCSVConverter(CSV_DELIMITER)),
          new IntSequencer());
        UtilityService utilitiService = new UtilityService();

        public MedicalRecordDTO GetById(int id)
        {
            MedicalRecordDTO medicalRecordDTO = utilitiService.ConvertMedicalRecordToMedicalRecordDTO(medicalRecordRepository.GetById(id));
            return medicalRecordDTO;
        }
        public List<MedicalRecordDTO> ShowMedicalRecords(UserDTO userDTO)
        {
            List<MedicalRecordDTO> medicalRecordDTOs = utilitiService.ConvertMedicalRecordToMedicalRecordDTOList(medicalRecordRepository.GetMedicalRecordByPatient(userDTO.ID));
            return medicalRecordDTOs;
        }


    }
}
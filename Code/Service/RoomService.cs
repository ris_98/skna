﻿using Bolnica.Model;
using Bolnica.Model.ModelDTO;
using Bolnica.Repository;
using Bolnica.Repository.CSV.Converter;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolnica.Service
{
    public class RoomService
    {
        UtilityService utilityService = new UtilityService();

        private const string ROOM_FILE = "../../../../../Files/rooms.csv";
        private const string CSV_DELIMITER = ",";
        public RoomRepository roomRepository = new RoomRepository(
               new CSVStream<Room>(ROOM_FILE, new RoomCSVConverter(CSV_DELIMITER)), new IntSequencer());

        public void AddRoom(RoomDTO roomDTO)
        {
            Room room = utilityService.ConvertRoomDTOToRoom(roomDTO);
            roomRepository.Create(room);
        }
        public void DeleteRoom(RoomDTO roomDTO)
        {
            Room room = utilityService.ConvertRoomDTOToRoom(roomDTO);
            roomRepository.Delete(room);
        }
        public void RenovateRoom(RoomDTO roomDTO)
        {
            Room room = utilityService.ConvertRoomDTOToRoom(roomDTO);
            roomRepository.Update(room);
        }

        public List<RoomDTO> getAllRooms()
        {
            List<Room> rooms = (List<Room>) roomRepository.GetAll();
            return utilityService.ConvertRoomToRoomDTOList(rooms);
        }

        public RoomDTO GetRoomByID(int id) => utilityService.ConvertRoomToRoomDTO(roomRepository.GetById(id));

        public void AddEqupmentToRoom(RoomDTO roomDTO, RoomEquipmentDTO roomEquipmentDTO)
        {
            Room room = utilityService.ConvertRoomDTOToRoom(roomDTO);
            RoomEquipment roomEquipment = utilityService.ConvertRoomEQDToRoomEq(roomEquipmentDTO);
            room.RoomEquipment.Add(roomEquipment);
            roomRepository.Update(room);
        }

        public void AddPatientToRoom(RoomDTO roomDTO, PatientDTO patientDTO)
        {
            Room room = utilityService.ConvertRoomDTOToRoom(roomDTO);
            if (!IsThereSpaceForNewPatient(room))
                throw new System.Exception();
            Patient patient = utilityService.ConvertPatientDTOToPatient(patientDTO);
            room.Patient.Add(patient);
            roomRepository.Update(room);
        }

        public void RemovePatientFromRoom(RoomDTO roomDTO, PatientDTO patientDTO)
        {
            Room room = utilityService.ConvertRoomDTOToRoom(roomDTO);
            Patient patient = utilityService.ConvertPatientDTOToPatient(patientDTO);
            room.Patient.Remove(patient);
            roomRepository.Update(room);
        }

        private bool IsThereSpaceForNewPatient(Room room)
        {
            foreach(RoomEquipment roomEquipment in room.RoomEquipment)
                if (roomEquipment.equipment.Name.Equals("bed"))
                {
                    if (roomEquipment.equipment.Quantity == room.Patient.Count()) return false;
                    
                    return true;
                }
            
            return false;
        }

    }
}

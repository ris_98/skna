﻿using Bolnica.Model;
using Bolnica.Model.ModelDTO;
using Bolnica.Repository;
using Bolnica.Repository.CSV.Converter;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;
using System.Collections.Generic;

namespace Bolnica.Service
{
    public class EquipmentService
    {
        UtilityService utilityService = new UtilityService();

        private const string EQUIPMENT_FILE = "../../../../../Files/equipment.csv";
        private const string CSV_DELIMITER = ",";

        private EquipmentRepository equipmentRepository = new EquipmentRepository(
           new CSVStream<Equipment>(EQUIPMENT_FILE, new EquipmentCSVConverter(CSV_DELIMITER)), new IntSequencer());

        
        public List<EquipmentDTO> GetAllEquipment()
        {
            List<Equipment> equipment = (List<Equipment>) equipmentRepository.GetAll();
            return utilityService.ConvertEquipmentToEquipmentDTODTOList(equipment);
        }

        public List<EquipmentDTO> GetAllStacionaryEquipment()
        {
            List<Equipment> equipment = (List<Equipment>)equipmentRepository.GetAllStacionaryEquipment();
            return utilityService.ConvertEquipmentToEquipmentDTODTOList(equipment);
        }

        public List<EquipmentDTO> GetAllConsumableEquipment()
        {
            List<Equipment> equipment = (List<Equipment>)equipmentRepository.GetAllConsumableEquipment();
            return utilityService.ConvertEquipmentToEquipmentDTODTOList(equipment);
        }

        public void UpdateEquipmentQuantity(EquipmentDTO equipmentDTO)
        {
            Equipment equipment = equipmentRepository.GetById(equipmentDTO.ID);
            equipment.Quantity = equipmentDTO.Quantity;
            equipmentRepository.Update(equipment);
        }

        public EquipmentDTO GetEquipmentByID(int id)
        {
            Equipment equipment = equipmentRepository.GetById(id);
            return utilityService.ConvertEquipmentToEquipmentDTO(equipment);
        }

        public void AddEquipment(EquipmentDTO equipmentDTO)
        {
            Equipment equipment = utilityService.ConvertEquipmentDTOTOEquipment(equipmentDTO);
            equipmentRepository.Create(equipment);
        }

        public void DeleteEquipment(EquipmentDTO equipmentDTO)
        {
            Equipment equipment = utilityService.ConvertEquipmentDTOTOEquipment(equipmentDTO);
            equipmentRepository.Delete(equipment);
        }

        public void UpdateEquipment(EquipmentDTO equipmentDTO)
        {
            Equipment equipment = utilityService.ConvertEquipmentDTOTOEquipment(equipmentDTO);
            equipmentRepository.Update(equipment);
        }

    }
}

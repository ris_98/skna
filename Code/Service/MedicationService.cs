using Bolnica.Model;
using Bolnica.Model.ModelDTO;
using Bolnica.Repository;
using Bolnica.Repository.CSV.Converter;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;
using System;
using System.Collections.Generic;

namespace Bolnca.Service
{
   public class MedicationService
   {
        private const string MEDICATION_FILE = "../../../../../Files/medications.csv";
        private const string CSV_DELIMITER = ",";
        public MedicationRepository medicationRepository = new MedicationRepository(
                new CSVStream<Medication>(MEDICATION_FILE, new MedicationCSVConverter(CSV_DELIMITER)),new IntSequencer());
      

        public void AddMedication(MedicationDTO medicationDTO) => medicationRepository.Create(ConvertMedicationDTOToMedication(medicationDTO));

        private Medication ConvertMedicationDTOToMedication(MedicationDTO medicationDTO)
        {
            Medication medication = new Medication(medicationDTO.ID, medicationDTO.Name, medicationDTO.Approved, medicationDTO.NumberOfVerification, medicationDTO.NumberOfVerification, 0);
            return medication;
        }

        public void VerifyMedication(MedicationDTO medicineDTO, DoctorDTO doctorDTO)
        {
            Medication medicine = ConvertMedicationDTOToMedication(medicineDTO);
            medicine.NumberOfVerification++;
            
            if (medicine.NumberOfVerification == 2)
            {
                medicine.Approved = true;
            }
            medicationRepository.Update(medicine);
        }
        private List<MedicationDTO> ConvertMedicationToMedicationDTOList(List<Medication> medications)
        {
            List<MedicationDTO> medicationDTOs = new List<MedicationDTO>();
            foreach (Medication m in medications)
            {
                MedicationDTO medicationDTO = new MedicationDTO(m.ID,m.Name,m.Approved,m.Quantity,m.NumberOfVerification,m.AlertAmount);
                medicationDTOs.Add(medicationDTO);
            }
            return medicationDTOs;
        }

        private MedicationDTO ConvertMedicationToMedicationDTO(Medication medication)
            => new MedicationDTO(medication.ID, medication.Name, medication.Approved, medication.Quantity,medication.NumberOfVerification ,medication.AlertAmount);

        public List<MedicationDTO> ShowMedication()
        {
            List<Medication> medications = (List<Medication>)medicationRepository.GetAll();
            return ConvertMedicationToMedicationDTOList(medications);
        }

        public List<MedicationDTO> ShowApprovedMedication()
        {
            List<Medication> medications = (List<Medication>)medicationRepository.GetAllApproved();
            return ConvertMedicationToMedicationDTOList(medications);
        }
        public List<MedicationDTO> ShowNotApprovedMedication()
        {
            List<Medication> medications = (List<Medication>)medicationRepository.GetAllNotApproved();
            return ConvertMedicationToMedicationDTOList(medications);
        }

        public void UpdateQuantity(MedicationDTO medicationDTO) => medicationRepository.Update(ConvertMedicationDTOToMedication(medicationDTO));

        public MedicationDTO GetMedicationByID(int id) => ConvertMedicationToMedicationDTO(medicationRepository.GetById(id));

        public void DeleteMedication(MedicationDTO medicationDTO) => medicationRepository.Delete(ConvertMedicationDTOToMedication(medicationDTO));

    }
}
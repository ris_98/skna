using Bolnica.Model;
using Bolnica.Repository;
using Bolnica.Model.ModelDTO;
using System;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;
using Bolnica.Repository.CSV.Converter;

namespace Bolnica.Service
{
   public class LogInService
   {
        private UtilityService utilityService = new UtilityService();

        private const string SECRATARY_FILE = "../../../../../Files/secretary.csv";
        private const string Patient_FILE = "../../../../../Files/patient.csv";
        private const string DOCTOR_FILE = "../../../../../Files/doctor.csv";
        private const string HEADMASTER_FILE = "../../../../../Files/headmaster.csv";

        private const string CSV_DELIMITER = ",";
        
        public PatientRepository patientRepository = new PatientRepository(
            new CSVStream<Patient>(Patient_FILE, new PatientCSVConverter(CSV_DELIMITER)),
            new IntSequencer());
        
        public DoctorRepository doctorRepository = new DoctorRepository(
            new CSVStream<Doctor>(DOCTOR_FILE, new DoctorCSVConverter(CSV_DELIMITER)),
            new IntSequencer());

        public SecretaryRepository secretaryRepository = new SecretaryRepository(
            new CSVStream<Secretary>(SECRATARY_FILE, new SecretaryCSVConverter(CSV_DELIMITER)),
            new IntSequencer());

        public HeadmasterRepository headmasterRepository = new HeadmasterRepository(
            new CSVStream<Headmaster>(HEADMASTER_FILE, new HeadmasterCSVConverter(CSV_DELIMITER)),
            new IntSequencer());


        public UserDTO SignIn(UserDTO userDTO)
        {
            User loginuser = ConvertUserDTOToUser(userDTO);
            if (findUser(loginuser) == null) return null;
            return utilityService.convertUserToUserDTO(findUser(loginuser));
        }

        private User ConvertUserDTOToUser(UserDTO userDTO) => new User(userDTO.Username, userDTO.Password);

        private User findUser(User u)
        {
            if (utilityService.findPatient(u) != null) return (User)utilityService.findPatient(u);

            else if (utilityService.findDoctor(u) != null) return (User)utilityService.findDoctor(u);

            else if (utilityService.findSecretary(u) != null) return (User)utilityService.findSecretary(u);

            else return (User)utilityService.findHeadmaster(u);
        }

        public bool SignOut(UserDTO korisnik) => true;

    }
}
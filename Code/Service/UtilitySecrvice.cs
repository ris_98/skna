﻿using Bolnica.Model;
using Bolnica.Repository;
using Bolnica.Model.ModelDTO;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;
using Bolnica.Repository.CSV.Converter;
using System.Collections.Generic;

namespace Bolnica.Service
{
    class UtilityService
    {
        private const string SECRATARY_FILE = "../../../../../Files/secretary.csv";
        private const string PATIENT_FILE = "../../../../../Files/patient.csv";
        private const string DOCTOR_FILE = "../../../../../Files/doctor.csv";
        private const string HEADMASTER_FILE = "../../../../../Files/headmaster.csv";

        private const string CSV_DELIMITER = ",";

        public PatientRepository patientRepository = new PatientRepository(
            new CSVStream<Patient>(PATIENT_FILE, new PatientCSVConverter(CSV_DELIMITER)),new IntSequencer());

        public DoctorRepository doctorRepository = new DoctorRepository(
            new CSVStream<Doctor>(DOCTOR_FILE, new DoctorCSVConverter(CSV_DELIMITER)),new IntSequencer());

        public SecretaryRepository secretaryRepository = new SecretaryRepository(
            new CSVStream<Secretary>(SECRATARY_FILE, new SecretaryCSVConverter(CSV_DELIMITER)),new IntSequencer());

        public HeadmasterRepository headmasterRepository = new HeadmasterRepository(
            new CSVStream<Headmaster>(HEADMASTER_FILE, new HeadmasterCSVConverter(CSV_DELIMITER)),new IntSequencer());


        public Person ConvertPersonDTOToPerson(PersonDTO personDTO)
        {
            Adress adress = ConvertAdressDTOToAdress(personDTO.Adress);
            return new Person(personDTO.ID, personDTO.FirstName, personDTO.LastName, personDTO.TelephoneNumber, personDTO.JMBG, adress);
        }

        public List<PersonDTO> ConvertPersonToPersonDTOList(List<Person> people)
        {
            List<PersonDTO> personDTOs = new List<PersonDTO>();
            foreach (Person person in people)
            {
                PersonDTO personDTO = ConvertPersonToPersonDTO(person);
                personDTOs.Add(personDTO);
            }
            return personDTOs;
        }


        public PersonDTO ConvertPersonToPersonDTO(Person person)
        {
            AdressDTO adressDTO = ConvertAdressToAdressDTO(person.Adress);
            return new PersonDTO(person.ID, person.FirstName, person.LastName, person.TelephoneNumber, person.JMBG, adressDTO);
        }

        public User ConvertUserDTOToUser(UserDTO userDTO)
        {
            User user = new User(userDTO.Username, userDTO.Password);
            return user;
        }

        public Doctor ConvertDoctorDTOToDoctor(DoctorDTO doctorDTO)
        {
            Adress adress = ConvertAdressDTOToAdress(doctorDTO.Adress);
            return new Doctor(doctorDTO.EMail, doctorDTO.Username, doctorDTO.Password, doctorDTO.ID, doctorDTO.FirstName, doctorDTO.LastName, doctorDTO.TelephoneNumber, doctorDTO.JMBG, adress, doctorDTO.Specialization, doctorDTO.Rating);
        }

        public UserDTO convertUserToUserDTO(User user)
        {
            AdressDTO adressDTO = ConvertAdressToAdressDTO(user.Adress);
            UserDTO userDTO = new UserDTO(user.ID, user.EMail, user.Username, user.Password, user.FirstName, user.LastName, user.TelephoneNumber, user.JMBG, adressDTO);
            return userDTO;
        }

        public Secretary ConvertSecretaryDTOTOSecretary(SecretaryDTO secretaryDTO)
        {
            Adress adress = ConvertAdressDTOToAdress(secretaryDTO.Adress);
            return new Secretary(secretaryDTO.EMail, secretaryDTO.Username, secretaryDTO.Password, secretaryDTO.ID, secretaryDTO.FirstName, secretaryDTO.LastName, secretaryDTO.TelephoneNumber, secretaryDTO.JMBG, adress);

        }

        public DoctorDTO ConvertDoctorTODoctorDTO(Doctor doctor)
        {
            AdressDTO adressDTO = ConvertAdressToAdressDTO(doctor.Adress);
            return new DoctorDTO(doctor.EMail, doctor.Username, doctor.Password, doctor.ID, doctor.FirstName, doctor.LastName, doctor.TelephoneNumber, doctor.JMBG, adressDTO, doctor.specialization, doctor.rating);
        }

        public AdressDTO ConvertAdressToAdressDTO(Adress adress)
        {
            return  new AdressDTO(adress.ID,adress.Country,adress.City,adress.ZipCode,adress.Street);

        }

        public Adress ConvertAdressDTOToAdress(AdressDTO adressDTO)
        {
            return new Adress(adressDTO.Country,adressDTO.City,adressDTO.ZipCode,adressDTO.Street);

        }

        public SecretaryDTO ConvertSecretaryTOSecretaryDTO(Secretary secretary)
        {
            AdressDTO adressDTO = ConvertAdressToAdressDTO(secretary.Adress);
            return new SecretaryDTO(secretary.EMail, secretary.Username, secretary.Password, secretary.ID, secretary.FirstName, secretary.LastName, secretary.TelephoneNumber, secretary.JMBG, adressDTO);
        }

        public PatientDTO ConvertPatientTOPatientDTO(Patient patient)
        {
            AdressDTO adressDTO = ConvertAdressToAdressDTO(patient.Adress);
            return new PatientDTO(patient.EMail, patient.Username, patient.Password, patient.ID, patient.FirstName, patient.LastName, patient.TelephoneNumber, patient.JMBG, adressDTO);
        }

        public Patient ConvertPatientDTOToPatient(PatientDTO patientDTO)
        {
            Adress adress = ConvertAdressDTOToAdress(patientDTO.Adress);
            return new Patient(patientDTO.EMail, patientDTO.Username, patientDTO.Password, patientDTO.ID, patientDTO.FirstName, patientDTO.LastName, patientDTO.TelephoneNumber, patientDTO.JMBG, adress);
        }

        public QuestionDTO ConvertQuestionToQuestionDTO(Question question)
        {
            PatientDTO patientDTO = ConvertPatientTOPatientDTO(question.Patient);
            QuestionDTO questionDTO = new QuestionDTO(question.ID, question.QuestionContent, question.QuestionAnswer, patientDTO);
            return questionDTO;
        }

        public MedicationDTO ConvertMedicationToMedicationDTO(Medication medication)
        {
            MedicationDTO medicationDTO = new MedicationDTO(medication.ID,medication.Name,medication.Approved,medication.Quantity,medication.NumberOfVerification, medication.AlertAmount);
            
            return medicationDTO;
        }

        public Medication ConvertMedicationDTOToMedication(MedicationDTO medicationDTO)
        {
            Medication medication = new Medication(medicationDTO.ID, medicationDTO.Name, medicationDTO.Approved, medicationDTO.Quantity, medicationDTO.NumberOfVerification, medicationDTO.AlertAmount);
            return medication;
        }

        public DiagnosisDTO ConvertDiagnosisToDiagnosisDTO(Diagnosis diagnosis)
        {
            DiagnosisDTO diagnosisDTO = new DiagnosisDTO(diagnosis.ID,diagnosis.Name);
            return diagnosisDTO;
        }

        public Diagnosis ConvertDiagnosisDTOToDiagnosis(DiagnosisDTO diagnosisDTO)
        {
            Diagnosis diagnosis = new Diagnosis(diagnosisDTO.ID, diagnosisDTO.Name);
            return diagnosis;
        }

        public LabTestRequisitionDTO ConvertLabTestRequisitionToLabTestRequisitionDTO(LabTestRequisition labTestRequisition)
        {
            LabTestRequisitionDTO labTestRequisitionDTO = new LabTestRequisitionDTO(labTestRequisition.ID,labTestRequisition.Note);
            return labTestRequisitionDTO;
        }

        public LabTestRequisition ConvertLabTestRequisitionDTOToLabTestRequisition(LabTestRequisitionDTO labTestRequisitionDTO)
        {
            LabTestRequisition labTestRequisition = new LabTestRequisition(labTestRequisitionDTO.ID, labTestRequisitionDTO.Note);
            return labTestRequisition;
        }

        public PrescriptionDTO ConvertPrescriptionToPrescriptionDTO(Prescription prescription)
        {
            MedicationDTO medicationDTO = ConvertMedicationToMedicationDTO(prescription.Medication);
            PrescriptionDTO prescriptionDTO = new PrescriptionDTO(prescription.ID, prescription.ExpirationDate, prescription.Dosage, medicationDTO);

            return prescriptionDTO;
        }
        public Prescription ConvertPrescriptionDTOToPrescription(PrescriptionDTO prescriptionDTO)
        {
            Medication medication = ConvertMedicationDTOToMedication(prescriptionDTO.Medication);
            Prescription prescription = new Prescription(prescriptionDTO.ID, prescriptionDTO.ExpirationDate, prescriptionDTO.Dosage, medication);

            return prescription;
        }

        public ExaminationReportDTO ConvertExaminationReportToExaminationReportDTO(ExaminationReport examinationReport)
        {
            PrescriptionDTO prescriptionDTO = ConvertPrescriptionToPrescriptionDTO(examinationReport.Prescription);
            LabTestRequisitionDTO labTestRequisitionDTO = ConvertLabTestRequisitionToLabTestRequisitionDTO(examinationReport.LabTestRequisition);
            DiagnosisDTO diagnosisDTO = ConvertDiagnosisToDiagnosisDTO(examinationReport.Diagnosis);
            ExaminationReportDTO examinationReportDTO = new ExaminationReportDTO(examinationReport.ID,prescriptionDTO,diagnosisDTO,labTestRequisitionDTO);

            return examinationReportDTO;
        }

        public ExaminationReport ConvertExaminationReportDTOToExaminationReport(ExaminationReportDTO examinationReportDTO)
        {
            Prescription prescription = ConvertPrescriptionDTOToPrescription(examinationReportDTO.Prescription);
            LabTestRequisition labTestRequisition = ConvertLabTestRequisitionDTOToLabTestRequisition(examinationReportDTO.LabTestRequisition);
            Diagnosis diagnosis = ConvertDiagnosisDTOToDiagnosis(examinationReportDTO.Diagnosis);
            ExaminationReport examinationReport = new ExaminationReport(examinationReportDTO.ID, prescription, diagnosis, labTestRequisition);
            return examinationReport;
        }

        public Question ConvertQuestionDTOToQuestion(QuestionDTO questionDTO)
        {
            Patient patient = patientRepository.GetById(questionDTO.Patient.ID);
            Question question = new Question(questionDTO.ID, questionDTO.QuestionContent, questionDTO.QuestionAnswer, patient);
            return question;
        }

        public AppointmentDTO ConvertAppointmentToAppointmentDTO(Appointment appointment)
        {
            DoctorDTO doctorDTO = ConvertDoctorTODoctorDTO(appointment.Doctor);
            PatientDTO patientDTO = ConvertPatientTOPatientDTO(appointment.Patient);

            return new AppointmentDTO(appointment.ID, appointment.DateTime, appointment.DurationInMinuts, appointment.AppointmentType, patientDTO, doctorDTO);
        }

        public List<AppointmentDTO> ConvertAppointmentToAppointmentDTOList(List<Appointment> appointment)
        {
            List<AppointmentDTO> appointmentDTOs = new List<AppointmentDTO>();
            foreach (Appointment a in appointment)
            {
                AppointmentDTO appointmentDTO = ConvertAppointmentToAppointmentDTO(a);
                appointmentDTOs.Add(appointmentDTO);
            }
            return appointmentDTOs;
        }
        public MedicalRecordDTO ConvertMedicalRecordToMedicalRecordDTO(MedicalRecord medicalRecord)
        {
            AppointmentDTO appointmentDTO = ConvertAppointmentToAppointmentDTO(medicalRecord.Appointment);
            ExaminationReportDTO examinationReportDTO = ConvertExaminationReportToExaminationReportDTO(medicalRecord.ExaminationReport);

            return new MedicalRecordDTO(medicalRecord.ID, examinationReportDTO, appointmentDTO);
        }

        public List<MedicalRecordDTO> ConvertMedicalRecordToMedicalRecordDTOList(List<MedicalRecord> medicalRecord)
        {
            List<MedicalRecordDTO> medicalRecordDTOs = new List<MedicalRecordDTO>();
            foreach (MedicalRecord m in medicalRecord)
            {
                MedicalRecordDTO medicalRecordDTO = ConvertMedicalRecordToMedicalRecordDTO(m);
                medicalRecordDTOs.Add(medicalRecordDTO);
            }
            return medicalRecordDTOs;
        }
        public List<DoctorDTO> ConvertDoctorToDoctorDTOList(List<Doctor> doctors)
        {
            List<DoctorDTO> doctorDTOs = new List<DoctorDTO>();
            foreach (Doctor d in doctors)
            {
                DoctorDTO doctorDTO = ConvertDoctorTODoctorDTO(d); 
                doctorDTOs.Add(doctorDTO);
            }
            return doctorDTOs;
        }

        public string findUserType(User u)
        {
            if (findPatient(u) != null) return "Patient";

            else if (findDoctor(u) != null) return "Doctor";

            else if (findSecretary(u) != null) return "Secretary";

            else return "Headmaster";
        }

        public Secretary findSecretary(User secretary)
        {
            return secretaryRepository.GetSecretaryByUserNameAndPassword(secretary.Username, secretary.Password);

        }

        public Headmaster findHeadmaster(User headmaster)
        {
            return headmasterRepository.GetHeadmasterByUserNameAndPassword(headmaster.Username, headmaster.Password);
        }

        public Doctor findDoctor(User doctor)
        {
            return doctorRepository.GetDoctorByUserNameAndPassword(doctor.Username, doctor.Password);
        }

        public Patient findPatient(User patinet)
        {
            return patientRepository.GetPatinetByUserNameAndPassword(patinet.Username, patinet.Password);
        }
        public Room ConvertRoomDTOToRoom(RoomDTO roomDTO)
        {
            List<Patient> patients = ConvertPatientDTOToPatientList(roomDTO.Patient);
            List<RoomEquipment> equipment = ConvertRoomEQDToRoomEqList(roomDTO.RoomEquipment);
            return new Room(roomDTO.ID, roomDTO.RoomType, patients,equipment);
        }

        public RoomDTO ConvertRoomToRoomDTO(Room room)
        {
            List<PatientDTO> patients = ConvertPatientToPatientDTOList(room.Patient);
            List<RoomEquipmentDTO> equipment = ConvertRoomEqToRoomEqDTOList(room.RoomEquipment);
            return new RoomDTO(room.ID, room.RoomType, patients,equipment);
        }

        public List<RoomEquipmentDTO> ConvertRoomEqToRoomEqDTOList(List<RoomEquipment> roomEquipment)
        {
            List<RoomEquipmentDTO> roomEquipmentDTO = new List<RoomEquipmentDTO>();
            foreach(RoomEquipment rE in roomEquipment)
            {
                EquipmentDTO e = ConvertEquipmentToEquipmentDTO(rE.equipment);
                RoomEquipmentDTO reDTO= new RoomEquipmentDTO(e, rE.Quantity);
                roomEquipmentDTO.Add(reDTO);
            }
            return roomEquipmentDTO;
        }



        public List<RoomEquipment> ConvertRoomEQDToRoomEqList(List<RoomEquipmentDTO> roomEquipmentDTO)
        {
            List<RoomEquipment> roomEquipment = new List<RoomEquipment>();
            foreach (RoomEquipmentDTO rE in roomEquipmentDTO)
            {
                Equipment e = ConvertEquipmentDTOTOEquipment(rE.equipment);
                RoomEquipment re = new RoomEquipment(e, rE.Quantity);
                roomEquipment.Add(re);
            }
            return roomEquipment;
        }

        public RoomEquipment ConvertRoomEQDToRoomEq(RoomEquipmentDTO roomEquipmentDTO)
        {
            Equipment e = ConvertEquipmentDTOTOEquipment(roomEquipmentDTO.equipment);
            return new RoomEquipment(e, roomEquipmentDTO.Quantity);

        }



        public Equipment ConvertEquipmentDTOTOEquipment(EquipmentDTO equipmentDTO) => new Equipment(equipmentDTO.ID, equipmentDTO.Name, equipmentDTO.Description, equipmentDTO.Quantity, equipmentDTO.Type, equipmentDTO.AlertAmount);

        public List<Equipment> ConvertEquipmentDTOTOEquipmentList(List<EquipmentDTO> equipmentDTO)
        {
            List<Equipment> equipment = new List<Equipment>();
            foreach(EquipmentDTO eDTO in equipmentDTO)
            {
                Equipment e = ConvertEquipmentDTOTOEquipment(eDTO);
                equipment.Add(e);
            }
            return equipment;
        }

        public List<EquipmentDTO> ConvertEquipmentToEquipmentDTODTOList(List<Equipment> equipment)
        {
            List<EquipmentDTO> equipmentDTOs = new List<EquipmentDTO>();
            foreach (Equipment e in equipment)
            {
                EquipmentDTO equipmentDTO = ConvertEquipmentToEquipmentDTO(e);
                equipmentDTOs.Add(equipmentDTO);
            }
            return equipmentDTOs;
        }

        public EquipmentDTO ConvertEquipmentToEquipmentDTO(Equipment equipment)
        {
            EquipmentDTO equipmentDTO = new EquipmentDTO(equipment.ID,equipment.Quantity,equipment.AlertAmount,equipment.Name,equipment.Description, equipment.Type);
            return equipmentDTO;
        }

        public List<PatientDTO> ConvertPatientToPatientDTOList(List<Patient> patients)
        {
            List<PatientDTO> patientDTOs = new List<PatientDTO>();
            foreach (Patient p in patients)
            {
                PatientDTO patientDTO = ConvertPatientTOPatientDTO(p);
                patientDTOs.Add(patientDTO);
            }
            return patientDTOs;
        }

        public List<Patient> ConvertPatientDTOToPatientList(List<PatientDTO> patientsDTO)
        {
            List<Patient> patients = new List<Patient>();
            foreach (PatientDTO p in patientsDTO)
            {
                Patient patient = ConvertPatientDTOToPatient(p);
                patients.Add(patient);
            }
            return patients;
        }

        public List<RoomDTO> ConvertRoomToRoomDTOList(List<Room> rooms)
        {
            List<RoomDTO> roomDTOs = new List<RoomDTO>();
            foreach (Room room in rooms)
            {
                RoomDTO roomDTO = ConvertRoomToRoomDTO(room);
                roomDTOs.Add(roomDTO);
            }
            return roomDTOs;
        }

    }
}

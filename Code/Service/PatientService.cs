using Bolnica.Model;
using Bolnica.Repository;
using Bolnica.Model.ModelDTO;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;
using Bolnica.Repository.CSV.Converter;
using System;

namespace Bolnica.Service
{
   public class PatientService
   {
        UtilityService utilityService = new UtilityService();
        private const string Patient_FILE = "../../../../../Files/patient.csv";
        private const string ADRESS_FILE = "../../../../../Files/adress.csv";
        private const string DOCTOR_FILE = "../../../../../Files/doctor.csv";
        private const string DOCTORFEEDBACK_FILE = "../../../../../Files/doctorfeedback.csv";

        private const string CSV_DELIMITER = ",";

        private PatientRepository patientRepository = new PatientRepository(
            new CSVStream<Patient>(Patient_FILE, new PatientCSVConverter(CSV_DELIMITER)),
            new IntSequencer());


        public AdressRepository adressRepository = new AdressRepository(
            new CSVStream<Adress>(ADRESS_FILE, new AdressCSVConverter(CSV_DELIMITER)),
            new IntSequencer());

        public DoctorRepository doctorRepository = new DoctorRepository(
            new CSVStream<Doctor>(DOCTOR_FILE, new DoctorCSVConverter(CSV_DELIMITER)),
            new IntSequencer());

        public DoctorFeedbackRepository doctorFeedbackRepository = new DoctorFeedbackRepository(
            new CSVStream<DoctorFeedback>(DOCTORFEEDBACK_FILE, new DoctorFeedbackCSVConverter(CSV_DELIMITER)),
            new IntSequencer());

        private DoctorFeedback ConvertDoctorFeedbackDTOToDoctorFeedback(DoctorFeedbackDTO doctorFeedbackDTO)
        {
            Doctor doctor = utilityService.ConvertDoctorDTOToDoctor(doctorFeedbackDTO.Doctor);
            DoctorFeedback doctorFeedback = new DoctorFeedback(doctorFeedbackDTO.ID,doctorFeedbackDTO.Rating,doctor,doctorFeedbackDTO.NumberOfRatings);
            return doctorFeedback;
        }

        public void DoctorFeedback(DoctorFeedbackDTO feedback)
        {
            DoctorFeedback doctorFeedback = ConvertDoctorFeedbackDTOToDoctorFeedback(feedback);
            DoctorFeedback dF = doctorFeedbackRepository.GetById(doctorFeedback.Doctor.ID);
            Doctor doctor= doctorRepository.GetById(feedback.Doctor.ID);
            float newRating = dF.Rating * dF.NumberOfRatings + feedback.Rating;
            dF.NumberOfRatings++;
            newRating /= dF.NumberOfRatings;
            doctor.rating = newRating;
            doctorFeedback.Rating = newRating;
            doctorFeedback.NumberOfRatings = dF.NumberOfRatings;
            doctorRepository.Update(doctor);
            doctorFeedbackRepository.Update(doctorFeedback);

        }

        public void RegisterPatient(UserDTO userDTO)
        {
            User user = patientRepository.Create(ConvertUserDTOToUser(userDTO));
            adressRepository.Create(user.Adress);
        }


        private Patient ConvertUserDTOToUser(UserDTO userDTO)
        {
            Patient patient = new Patient(userDTO.EMail, userDTO.Username, userDTO.Password,userDTO.ID,userDTO.FirstName,userDTO.LastName,userDTO.TelephoneNumber,userDTO.JMBG, new Adress());
            return patient;
        }
   
   }
}
using Bolnica.Model;
using Bolnica.Model.ModelDTO;
using Bolnica.Repository;
using Bolnica.Repository.CSV.Converter;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;
using Bolnica.Service;
using System;

namespace Service
{
   public class HeadmasterService
   {
        UtilityService UtilityService = new UtilityService();
        private const string SECRATARY_FILE = "../../../../../Files/secretary.csv";
        private const string DOCTOR_FILE = "../../../../../Files/doctor.csv";
        private const string HEADMASTER_FILE = "../../../../../Files/headmaster.csv";
        private const string ADRESS_FILE = "../../../../../Files/adress.csv";
        private const string DOCTORFEEDBACK_FILE = "../../../../../Files/doctorfeedback.csv";


        private const string CSV_DELIMITER = ",";

        public DoctorRepository doctorRepository = new DoctorRepository(
            new CSVStream<Doctor>(DOCTOR_FILE, new DoctorCSVConverter(CSV_DELIMITER)), new IntSequencer());

        public SecretaryRepository secretaryRepository = new SecretaryRepository(
            new CSVStream<Secretary>(SECRATARY_FILE, new SecretaryCSVConverter(CSV_DELIMITER)), new IntSequencer());

        public HeadmasterRepository headmasterRepository = new HeadmasterRepository(
            new CSVStream<Headmaster>(HEADMASTER_FILE, new HeadmasterCSVConverter(CSV_DELIMITER)), new IntSequencer());

        public AdressRepository adressRepository = new AdressRepository(
            new CSVStream<Adress>(ADRESS_FILE, new AdressCSVConverter(CSV_DELIMITER)), new IntSequencer());


        public DoctorFeedbackRepository doctorFeedbackRepository = new DoctorFeedbackRepository(
            new CSVStream<DoctorFeedback>(DOCTORFEEDBACK_FILE, new DoctorFeedbackCSVConverter(CSV_DELIMITER)), new IntSequencer());


        public void RegisterDoctor(DoctorDTO doctorDTO)
        {
            Doctor doctor = UtilityService.ConvertDoctorDTOToDoctor(doctorDTO);
            DoctorFeedback  doctorFeedback = new DoctorFeedback(doctor.ID, 0, doctor, 0);
            doctorFeedbackRepository.Create(doctorFeedback);
            doctorRepository.Create(doctor);
            adressRepository.Create(doctor.Adress);
        }
      
        public void RegisterSecretary(SecretaryDTO secretaryDTO)
        {
            Secretary secretary = UtilityService.ConvertSecretaryDTOTOSecretary(secretaryDTO);
            secretaryRepository.Create(secretary);
            adressRepository.Create(secretary.Adress);
        }
      
        public ProcurementDTO MakeProcuremnt(ProcurementDTO procurmentDTO)
        {
            throw new NotImplementedException();
        }

      
        public void DeleteEmployee(UserDTO employeeDTO)
        {
            User employee = UtilityService.ConvertUserDTOToUser(employeeDTO);
            string type = UtilityService.findUserType(employee);
            if (type.Equals("Doctor"))
            {
                Doctor doctor = UtilityService.findDoctor(employee);
                doctorRepository.Delete(doctor);
            }
            else if (type.Equals("Secretary"))
            {
                Secretary secretary = UtilityService.findSecretary(employee);
                secretaryRepository.Delete(secretary);
            }
        }

        public void GiveDoctorSpecialization(DoctorDTO doctorDTO)
        {
            Doctor doctor = UtilityService.ConvertDoctorDTOToDoctor(doctorDTO);
            doctorRepository.Update(doctor);
        }

    }
}
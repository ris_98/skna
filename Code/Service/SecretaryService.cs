using Bolnica.Repository.CSV.Converter;
using Bolnica.Model;
using Bolnica.Model.ModelDTO;
using Bolnica.Repository;
using Bolnica.Repository.CSV.Converter;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;

namespace Bolnica.Service
{
   public class SecretaryService
   {
        private UtilityService utilityService = new UtilityService();

        private const string Patient_FILE = "../../../../../Files/patient.csv";
        private const string Person_FILE = "../../../../../Files/person.csv";
        private const string ADRESS_FILE = "../../../../../Files/adress.csv";
        private const string SECRATARY_FILE = "../../../../../Files/secretary.csv";

        private const string CSV_DELIMITER = ",";

        public SecretaryRepository secretaryRepository = new SecretaryRepository(
            new CSVStream<Secretary>(SECRATARY_FILE, new SecretaryCSVConverter(CSV_DELIMITER)),
            new IntSequencer());
        public AdressRepository adressRepository = new AdressRepository(
            new CSVStream<Adress>(ADRESS_FILE, new AdressCSVConverter(CSV_DELIMITER)),
            new IntSequencer());

        public PatientRepository patientRepository = new PatientRepository(
             new CSVStream<Patient>(Patient_FILE, new PatientCSVConverter(CSV_DELIMITER)),
            new IntSequencer());

        public PersonRepository personRepository = new PersonRepository(
             new CSVStream<Person>(Person_FILE, new PersonCSVConverter(CSV_DELIMITER)),
            new IntSequencer());


        public void RegisterUser(PatientDTO userDTO)
        {
            Patient patient = utilityService.ConvertPatientDTOToPatient(userDTO);
            adressRepository.Create(patient.Adress);
            patientRepository.Create(patient);
        }

        public void RegisterPerson(PersonDTO personDTO)
        {   
            Person person = utilityService.ConvertPersonDTOToPerson(personDTO);
            adressRepository.Create(person.Adress);
            personRepository.Create(person);
        }
      
        public void UpdateUser(PatientDTO userDTO)
        {
            Patient patient = utilityService.ConvertPatientDTOToPatient(userDTO);
            patientRepository.Update(patient);
            adressRepository.Update(patient.Adress);
        }
      
        public void UpdatePerson(PersonDTO personDTO)
        {
            Person person = utilityService.ConvertPersonDTOToPerson(personDTO);
            personRepository.Update(person);
            adressRepository.Update(person.Adress);
        
        }
        public void UpdateMYACC(SecretaryDTO secretaryDTO)
        {
            Secretary secretary = utilityService.ConvertSecretaryDTOTOSecretary(secretaryDTO);
            secretaryRepository.Update(secretary);
            adressRepository.Update(secretary.Adress);
        }

        public PatientDTO GetPatientByID(int id)
        {
            Patient patient = patientRepository.GetById(id);
            return utilityService.ConvertPatientTOPatientDTO(patient);
        }

        public PersonDTO GetPersonByID(int id)
        {
            Person person = personRepository.GetById(id);
            return utilityService.ConvertPersonToPersonDTO(person);
        }

    }
}
using Bolnica.Model;
using Bolnica.Model.ModelDTO;
using Bolnica.Repository;
using Bolnica.Repository.CSV.Converter;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;
using System.Collections.Generic;

namespace Bolnica.Service
{
   public class EmployeeService
    {
        private UtilityService utilityService = new UtilityService();

        private const string Patient_FILE = "../../../../../Files/patient.csv";
        private const string Person_FILE = "../../../../../Files/person.csv";

        private const string CSV_DELIMITER = ",";

        private PatientRepository patientRepository = new PatientRepository(
              new CSVStream<Patient>(Patient_FILE, new PatientCSVConverter(CSV_DELIMITER)),
              new IntSequencer());

        public PersonRepository personRepository = new PersonRepository(
             new CSVStream<Person>(Person_FILE, new PersonCSVConverter(CSV_DELIMITER)),
            new IntSequencer());


        public List<PatientDTO> ShowPatients()
        {
            List<Patient> patients = (List<Patient>) patientRepository.GetAll();
            return ConvertPatientToPatientDTOList(patients);
        }

        private List<PatientDTO> ConvertPatientToPatientDTOList(List<Patient> patients)
        {
            List<PatientDTO> patientDTOs = new List<PatientDTO>();
            foreach (Patient p in patients)
            {
                PatientDTO patientDTO = utilityService.ConvertPatientTOPatientDTO(p);
                patientDTOs.Add(patientDTO);
            }
            return patientDTOs;
        }

        public PatientDTO GetPatientByID(int id)
        {
            Patient patient = patientRepository.GetById(id);
            return utilityService.ConvertPatientTOPatientDTO(patient);
        }

        public List<PersonDTO> GetPersons()
        {
            List<Person> people =(List<Person>) personRepository.GetAll();
            return utilityService.ConvertPersonToPersonDTOList(people);
        }

    }
}
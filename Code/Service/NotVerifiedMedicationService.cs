﻿using Bolnica.Model;
using Bolnica.Model.ModelDTO;
using Bolnica.Repository;
using Bolnica.Repository.CSV.Converter;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;
using System.Collections.Generic;

namespace Bolnica.Service
{
    public class NotVerifiedMedicationService
    {
        private const string EXPERIMENTMEDICATION_FILE = "../../../../../Files/experimentmedications.csv";
        private const string MEDICATION_FILE = "../../../../../Files/medications.csv";
        private const string CSV_DELIMITER = ",";

        public ExperimentMedicationRepository experimentMedicationRepository = new ExperimentMedicationRepository(
               new CSVStream<NotVerifiedMedication>(EXPERIMENTMEDICATION_FILE, new ExperimentMedicationCSVConverter(CSV_DELIMITER)),
               new IntSequencer());
        public MedicationRepository medicationRepository = new MedicationRepository(
               new CSVStream<Medication>(MEDICATION_FILE, new MedicationCSVConverter(CSV_DELIMITER)),
               new IntSequencer());

        private List<NotVerifiedMedicationDTO> ConvertExperimentMedicationToExperimentMedicationDTOList(List<NotVerifiedMedication> notVerifiedMedicationDTO)
        {
            List<NotVerifiedMedicationDTO> experimentmedicationDTOs = new List<NotVerifiedMedicationDTO>();
            foreach (NotVerifiedMedication m in notVerifiedMedicationDTO)
            {
                NotVerifiedMedicationDTO experimentmedicationDTO = new NotVerifiedMedicationDTO(m.ID,m.Name, m.Note);
                experimentmedicationDTOs.Add(experimentmedicationDTO);
            }
            return experimentmedicationDTOs;
        }

        private Medication ConvertExperimentMedicationDTOToMedication(NotVerifiedMedicationDTO notVerifiedMedicationDTO)
        {
            Medication medication = new Medication(notVerifiedMedicationDTO.ID, notVerifiedMedicationDTO.Name,false,0,0,0);
            return medication;
        }
        private NotVerifiedMedication ConvertExperimentMedicationDTOToExperimentMedication(NotVerifiedMedicationDTO experimentmedicationDTO)
        {
            NotVerifiedMedication medication = new NotVerifiedMedication(experimentmedicationDTO.ID,experimentmedicationDTO.Name,experimentmedicationDTO.Note);
            return medication;
        }

        public void SendMedicationToVerify(NotVerifiedMedicationDTO experimentmedicineDTO)
        {
            medicationRepository.Create(ConvertExperimentMedicationDTOToMedication(experimentmedicineDTO));
            experimentMedicationRepository.Delete(ConvertExperimentMedicationDTOToExperimentMedication(experimentmedicineDTO));
        }

        public List<NotVerifiedMedicationDTO> ShowExperimentMedication()
        {
            List<NotVerifiedMedication> experimentmedications = (List<NotVerifiedMedication>)experimentMedicationRepository.GetAll();
            return ConvertExperimentMedicationToExperimentMedicationDTOList(experimentmedications);
        }    

    }
}
using Bolnica.Model;
using Bolnica.Model.ModelDTO;
using Bolnica.Repository;
using Bolnica.Repository.CSV.Converter;
using Bolnica.Repository.CSV.Stream;
using Bolnica.Repository.Sequencer;
using System.Collections.Generic;

namespace Bolnica.Service
{
   public class UserService
    {
        UtilityService utilityService = new UtilityService();

        private const string FEEDBACK_FILE = "../../../../../Files/feedback.csv";
        private const string DOCTOR_FILE = "../../../../../Files/doctor.csv";
        private const string ADRESS_FILE = "../../../../../Files/adress.csv";
        private const string PATIENT_FILE = "../../../../../Files/patient.csv";
        private const string SECRATARY_FILE = "../../../../../Files/secretary.csv";
        private const string REPORT_FILE = "../../../../../Files/report.csv";

        private const string CSV_DELIMITER = ",";

        public FeedbackRepository feedbackRepository = new FeedbackRepository(
            new CSVStream<Feedback>(FEEDBACK_FILE, new FeedbackCSVConverter(CSV_DELIMITER)),new IntSequencer());

        public DoctorRepository doctorRepository = new DoctorRepository(
          new CSVStream<Doctor>(DOCTOR_FILE, new DoctorCSVConverter(CSV_DELIMITER)),new IntSequencer());

        public AdressRepository adressRepository = new AdressRepository(
            new CSVStream<Adress>(ADRESS_FILE, new AdressCSVConverter(CSV_DELIMITER)),new IntSequencer());

        private PatientRepository patientRepository = new PatientRepository(
            new CSVStream<Patient>(PATIENT_FILE, new PatientCSVConverter(CSV_DELIMITER)),new IntSequencer());

        public SecretaryRepository secretaryRepository = new SecretaryRepository(
            new CSVStream<Secretary>(SECRATARY_FILE, new SecretaryCSVConverter(CSV_DELIMITER)),new IntSequencer());

        public ReportRepository reportRepository = new ReportRepository(
            new CSVStream<Report>(REPORT_FILE, new ReportCSVConverter(CSV_DELIMITER)), new IntSequencer());


        public void LeaveFeedback(FeedbackDTO feedbackDTO) => feedbackRepository.Create(ConvertFeedbackDTOToFeedback(feedbackDTO));

        public void LeaveBugReport(ReportDTO reportDTO) => reportRepository.Create(ConvertReportDTOToReport(reportDTO));

        private Feedback ConvertFeedbackDTOToFeedback(FeedbackDTO feedbackDTO) => new Feedback(feedbackDTO.ID, feedbackDTO.Rating, feedbackDTO.Message);

        private Report ConvertReportDTOToReport(ReportDTO reportDTO) => new Report(reportDTO.ID,  reportDTO.Data);

        public List<DoctorDTO> GetAllDoctors()
        {
            List<Doctor> doctors = (List < Doctor >) doctorRepository.GetAll();
            return utilityService.ConvertDoctorToDoctorDTOList(doctors);
        }

        public DoctorDTO GetDoctorByID(int id)
        {
            Doctor doctor = doctorRepository.GetById(id);
            return utilityService.ConvertDoctorTODoctorDTO(doctor);
        }

        public List<DoctorDTO> GetAllDoctorsBySpecialization(string specialization)
        {
            List<Doctor> doctors = (List<Doctor>)doctorRepository.GetAllBySpecialization(specialization);
            return utilityService.ConvertDoctorToDoctorDTOList(doctors);
        }

        public void UpdateMyAccDoctor(DoctorDTO doctorDTO)
        {
            Doctor doctor = utilityService.ConvertDoctorDTOToDoctor(doctorDTO);
            doctorRepository.Update(doctor);
            adressRepository.Update(doctor.Adress);
        }

        public void UpdateMyAccPatient(PatientDTO patientDTO)
        {
            Patient patient = utilityService.ConvertPatientDTOToPatient(patientDTO);
            patientRepository.Update(patient);
            adressRepository.Update(patient.Adress);
        }

        public void UpdateMyAccSecretary(SecretaryDTO secretaryDTO)
        {
            Secretary secretary = utilityService.ConvertSecretaryDTOTOSecretary(secretaryDTO);
            secretaryRepository.Update(secretary);
            adressRepository.Update(secretary.Adress);
        }

        public void UpdateMyAccHeadmaster(HeadmasterDTO headmasterDTO)
        {
            return;
        }
    }
}